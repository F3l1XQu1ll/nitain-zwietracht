# Nitain Crab for Discord

Nitain Crab (or NitainCrab, Crab) is - you guessed it: a Discord Bot.

# Why?

> Why not

This project has no clear aim. Features are added at will.

# What can do?

*Inhales*

In the beginning, the bot was designed as an utility tool for playing Warframe:
 - Get price information for certain items from Warframe.market
 - Worldstate info, syndicate missions, world cycles
 - Void Trader (Baro) info
 - Daily Sortie info
 - Events (may or may not work, is too rare to be tested correctly)
 - Fissures info

Later, even before the Rythm project was discontinued,
a more and more advanced music player functionality was added.

So far this includes:
 - Playing almost any video as audio from Youtube (doesn't work for age restricted content)
 - Playing Youtube playlists
 - Playing Songs, Albums, Playlists from Spotify via Youtube
 - Player interface in discord with:
   - progress indicator
   - track navigation
   - Loop option
   - Shuffle (as command)
   - Autoplay (automatically adding new tracks once the queue if empty)
 - Lyrics lookup from Genius and MusixMatch (can be used for current track or a search string)

Additional to this we added commands regarding DOTA 2:
 - View your last, or any match by id
 - Profile info
 - Pro-player info (recent matches, with items)
 - and some more experimental features

Finaly we have build a Web Interface for the Warframe worldstate and controlling the player.
 - This is still work in progress, the finaly goal is to archive feature parity with the discord client.

 [WEB INTERFACE](https://f3l1xqu1ll.gitlab.io/nitain-zwietracht)
