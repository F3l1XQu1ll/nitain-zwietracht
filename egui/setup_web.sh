#!/usr/bin/env bash
set -eu

rustup target add wasm32-unknown-unknown
cargo install wasm-bindgen-cli
cargo install trunk

cargo install basic-http-server

