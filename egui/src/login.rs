use egui::{Color32, Context};

use crate::{
    api::Api,
    app::{App, Page},
};

pub trait LoginPage {
    fn login_page(&mut self, ctx: &Context);
}

impl LoginPage for App {
    fn login_page(&mut self, ctx: &Context) {
        egui::Window::new("Login")
            .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::ZERO)
            .resizable(false)
            .collapsible(false)
            .show(ctx, |ui| {
                ui.vertical_centered_justified(|ui| {
                    ui.horizontal(|ui| {
                        ui.label("Token:");
                        ui.text_edit_singleline(&mut self.token);
                    });
                    ui.add_space(16.);
                    let login = &mut self.login_pending;
                    match login {
                        None => {
                            if ui.button("Login").clicked() {
                                *login = Some(self.api_endpoint.auth(&self.token));
                                debug!("Clicked on login button");
                            }
                        }
                        Some(promise) => match promise.ready() {
                            Some(Some(user)) => {
                                self.user = Some(user.clone());
                                self.page = Page::Dashboard
                            }
                            Some(None) => {
                                ui.colored_label(Color32::RED, "Login failed!");
                            }
                            None => {
                                ui.spinner();
                            }
                        },
                    }
                });
            });
    }
}
