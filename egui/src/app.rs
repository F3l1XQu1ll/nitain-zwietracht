use std::time::Duration;

use crab_model::{
    auth::UserRecord,
    player::{AvailableGuild, QueueState, State},
    warframe::WFWorld,
};
use egui::Color32;
use instant::Instant;
use poll_promise::Promise;

use serde::{Deserialize, Serialize};

use crate::{
    api::{Api, ApiEndpoint, ApiState},
    dashboard::DashboardPage,
    login::LoginPage,
    world::WorldPage,
};

#[derive(Deserialize, Serialize)]
pub(crate) enum Page {
    Login,
    World,
    Dashboard,
}

#[derive(Deserialize, Serialize, PartialEq, Clone)]
pub(crate) enum SelectedConnection {
    Nitain,
    Void,
    Custom(ApiEndpoint),
}

#[derive(Deserialize, Serialize)]
pub(crate) struct App {
    pub(crate) api_endpoint: ApiEndpoint,
    pub(crate) selected_connection: SelectedConnection,
    /// Current page / view
    pub(crate) page: Page,
    /// Token set at login page, validity not guarantied
    pub(crate) token: String,
    #[serde(skip)]
    api_watcher: Option<Promise<Option<ApiState>>>,
    #[serde(skip)]
    api_refreshed: Option<Instant>,
    #[serde(skip)]
    pub(crate) world_loader: Option<Promise<Option<WFWorld>>>,
    #[serde(skip)]
    pub(crate) world_refreshed: Option<Instant>,
    pub(crate) user: Option<UserRecord>,
    #[serde(skip)]
    pub(crate) login_pending: Option<Promise<Option<UserRecord>>>,
    #[serde(skip)]
    pub(crate) logs_loader: Option<Promise<Option<String>>>,
    #[serde(skip)]
    pub(crate) data_loader: Option<Promise<Option<String>>>,
    #[serde(skip)]
    pub(crate) player_state: Option<Promise<Option<State>>>,
    #[serde(skip)]
    pub(crate) player_cache: Option<State>,
    #[serde(skip)]
    pub(crate) player_state_updated: Option<Instant>,
    #[serde(skip)]
    pub(crate) available_players: Option<Promise<Option<Vec<AvailableGuild>>>>,
    pub(crate) player_selected_guild: Option<AvailableGuild>,
    #[serde(skip)]
    pub(crate) queue_loader: Option<Promise<Option<QueueState>>>,
    #[serde(skip)]
    pub(crate) queue_refreshed: Option<Instant>,
    pub(crate) queue_show_history: bool,
    pub(crate) queue_cache: Option<QueueState>,
}

impl Default for App {
    fn default() -> Self {
        Self {
            // api_endpoint: ApiEndpointDescriptor::custom("http://0.0.0.0:8000".to_string()),
            api_endpoint: ApiEndpoint::nitain_crab(),
            selected_connection: SelectedConnection::Nitain,
            page: Page::Login,
            token: "".to_owned(),
            api_watcher: None,
            api_refreshed: None,
            world_loader: None,
            world_refreshed: None,
            user: None,
            login_pending: None,
            logs_loader: None,
            data_loader: None,
            player_state: None,
            player_cache: None,
            player_state_updated: None,
            available_players: None,
            player_selected_guild: None,
            queue_loader: None,
            queue_refreshed: None,
            queue_show_history: false,
            queue_cache: None,
        }
    }
}

impl App {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }

        Self::default()
    }
}

impl eframe::App for App {
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::TopBottomPanel::top("top-panel").show(ctx, |ui| {
            egui::menu::bar(ui, |ui| {
                ui.menu_button("Connection", |ui| {
                    ui.selectable_value(
                        &mut self.selected_connection,
                        SelectedConnection::Nitain,
                        "NitainCrab",
                    );
                    ui.selectable_value(
                        &mut self.selected_connection,
                        SelectedConnection::Void,
                        "NitainCrab(2)",
                    );
                    let custom_connection = {
                        if let SelectedConnection::Custom(custom) = self.selected_connection.clone()
                        {
                            SelectedConnection::Custom(custom)
                        } else {
                            SelectedConnection::Custom(ApiEndpoint::custom(
                                "http://0.0.0.0:8000/".to_string(),
                            ))
                        }
                    };
                    ui.selectable_value(&mut self.selected_connection, custom_connection, "Custom");
                    match &mut self.selected_connection {
                        SelectedConnection::Nitain => {
                            self.api_endpoint = ApiEndpoint::nitain_crab()
                        }
                        SelectedConnection::Void => self.api_endpoint = ApiEndpoint::void_crab(),
                        SelectedConnection::Custom(custom) => {
                            ui.text_edit_singleline(custom.base_mut());
                            self.api_endpoint = custom.clone();
                        }
                    }
                });
                if self.user.is_some() {
                    if ui.button("Dashboard").clicked() {
                        debug!("Clicked on dashboard button");
                        self.page = Page::Dashboard;
                    }
                } else {
                    if ui.button("Login").clicked() {
                        debug!("Clicked on login button");
                        self.page = Page::Login;
                    }
                }
                if ui.button("World").clicked() {
                    debug!("Clicked on world button");
                    self.page = Page::World;
                }
                if let Some(user) = self.user.clone() {
                    ui.with_layout(egui::Layout::right_to_left(), |ui| {
                        ui.menu_button(format!("{}#{}", user.name, user.id), |ui| {
                            if ui.button("Logout").clicked() {
                                self.user = None;
                                self.token.clear();
                            }
                        })
                    });
                }
            });
        });

        egui::CentralPanel::default().show(ctx, |ui| match self.page {
            Page::Login => {
                self.login_page(ctx);
            }
            Page::World => {
                self.world_page(ctx, ui);
            }
            Page::Dashboard => {
                self.dashboard_page(ctx);
            }
        });

        let promise = self
            .api_watcher
            .get_or_insert_with(|| self.api_endpoint.nitain());

        egui::TopBottomPanel::bottom("bottom-panel").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.vertical(|ui| {
                    match promise.ready() {
                        Some(res) => {
                            match res {
                                Some(ApiState::Online) => {
                                    ui.colored_label(Color32::GREEN, "API Online");
                                }
                                Some(ApiState::Error) => {
                                    ui.colored_label(Color32::RED, "API Error");
                                }
                                _ => {
                                    ui.colored_label(Color32::RED, "API Offline");
                                }
                            };
                            if self.api_refreshed.is_none() {
                                self.api_refreshed = Some(Instant::now());
                            }
                        }
                        None => {
                            if self.api_refreshed.is_some() {
                                ui.colored_label(Color32::GREEN, "API Online");
                            } else {
                                ui.spinner();
                            }
                        }
                    };
                    egui::warn_if_debug_build(ui);
                });
                ui.with_layout(egui::Layout::right_to_left(), |ui| {
                    if ui
                        .button("Reset GUI")
                        .on_hover_text("Reset the ui in case some window bugs out.")
                        .clicked()
                    {
                        *ui.ctx().memory() = Default::default();
                    }
                    if ui.button("Debug").clicked() {
                        ui.ctx().set_debug_on_hover(!ui.ctx().debug_on_hover());
                    }
                });
            });
        });

        // Force spawning a new watcher
        if promise.ready().is_some()
            && self.api_refreshed.map(|refreshed| {
                Instant::now().duration_since(refreshed) >= Duration::from_secs(10)
            }) == Some(true)
        {
            self.api_watcher = None;
            self.api_refreshed = None;
        }
    }
}
