use crab_model::player::{AutoplayState, Interaction, LoopState, QueueState, State};
use egui::{Color32, Context, Ui};
use instant::{Duration, Instant};

use crate::{
    api::Api,
    app::{App, Page},
};

pub(crate) trait DashboardPage {
    fn dashboard_page(&mut self, ctx: &Context);
}

impl DashboardPage for App {
    fn dashboard_page(&mut self, ctx: &Context) {
        if let Some(user) = &self.user {
            if user.admin {
                egui::Window::new("Logs")
                    .resizable(true)
                    .default_height(300.0)
                    .show(ctx, |ui| {
                        let logs_loader = self
                            .logs_loader
                            .get_or_insert_with(|| self.api_endpoint.logs(&self.token));
                        match logs_loader.ready() {
                            None => {
                                ui.spinner();
                            }
                            Some(json) => {
                                let mut buffer = json.clone().unwrap_or_default();
                                ui.vertical_centered(|ui| {
                                    if ui.button("Refresh").clicked() {
                                        self.logs_loader = None;
                                    }
                                });
                                egui::ScrollArea::vertical().show(ui, |ui| {
                                    ui.with_layout(
                                        egui::Layout::top_down(egui::Align::LEFT)
                                            .with_cross_justify(true),
                                        |ui| {
                                            egui::TextEdit::multiline(&mut buffer)
                                                .code_editor()
                                                .show(ui);
                                        },
                                    );
                                });
                            }
                        }
                    });
                egui::Window::new("Data")
                    .resizable(true)
                    .default_height(300.0)
                    .show(ctx, |ui| {
                        let data_loader = self
                            .data_loader
                            .get_or_insert_with(|| self.api_endpoint.data(&self.token));
                        match data_loader.ready() {
                            None => {
                                ui.spinner();
                            }
                            Some(json) => {
                                let mut buffer = json.clone().unwrap_or_default();
                                ui.vertical_centered(|ui| {
                                    if ui.button("Refresh").clicked() {
                                        self.data_loader = None;
                                    }
                                });
                                egui::ScrollArea::vertical().show(ui, |ui| {
                                    ui.with_layout(
                                        egui::Layout::top_down(egui::Align::LEFT)
                                            .with_cross_justify(true),
                                        |ui| {
                                            egui::TextEdit::multiline(&mut buffer)
                                                .code_editor()
                                                .show(ui);
                                        },
                                    );
                                });
                            }
                        }
                    });
            }
            egui::Window::new("Player")
                .resizable(true)
                .default_height(500.0)
                .show(ctx, |ui| {
                    let do_we_need_to_refresh = self.player_state_updated.map(|updated| {
                        Instant::now().duration_since(updated) >= Duration::from_secs(5)
                    });
                    if do_we_need_to_refresh == Some(true) {
                        if let Some(guild) = self.player_selected_guild.as_ref() {
                            debug!("Loading player");
                            self.player_state =
                                Some(self.api_endpoint.player(&self.token, guild.guild_id));
                            self.player_state_updated = None;
                        }
                    }
                    if let Some(promise) = &mut self.player_state {
                        match promise.ready().cloned() {
                            Some(status_result) => {
                                if self.player_state_updated.is_none() {
                                    self.player_state_updated = Some(Instant::now());
                                }
                                self.player_cache = status_result.clone();
                                match status_result {
                                    Some(player) => {
                                        build_player_ui(self, ui, player);
                                    }
                                    None => {
                                        self.player_state = None;
                                    }
                                }
                            }
                            None => {
                                if let Some(cached_player_state) = self.player_cache.clone() {
                                    build_player_ui(self, ui, cached_player_state);
                                } else {
                                    ui.spinner();
                                }
                            }
                        }
                    } else {
                        match self
                            .available_players
                            .get_or_insert_with(|| self.api_endpoint.players(&self.token))
                            .ready()
                        {
                            Some(Some(players)) => {
                                if players.is_empty() {
                                    ui.label("No players available");
                                } else {
                                    if self.player_selected_guild.is_none() {
                                        self.player_selected_guild = Some(players[0].clone());
                                    }
                                    egui::ComboBox::from_label("Pick a guild")
                                        .selected_text(
                                            self.player_selected_guild
                                                .as_ref()
                                                .map(|guild| guild.guild_name.clone())
                                                .unwrap_or_default(),
                                        )
                                        .show_ui(ui, |ui| {
                                            for player in players {
                                                ui.selectable_value(
                                                    &mut self.player_selected_guild,
                                                    Some(player.clone()),
                                                    player.guild_name.clone(),
                                                );
                                            }
                                        });
                                }
                                if ui.button("Refresh").clicked() {
                                    self.available_players = None;
                                    self.player_selected_guild = None;
                                }
                            }
                            Some(None) => {
                                ui.colored_label(Color32::RED, "Could not load available players!");
                                if ui.button("Retry").clicked() {
                                    self.available_players = None;
                                    self.player_selected_guild = None;
                                }
                            }
                            None => {
                                ui.label("Loading available players …");
                                ui.spinner();
                            }
                        }
                        if let Some(guild) = self.player_selected_guild.as_ref() {
                            if ui.button("Load").clicked() {
                                self.player_state =
                                    Some(self.api_endpoint.player(&self.token, guild.guild_id));
                            }
                        }
                    }
                });
        } else {
            self.page = Page::Login;
        }
    }
}

fn build_player_ui(app: &mut App, ui: &mut Ui, player: State) {
    if let Some(title) = &player.title {
        ui.label(title);
    } else {
        ui.label("Nothing is playing right now.");
    }
    if let Some(artitst) = &player.artist {
        ui.label(artitst);
    }
    if let (Some(position), Some(duration)) = (&player.position, &player.duration) {
        let percentage = position.as_secs_f32() / duration.as_secs_f32();
        // debug!(percentage);
        let progress_bar = egui::ProgressBar::new(percentage);
        ui.add(progress_bar);
    }
    if let Some(guild) = app.player_selected_guild.as_ref() {
        ui.horizontal(|ui| {
            if ui.button("Prev").clicked() {
                app.player_state = Some(app.api_endpoint.player_interact(
                    &app.token,
                    guild.guild_id,
                    Interaction::Back,
                ));
                app.player_cache = None;
                app.queue_loader = None;
                app.queue_cache = None;
            }
            if ui.checkbox(&mut player.paused.clone(), "Pause").clicked() {
                app.player_state = Some(app.api_endpoint.player_interact(
                    &app.token,
                    guild.guild_id,
                    if player.paused {
                        Interaction::Resume
                    } else {
                        Interaction::Pause
                    },
                ));
                app.player_cache = None;
            };
            if ui
                .checkbox(
                    &mut (player.autoplay_state == AutoplayState::Enabled),
                    "Autoplay",
                )
                .clicked()
            {
                app.player_state = Some(app.api_endpoint.player_interact(
                    &app.token,
                    guild.guild_id,
                    match player.autoplay_state {
                        AutoplayState::Enabled => Interaction::AutoOff,
                        AutoplayState::Disabled => Interaction::Autoplay,
                    },
                ));
                app.player_cache = None;
            }
            if ui
                .checkbox(&mut (player.loop_state == LoopState::Enabled), "Loop")
                .clicked()
            {
                app.player_state = Some(app.api_endpoint.player_interact(
                    &app.token,
                    guild.guild_id,
                    match player.loop_state {
                        LoopState::Enabled => Interaction::Unloop,
                        LoopState::Disabled => Interaction::Loop,
                    },
                ));
                app.player_cache = None;
            }
            if ui.button("Next").clicked() {
                app.player_state = Some(app.api_endpoint.player_interact(
                    &app.token,
                    guild.guild_id,
                    Interaction::Next,
                ));
                app.player_cache = None;
                app.queue_loader = None;
                app.queue_cache = None;
            }
        });
        // ui.collapsing("Queue", |ui| {
        if app
            .queue_refreshed
            .map(|refreshed| Instant::now().duration_since(refreshed) >= Duration::from_secs(10))
            == Some(true)
        {
            app.queue_loader = None;
            app.queue_refreshed = None;
        }

        let queue_loader = app
            .queue_loader
            .get_or_insert_with(|| app.api_endpoint.queue(&app.token, guild.guild_id));
        match queue_loader.ready().cloned() {
            Some(maybe_queue_state) => {
                if app.queue_refreshed.is_none() {
                    app.queue_refreshed = Some(Instant::now());
                }
                app.queue_cache = maybe_queue_state.clone();
                match maybe_queue_state {
                    Some(queue_state) => build_queue_ui(app, ui, queue_state, guild.guild_id),
                    None => {
                        ui.label("Failed to load queue.");
                        if ui.button("Retry").clicked() {
                            app.queue_loader = None;
                        }
                    }
                }
            }
            None => {
                if let Some(queue_state) = app.queue_cache.clone() {
                    build_queue_ui(app, ui, queue_state, guild.guild_id)
                } else {
                    ui.spinner();
                }
            }
        }
        // });
    }
    ui.separator();
    if ui.button("Quit").clicked() {
        app.player_state = None;
        app.player_state_updated = None;
    }
}

fn build_queue_ui(app: &mut App, ui: &mut Ui, queue_state: QueueState, guild_id: u64) {
    ui.separator();
    ui.horizontal(|ui| {
        if ui
            .selectable_label(!app.queue_show_history, "Queue")
            .clicked()
        {
            app.queue_show_history = false;
        }
        if ui
            .selectable_label(app.queue_show_history, "History")
            .clicked()
        {
            app.queue_show_history = true;
        }
    });
    let height = ui.available_height() - 30.0;
    egui_extras::StripBuilder::new(ui)
        .size(egui_extras::Size::exact(height))
        .vertical(|mut strip| {
            strip.cell(|ui| {
                ui.vertical(|ui| {
                    egui_extras::TableBuilder::new(ui)
                        .column(egui_extras::Size::initial(75.0).at_least(75.0))
                        .column(egui_extras::Size::initial(10.0).at_least(5.0))
                        .column(egui_extras::Size::remainder().at_least(200.0))
                        .cell_layout(
                            egui::Layout::left_to_right().with_cross_align(egui::Align::Center),
                        )
                        .striped(true)
                        // .resizable(false)
                        // .scroll(false)
                        .body(|mut body| {
                            if app.queue_show_history {
                                for (index, track) in queue_state.played.iter().rev().enumerate() {
                                    body.row(30.0, |mut row| {
                                        row.col(|_| {});
                                        row.col(|ui| {
                                            ui.label((index + 1).to_string());
                                        });
                                        row.col(|ui| {
                                            ui.label(track.title.clone());
                                        });
                                    });
                                }
                            } else {
                                for (index, track) in queue_state.enqueued.iter().enumerate() {
                                    body.row(30.0, |mut row| {
                                        row.col(|ui| {
                                            if ui.button("❌").clicked() {
                                                app.player_state =
                                                    Some(app.api_endpoint.player_interact(
                                                        &app.token,
                                                        guild_id,
                                                        Interaction::Remove(track.id),
                                                    ));
                                                app.player_cache = None;
                                                app.queue_loader = None;
                                                app.queue_cache = None;
                                            }
                                            if index > 0 {
                                                if ui.button("⏶").clicked() {
                                                    let prev_id =
                                                        queue_state.enqueued[index - 1].id;
                                                    app.player_state =
                                                        Some(app.api_endpoint.player_interact(
                                                            &app.token,
                                                            guild_id,
                                                            Interaction::Swap(track.id, prev_id),
                                                        ));
                                                    app.player_cache = None;
                                                    app.queue_loader = None;
                                                    app.queue_cache = None;
                                                }
                                            }
                                            if index < queue_state.enqueued.len() - 1 {
                                                if ui.button("⏷").clicked() {
                                                    let next_id =
                                                        queue_state.enqueued[index + 1].id;
                                                    app.player_state =
                                                        Some(app.api_endpoint.player_interact(
                                                            &app.token,
                                                            guild_id,
                                                            Interaction::Swap(track.id, next_id),
                                                        ));
                                                    app.player_cache = None;
                                                    app.queue_loader = None;
                                                    app.queue_cache = None;
                                                }
                                            }
                                        });
                                        row.col(|ui| {
                                            ui.label((index + 1).to_string());
                                        });
                                        row.col(|ui| {
                                            ui.label(track.title.clone());
                                        });
                                    });
                                }
                            }
                        });
                });
            });
        });
}
