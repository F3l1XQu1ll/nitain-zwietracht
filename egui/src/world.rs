use crab_model::warframe::{WFCycle, WFWorld};
use egui::{Context, RichText, Ui};
use egui_extras::Size;
use instant::{Duration, Instant};

use crate::{
    api::Api,
    app::App,
    utils::{CountdownLabel, CycleEmote},
};

pub(crate) trait WorldPage {
    fn world_page(&mut self, ctx: &Context, ui: &mut Ui);
}

impl WorldPage for App {
    fn world_page(&mut self, ctx: &Context, ui: &mut Ui) {
        let world = self
            .world_loader
            .get_or_insert_with(|| self.api_endpoint.world());

        match world.ready() {
            Some(world) => {
                match world {
                    Some(world) => {
                        arbitration(ctx, world);

                        {
                            egui::Window::new("Cambion Drift").show(ctx, |ui| {
                                let cambion = world.cycles().necralisk;
                                cycle_window(ui, cambion);
                            });

                            egui::Window::new("Orb Vallis").show(ctx, |ui| {
                                cycle_window(ui, world.cycles().fortuna);
                            });

                            egui::Window::new("Plains of Eidolon").show(ctx, |ui| {
                                cycle_window(ui, world.cycles().cetus);
                            });

                            egui::Window::new("Earth Forrests").show(ctx, |ui| {
                                cycle_window(ui, world.cycles().earth);
                            });
                        }

                        baro(ctx, world);

                        {
                            let fissure_types = vec![
                                ("Lith", 1),
                                ("Meso", 2),
                                ("Neo", 3),
                                ("Axi", 4),
                                ("Requiem", 5),
                            ];
                            egui::Window::new("Void Fissures").show(ctx, |ui| {
                                for (fissure_type, tier) in &fissure_types {
                                    make_fissure_table(ui, world, fissure_type, tier, false);
                                }
                            });
                            egui::Window::new("Void Storms").show(ctx, |ui| {
                                for (fissure_type, tier) in &fissure_types {
                                    if tier == &5 {
                                        continue;
                                    }
                                    make_fissure_table(ui, world, fissure_type, tier, true);
                                }
                            });
                        }
                    }
                    None => {
                        egui::Window::new("Error")
                            .anchor(egui::Align2::CENTER_CENTER, egui::Vec2::ZERO)
                            .resizable(false)
                            .collapsible(false)
                            .show(ctx, |ui| {
                                ui.vertical_centered_justified(|ui| {
                                    ui.label("Could not load worldstate");
                                })
                            });
                    }
                }
                if self.world_refreshed.is_none() {
                    self.world_refreshed = Some(Instant::now());
                }
            }
            None => {
                ui.spinner();
            }
        }

        if world.ready().is_some()
            && self.world_refreshed.map(|refreshed| {
                Instant::now().duration_since(refreshed) >= Duration::from_secs(20)
            }) == Some(true)
        {
            self.world_loader = None;
            self.world_refreshed = None;
        }
    }
}

fn arbitration(ctx: &Context, world: &WFWorld) {
    egui::Window::new("Arbitration").show(ctx, |ui| {
        let arbitration = world.arbitration();
        ui.vertical(|ui| {
            ui.label(arbitration.node);
            ui.horizontal(|ui| {
                ui.label(arbitration.enemy.to_string());
                ui.label(
                    arbitration
                        .mission_type
                        .unwrap_or_else(|| "No mission type".to_string()),
                );
            });
            ui.countdown(arbitration.expiry.unwrap())
        })
    });
}

fn cycle_window(ui: &mut Ui, cycle: WFCycle) {
    ui.vertical(|ui| {
        ui.horizontal(|ui| {
            ui.label(cycle.active.emote());
            ui.label(RichText::new(cycle.active.to_string()).size(48.0));
        });
        ui.countdown(cycle.expiry.unwrap());
    });
}

fn baro(ctx: &Context, world: &WFWorld) {
    egui::Window::new("Baro Ki'Teer")
        .default_height(300.0)
        .show(ctx, |ui| {
            let baro = world.baro();
            if baro.active {
                ui.label(
                    baro.location
                        .unwrap_or_else(|| "Unknown Location".to_string()),
                );
                ui.collapsing("Inventory", |ui| {
                    let inventory = baro.inventory;
                    egui_extras::TableBuilder::new(ui)
                        .column(Size::remainder().at_least(100.0))
                        .column(Size::remainder().at_least(10.0))
                        .column(Size::remainder())
                        .body(|mut body| {
                            for item in inventory {
                                body.row(30.0, |mut row| {
                                    row.col(|ui| {
                                        ui.label(item.item);
                                    });
                                    row.col(|ui| {
                                        ui.label(format!("{:>3}", item.ducats));
                                    });
                                    row.col(|ui| {
                                        ui.label(format!("{:>4}", item.credits));
                                    });
                                });
                            }
                        });
                });
                ui.countdown(baro.expiry.unwrap());
            } else {
                ui.countdown(baro.activation.unwrap());
                ui.label("until arrival");
                ui.label(format!("at {}", baro.location.unwrap()));
            }
        });
}

fn make_fissure_table(
    ui: &mut Ui,
    world: &WFWorld,
    header: &'static str,
    tier_num: &u32,
    storm: bool,
) {
    egui::CollapsingHeader::new(header)
        .default_open(true)
        .show(ui, |ui| {
            egui_extras::TableBuilder::new(ui)
                .scroll(false)
                .striped(true)
                .column(Size::remainder().at_least(80.0))
                .column(Size::remainder().at_least(160.0))
                .column(Size::remainder().at_least(80.0))
                .column(Size::remainder().at_least(100.0))
                .body(|mut body| {
                    for fissure in world.fissures() {
                        if fissure.is_storm == storm && &fissure.tier_num == tier_num {
                            body.row(30.0, |mut row| {
                                row.col(|ui| {
                                    ui.label(fissure.mission_type);
                                });
                                row.col(|ui| {
                                    ui.label(fissure.node);
                                });
                                row.col(|ui| {
                                    ui.label(fissure.enemy.to_string());
                                });
                                row.col(|ui| {
                                    ui.countdown(fissure.expiry.unwrap());
                                });
                            })
                        }
                    }
                })
        });
}
