use std::fmt::Display;

use crab_model::{
    auth::UserRecord,
    player::{AvailableGuild, Interaction, QueueState, State},
    warframe::WFWorld,
};
use ehttp::Request;
use poll_promise::Promise;
use serde::{de::DeserializeOwned, Deserialize, Serialize};

pub(crate) trait Api {
    fn nitain(&self) -> Promise<Option<ApiState>> {
        self.request(format!("/v1/nitain"), None, |text| match text {
            "Crab" => Some(ApiState::Online),
            _ => Some(ApiState::Error),
        })
    }

    fn auth<'a>(&self, token: &'a str) -> Promise<Option<UserRecord>> {
        self.request_parsed(format!("/v1/auth?token={token}"), None) // auth is special for now as token is still a paramter
    }

    fn world(&self) -> Promise<Option<WFWorld>> {
        self.request_parsed("/v1/world", None)
    }

    fn logs<'a>(&self, token: &'a str) -> Promise<Option<String>> {
        self.request_json_pretty(format!("/v1/logs"), Some(token))
    }

    fn data<'a>(&self, token: &'a str) -> Promise<Option<String>> {
        self.request_json_pretty(format!("/v1/data"), Some(token))
    }

    fn player<'a, ID: Display>(&self, token: &'a str, guild_id: ID) -> Promise<Option<State>> {
        self.request_parsed(format!("/v1/{guild_id}/player"), Some(token))
    }

    fn players<'a>(&self, token: &'a str) -> Promise<Option<Vec<AvailableGuild>>> {
        self.request_parsed(format!("/v1/me/players"), Some(token))
    }

    fn player_interact<'a, ID: Display>(
        &self,
        token: &'a str,
        guild_id: ID,
        interaction: Interaction,
    ) -> Promise<Option<State>> {
        let id_param = match interaction {
            Interaction::Remove(id) => format!("?id={id}"),
            Interaction::Swap(id, other) => format!("?id={id}&other_id={other}"),
            _ => "".to_string(),
        };
        let interaction = interaction.as_request();
        self.request_parsed(
            format!("/v1/{guild_id}/player/{interaction}{id_param}",),
            Some(token),
        )
    }

    fn queue<'a, ID: Display>(&self, token: &'a str, guild_id: ID) -> Promise<Option<QueueState>> {
        self.request_parsed(format!("/v1/{guild_id}/queue"), Some(token))
    }

    /// Perform an http get (`url`) request, apply a function (`parser`) on the text result of that request.
    ///
    /// Returns a promise for the final result
    fn request<
        'a,
        S: Into<String>,
        F: FnOnce(&str) -> Option<T> + Send + 'static,
        T: Send + Clone,
    >(
        &self,
        url: S,
        token: Option<&'a str>,
        parser: F,
    ) -> Promise<Option<T>> {
        let (sender, promise) = Promise::new();
        let mut request = Request::get(self.base().clone() + url.into().as_str());
        if let Some(token) = token {
            request.headers.insert("token".to_owned(), token.into());
        }
        ehttp::fetch(request, |res| {
            sender.send(res.ok().and_then(|res| res.text().and_then(parser)));
        });
        promise
    }

    /// Perform a get request to `url` and parse the result into `T`
    fn request_parsed<'a, S: Into<String>, T: Send + DeserializeOwned + Clone>(
        &self,
        url: S,
        token: Option<&'a str>,
    ) -> Promise<Option<T>> {
        self.request(url, token, |text| {
            serde_json::from_str::<T>(text.clone())
                .map_err(|e| error!("{e:#?}"))
                .ok()
                .clone()
        })
    }

    /// Perfor a get request to `url` and parse the result a pretty-printed json
    fn request_json_pretty<'a, S: Into<String>>(
        &self,
        url: S,
        token: Option<&'a str>,
    ) -> Promise<Option<String>> {
        self.request(url, token, |text| {
            serde_json::from_str(text.clone())
                .ok()
                .and_then(|value: serde_json::Value| serde_json::to_string_pretty(&value).ok())
        })
    }

    fn base(&self) -> &String;
    fn base_mut(&mut self) -> &mut String;
}

#[derive(Clone)]
pub(crate) enum ApiState {
    Online,
    Error,
}

#[non_exhaustive] // Don't instance this manually
#[derive(Deserialize, Serialize, PartialEq, Clone)]
pub(crate) struct ApiEndpoint {
    title: String,
    base: String,
}

impl ApiEndpoint {
    pub fn nitain_crab() -> Self {
        Self {
            title: "NitainCrab".to_owned(),
            base: "https://nitain-crab.herokuapp.com/".to_string(),
        }
    }

    pub fn void_crab() -> Self {
        Self {
            title: "NitainCrab (2)".to_owned(),
            base: "https://void-crab.herokuapp.com/".to_string(),
        }
    }

    pub fn custom(base: String) -> Self {
        Self {
            title: "Custom".to_owned(),
            base,
        }
    }
}

impl Api for ApiEndpoint {
    fn base(&self) -> &String {
        &self.base
    }

    fn base_mut(&mut self) -> &mut String {
        &mut self.base
    }
}
