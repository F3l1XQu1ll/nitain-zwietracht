mod api;
mod app;
mod dashboard;
mod login;
mod utils;
mod world;

#[macro_use]
extern crate tracing;

#[cfg(target_arch = "wasm32")]
fn main() {
    // Make sure panics are logged using `console.error`.
    console_error_panic_hook::set_once();

    // Redirect tracing to console.log and friends:
    tracing_wasm::set_as_global_default();

    eframe::start_web(
        "the_canvas_id", // hardcode it
        Box::new(|cc| Box::new(app::App::new(cc))),
    )
    .expect("failed to start eframe");
}
