use crab_model::{
    chrono::{DateTime, Local, TimeZone},
    warframe::WFCycleActive,
};
use egui::{Color32, Response, RichText, Ui};

pub(crate) trait CountdownLabel {
    fn countdown<T: TimeZone>(&mut self, eta: DateTime<T>) -> Response;
}

impl CountdownLabel for Ui {
    fn countdown<T: TimeZone>(&mut self, eta: DateTime<T>) -> Response {
        let time_difference = eta.signed_duration_since(Local::now());
        let seconds = time_difference.num_seconds() % 60;
        let minutes = (time_difference.num_seconds() / 60) % 60;
        let hours = ((time_difference.num_seconds() / 60) / 60) % 24;
        let days = ((time_difference.num_seconds() / 60) / 60) / 24;

        let zero_str = |num: i64, ext: &str| {
            if num != 0 {
                format!(" {num:>2}{ext}")
            } else {
                "".to_string()
            }
        };

        let days_str = zero_str(days, "d");
        let hours_str = zero_str(hours, "h");
        let mins_str = zero_str(minutes, "m");
        let secs_str = zero_str(seconds, "s");

        self.label(format!("ETA:{days_str}{hours_str}{mins_str}{secs_str}"))
    }
}

pub(crate) trait CycleEmote {
    fn emote(&self) -> RichText;
}

impl CycleEmote for WFCycleActive {
    fn emote(&self) -> RichText {
        let (symbol, color) = match self {
            WFCycleActive::Fass => ("☀", Color32::GOLD),
            WFCycleActive::Vome => ("🌙", Color32::LIGHT_BLUE),
            WFCycleActive::Cold => ("❄", Color32::LIGHT_BLUE),
            WFCycleActive::Warm => ("☀", Color32::GOLD),
            WFCycleActive::Night => ("🌙", Color32::LIGHT_BLUE),
            WFCycleActive::Day => ("☀", Color32::GOLD),
        };
        RichText::new(symbol).size(48.0).color(color)
    }
}
