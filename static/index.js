function reveal_element(element_id, visibility) {
    document.getElementById(element_id).style.visibility = visibility;
}

function parse_time(time_str) {
    return Date.parse(time_str);
}

/** @param {Date} eta */
function eta_string(eta) {
    const days = ("0" + Math.floor(Math.abs(eta / 1000) / (60 * 60 * 24)).toString()).slice(-2);
    const hours = ("0" + eta.getUTCHours()).slice(-2);
    const minutes = ("0" + eta.getUTCMinutes()).slice(-2);
    const seconds = ("0" + eta.getUTCSeconds()).slice(-2);
    return (days !== "00" ? days + "d " : "") +
        (hours !== "00" ? hours + "h " : "") +
        (minutes !== "00" ? minutes + "m " : "") +
        seconds + "s";
}

function time_eta(time_str) {
    const time = parse_time(time_str);
    const now = Date.now();
    if (now < time) {
        const eta = new Date(time - now);
        return eta_string(eta);
    } else {
        const eta = new Date(now - time);
        return "Expired -" + eta_string(eta);
    }

}

function update_etas() {
    const etas = document.getElementsByClassName('eta');
    for (const element of etas) {
        const expiry = element.getAttribute('expiry');
        const eta = time_eta(expiry);
        element.innerHTML = eta;
    }
}

// Update etas every second
setInterval(update_etas, 1 * 1000);

// run this on_load to refresh the page with the given interval
function reload_page_timeout(timeout_secs) {
    setTimeout(function () {
        window.location.reload();
    }, timeout_secs * 1000);
}