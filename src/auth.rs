use crate::{
    commands::CommandContext,
    error::{ErrorContext, LogError},
    message_box::CrabMessage,
    storage::manager::ContextStorageManager,
};
use anyhow::Result;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
#[serde(default)]
pub struct CrabToken {
    token: String,
    admin: bool,
}

impl CrabToken {
    pub fn token(&self) -> String {
        self.token.clone()
    }

    /// Get a reference to the crab token's admin value.
    pub fn is_admin(&self) -> bool {
        self.admin
    }

    pub fn user(&self) -> Option<u64> {
        self.token.split_once(';')?.0.parse::<u64>().ok()
    }
}

async fn gen_token(ctx: &CommandContext, admin: bool) -> Result<CrabToken> {
    let mut storage = ctx.storage().await;
    let user = ctx.user.id.as_u64();

    let secret: String = thread_rng()
        .sample_iter(Alphanumeric)
        .take(20)
        .map(char::from)
        .collect();

    let token = user.to_string() + ";" + &secret;
    let crab_token = CrabToken { token, admin };

    storage
        .update(|mut write| {
            write
                .config_mut()
                .tokens_mut()
                .insert(*user, crab_token.clone());
        })
        .await?;
    Ok(crab_token)
}

pub async fn token(ctx: &CommandContext) {
    let user_id = *ctx.user.id.as_u64();
    let admin = user_id == 413425902077345792 || user_id == 416264334100922368;
    match gen_token(ctx, admin).await {
        Ok(token) => {
            if let Ok(user_channel) = ctx.user.create_dm_channel(&ctx.ctx()).await {
                CrabMessage::build(
                    ctx.ctx().shard.clone(),
                    ctx.ctx().http.clone(),
                    user_channel.id,
                )
                .title("Your token:")
                .description(base64::encode_config(
                    token.token(),
                    base64::STANDARD_NO_PAD, // Make copying easier by stripping off the ocasional '=='
                ))
                .mention(|m| m.replied_user(true)) // TODO: Find a way to make this work
                .message()
                .await
                .context("Failed to send token message!")
                .handle();
            }
        }
        Err(e) => {
            eprintln!("{e}");
        }
    }
}
