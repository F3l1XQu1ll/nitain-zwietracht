use rocket_lib::{http::Status, Shutdown, State};

use crate::ShutdownFlag;

#[get("/shutdown")]
pub fn shutdown(shutdown: Shutdown, flag: &State<ShutdownFlag>) -> Status {
    if flag.0.load(std::sync::atomic::Ordering::Acquire) {
        shutdown.notify();
        info!("Notifyed rocket to shutdown");
        Status::Ok
    } else {
        info!("Shutdown request not allowed");
        Status::Forbidden
    }
}
