use crate::{auth::CrabToken, storage::crab_data::CrabData};

use rocket_cors::{AllowedHeaders, AllowedOrigins, Cors, CorsOptions};
use rocket_lib::http::Method;

pub mod api;
pub mod control;
pub mod ping;

pub async fn get_record(token: &str, data: &CrabData) -> Option<CrabToken> {
    let decoded =
        String::from_utf8(base64::decode_config(token, base64::STANDARD_NO_PAD).ok()?).ok()?;
    let user = decoded.split_once(';')?.0;
    data.config()
        .tokens()
        .get(&user.parse::<u64>().ok()?)
        .cloned()
}

pub fn cors() -> Cors {
    let allowed_origins = AllowedOrigins::all();
    let cors_options = CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::some(&[
            "Authorization",
            "Accept",
            "Access-Control-Allow-Origin",
            "token",
        ]),
        allow_credentials: true,
        ..Default::default()
    };
    cors_options.to_cors().expect("error while building CORS!")
}
