use futures::StreamExt;
use rocket_lib::{
    http::Status,
    request::{FromRequest, Outcome},
    response::status::{Custom, Forbidden, NotFound},
    serde::json::Json,
    Request, State,
};
use rocket_okapi::{
    okapi::openapi3::{Object, SecurityRequirement, SecurityScheme},
    openapi,
    request::{OpenApiFromRequest, RequestHeaderInput},
};

use std::{io::Read, sync::Arc};
use tokio::sync::RwLock;

use serenity::{http::Http, model::prelude::Member};

use crate::{
    auth::CrabToken,
    warframe::api::{InsertMissionTypeFromNodeStr, WFWorld},
};
use crate::{controller::CrabController, rocket::get_record};
use crate::{CrabData, LogBuffer};

#[rocket_lib::async_trait]
impl<'r> FromRequest<'r> for CrabToken {
    type Error = ();
    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let keys: Vec<_> = request.headers().get("token").collect();
        match request.guard::<&State<Arc<RwLock<CrabData>>>>().await {
            Outcome::Success(state) => {
                let data = { state.read().await.clone() };
                match keys.len() {
                    0 => Outcome::Failure((Status::Forbidden, ())),
                    1 => match get_record(keys[0], &data).await {
                        Some(token) => Outcome::Success(token),
                        None => Outcome::Failure((Status::Forbidden, ())),
                    },
                    _ => Outcome::Failure((Status::BadRequest, ())),
                }
            }
            Outcome::Forward(forward) => Outcome::Forward(forward),
            Outcome::Failure(failure) => Outcome::Failure(failure),
        }
    }
}

impl<'a> OpenApiFromRequest<'a> for CrabToken {
    fn from_request_input(
        _gen: &mut rocket_okapi::gen::OpenApiGenerator,
        _name: String,
        _required: bool,
    ) -> rocket_okapi::Result<rocket_okapi::request::RequestHeaderInput> {
        let sheme = SecurityScheme {
            description: Some("Requires a valid token to access, key is: `token`.".to_owned()),
            data: rocket_okapi::okapi::openapi3::SecuritySchemeData::ApiKey {
                name: "token".to_owned(),
                location: "header".to_owned(),
            },
            extensions: Object::default(),
        };
        let mut req = SecurityRequirement::new();
        let sheme_name = "ApiKeyAuth";
        req.insert(sheme_name.to_owned(), Vec::new());
        Ok(RequestHeaderInput::Security(
            sheme_name.to_owned(),
            sheme,
            req,
        ))
    }
}

#[openapi(tag = "Nitain")]
#[get("/nitain")]
pub fn nitain() -> String {
    "Crab".to_string()
}

#[openapi(tag = "World")]
#[get("/world")]
pub async fn world() -> Result<Json<WFWorld>, Custom<String>> {
    let api = crate::warframe::api::WFApi::new();
    match api.world().await {
        Ok(mut world) => Ok(Json({
            world
                .arbitration_mut()
                .insert_mission_type()
                .await
                .map_err(|e| Custom(Status::InternalServerError, e.to_string()))?;
            world
        })),
        Err(e) => Err(Custom(Status::InternalServerError, e.to_string())),
    }
}

#[openapi(tag = "Data")]
#[get("/data")]
/// The returned data is a pretty-printed json-string generated from CrabData.
///
/// As CrabData is not included in crab_model,
/// it is also not planned to build a proper interface.
///
/// This api-endpoint solely serves for debugging purposes.
pub async fn data(
    token: CrabToken,
    data: &State<Arc<RwLock<CrabData>>>,
) -> Result<Json<serde_json::Value>, Forbidden<()>> {
    let data_read = { data.read().await.clone() };
    debug!("{token:?}");
    let admin = token.is_admin();
    if admin {
        Ok(Json(serde_json::to_value(&data_read).unwrap_or_default()))
    } else {
        Err(Forbidden(None))
    }
}

#[openapi(tag = "Logs")]
#[get("/logs")]
/// Runtime collected Logs
///
/// Similar to the data endpoint this is only for debugging purposes.
/// Also, due to the implementation of the LogBuffer,
/// it is not possible to provide a proper model
pub async fn logs(
    token: CrabToken,
    logs: &State<LogBuffer>,
) -> Result<Json<serde_json::Value>, Forbidden<()>> {
    let admin = token.is_admin();
    if admin {
        // no idea, why rustfmt insists on the space
        let log_read = logs.0 .0.read().unwrap();
        // get a &Vec<u8> from our lock, deref to get [u8] and reborrow for &[u8]
        let mut log_buffer: &[u8] = log_read.as_ref();
        let mut log_str = String::new();
        // we can read from &[u8], write to Vec<u8>
        log_buffer.read_to_string(&mut log_str).unwrap();
        // pretty output
        log_str = format!(
            "[{}]",
            log_str.replacen('\n', ",\n", log_str.lines().count() - 1)
        );
        Ok(Json(serde_json::from_str(&log_str).unwrap_or_default()))
    } else {
        Err(Forbidden(None))
    }
}

#[openapi(tag = "Authenticate")]
#[get("/auth?<token>")]
pub async fn authenticate(
    token: &str,
    data: &State<Arc<RwLock<CrabData>>>,
    http: &State<Arc<Http>>,
) -> Result<Json<crab_model::auth::UserRecord>, NotFound<()>> {
    let data = { data.read().await.clone() };
    let record = get_record(token, &data).await.ok_or(NotFound(()))?;
    let user = http
        .get_user(record.user().ok_or(NotFound(()))?)
        .await
        .map_err(|_| NotFound(()))?;
    let name = user.name.clone();
    let id = user.discriminator;
    let profile_img = user.avatar_url();
    let admin = record.is_admin();

    Ok(Json(crab_model::auth::UserRecord {
        name,
        id,
        profile_img,
        admin,
    }))
}

#[openapi(tag = "Player")]
#[get("/<guild_id>/player")]
pub async fn player(
    token: CrabToken,
    http: &State<Arc<Http>>,
    guild_id: u64,
    controller: &State<CrabController>,
) -> Result<Json<crab_model::player::State>, Forbidden<()>> {
    // let user = http.get_user(user_id).await.map_err(|_| Forbidden(None))?;
    if user_in_guild(http, token, guild_id).await.is_ok() {
        let player_state = controller.player_info(guild_id.into()).await;
        Ok(Json(player_state))
    } else {
        Err(Forbidden(None))
    }
}

#[openapi(tag = "Queue")]
#[get("/<guild_id>/queue")]
pub async fn queue(
    token: CrabToken,
    http: &State<Arc<Http>>,
    guild_id: u64,
    controller: &State<CrabController>,
) -> Result<Json<crab_model::player::QueueState>, Forbidden<()>> {
    if user_in_guild(http, token, guild_id).await.is_ok() {
        let queue_state = controller.queue_info(guild_id.into()).await;
        Ok(Json(queue_state))
    } else {
        Err(Forbidden(None))
    }
}

use crab_model::{player::AvailableGuild, schemars};
#[derive(rocket_okapi::JsonSchema)]
/// Wrapper for PlayerInteraction, to avoid pulling in the rocket dependency
pub enum PlayerInteraction {
    Resume,
    Pause,
    Back,
    Next,
    Autoplay,
    AutoOff,
    Loop,
    Unloop,
    Remove(u64),
    Swap(u64, u64),
}

impl From<crab_model::player::Interaction> for PlayerInteraction {
    fn from(val: crab_model::player::Interaction) -> Self {
        match val {
            crab_model::player::Interaction::Autoplay => Self::Autoplay,
            crab_model::player::Interaction::Resume => Self::Resume,
            crab_model::player::Interaction::Pause => Self::Pause,
            crab_model::player::Interaction::Back => Self::Back,
            crab_model::player::Interaction::Next => Self::Next,
            crab_model::player::Interaction::Loop => Self::Loop,
            crab_model::player::Interaction::Unloop => Self::Unloop,
            crab_model::player::Interaction::AutoOff => Self::AutoOff,
            crab_model::player::Interaction::Remove(id) => Self::Remove(id),
            crab_model::player::Interaction::Swap(id, other) => Self::Swap(id, other),
        }
    }
}

impl From<PlayerInteraction> for crab_model::player::Interaction {
    fn from(val: PlayerInteraction) -> Self {
        match val {
            PlayerInteraction::Resume => crab_model::player::Interaction::Resume,
            PlayerInteraction::Pause => crab_model::player::Interaction::Pause,
            PlayerInteraction::Back => crab_model::player::Interaction::Back,
            PlayerInteraction::Next => crab_model::player::Interaction::Next,
            PlayerInteraction::Autoplay => crab_model::player::Interaction::Autoplay,
            PlayerInteraction::AutoOff => crab_model::player::Interaction::AutoOff,
            PlayerInteraction::Loop => crab_model::player::Interaction::Loop,
            PlayerInteraction::Unloop => crab_model::player::Interaction::Unloop,
            PlayerInteraction::Remove(id) => crab_model::player::Interaction::Remove(id),
            PlayerInteraction::Swap(id, other) => crab_model::player::Interaction::Swap(id, other),
        }
    }
}

impl<'r> rocket_lib::request::FromParam<'r> for PlayerInteraction {
    type Error = &'r str;

    fn from_param(param: &'r str) -> Result<Self, Self::Error> {
        match param.parse::<crab_model::player::Interaction>() {
            Ok(interaction) => Ok(interaction.into()),
            Err(_) => Err(param),
        }
    }
}

#[openapi(tag = "Player Interactions")]
#[get("/<guild_id>/player/<interaction>?<id>&<other_id>")]
pub async fn player_pause(
    token: CrabToken,
    http: &State<Arc<Http>>,
    guild_id: u64,
    interaction: PlayerInteraction,
    id: Option<u64>,
    other_id: Option<u64>,
    controller: &State<CrabController>,
) -> Result<Json<crab_model::player::State>, Forbidden<()>> {
    let interaction = match interaction {
        PlayerInteraction::Remove(0) => {
            if let Some(id) = id {
                PlayerInteraction::Remove(id)
            } else {
                return Err(Forbidden(None));
            }
        }
        PlayerInteraction::Swap(0, 0) => {
            if let (Some(id), Some(other)) = (id, other_id) {
                PlayerInteraction::Swap(id, other)
            } else {
                return Err(Forbidden(None));
            }
        }
        other => other,
    };
    if user_in_guild(http, token.clone(), guild_id).await.is_ok() {
        controller
            .player_interaction(guild_id.into(), interaction.into())
            .await;
        player(token, http, guild_id, controller).await
    } else {
        Err(Forbidden(None))
    }
}

#[openapi(tag = "User available players")]
#[get("/me/players")]
pub async fn user_available_players(
    token: CrabToken,
    data: &State<Arc<RwLock<CrabData>>>,
    http: &State<Arc<Http>>,
) -> Result<Json<Vec<AvailableGuild>>, Forbidden<()>> {
    let data_read = { data.read().await.clone() };
    let playing_in: Vec<_> = data_read
        .kv()
        .iter()
        .filter(|(_id, guild)| guild.playing_in.is_some())
        .map(|(id, _)| *id)
        .collect();
    debug!("{playing_in:?}");
    let stream = futures::stream::iter(playing_in);
    let visible_guilds = stream
        .filter(|guild_id| {
            let guild_id = *guild_id;
            let token = token.clone();
            async move { user_in_guild(http, token, guild_id).await.is_ok() }
        })
        .filter_map(|guild_id| async move { http.get_guild(guild_id).await.ok() })
        .map(|guild| AvailableGuild {
            guild_id: guild.id.0,
            guild_name: guild.name,
        })
        .collect()
        .await;
    debug!("{visible_guilds:?}");
    Ok(Json(visible_guilds))
}

async fn user_in_guild(
    http: &State<Arc<Http>>,
    record: CrabToken,
    guild_id: u64,
) -> Result<Member, Forbidden<()>> {
    http.get_member(guild_id, record.user().ok_or(Forbidden(None))?)
        .await
        .map_err(|_| Forbidden(None))
}
