use crate::{
    commands::CommandContext,
    error::{CommandHandleError, CommandHandleResult},
    message_box::CrabMessage,
    msg_send,
};

const GITLAB_PROJECT_ID: &str = "27756809";
const GITLAB_ROOT: &str = "https://gitlab.com/api/v4/projects/";

pub async fn new_issue(ctx: &CommandContext) -> CommandHandleResult {
    let mut args = ctx.args();
    if args.len() == 1 {
        if let Some(title) = args.quoted().current() {
            let quotes_count = title.matches('\"').count();
            if quotes_count == 0 {
                if let Ok(gitlab_token) = std::env::var("GITLAB_ISSUE_TOKEN") {
                    if let Ok(client) = reqwest::Client::builder().build() {
                        let url = GITLAB_ROOT.to_string()
                            + GITLAB_PROJECT_ID
                            + "/issues?title="
                            + urlencoding::encode(title).to_string().as_str()
                            + "&labels=discord";
                        let response = client
                            .post(url)
                            .header("PRIVATE-TOKEN", gitlab_token)
                            .send()
                            .await;
                        if let Err(e) = response {
                            eprintln!("{e}");
                            Err(anyhow::anyhow!("Unexpected response from gitlab.").into())
                        } else {
                            CrabMessage::from_ctx(ctx).title("Issue created").description("Issue opened at [GitLab](https://gitlab.com/F3l1XQu1ll/nitain-zwietracht/-/issues?sort=created_date&state=opened)").reference(ctx.ref_msg()).message().await?;
                            Ok(())
                        }
                    } else {
                        eprintln!("Failed to create client!");
                        Err(anyhow::anyhow!("Failed to create issue.")
                            .context("Failed to create client")
                            .into())
                    }
                } else {
                    eprintln!("Gitlab token not found!");
                    Err(anyhow!("Failed to create issue.")
                        .context("Gitlab token not found!")
                        .into())
                }
            } else {
                msg_send!(ctx, "The issue title must not contain quotes!");
                Err(CommandHandleError::Custom {
                    message: "The issue title must not contain quotes!".to_string(),
                })
            }
        } else {
            Err(CommandHandleError::Custom {
                message:
                    "Please supply a title for the issue, like this +issue \"*issue title here*\""
                        .to_string(),
            })
        }
    } else {
        Err(CommandHandleError::Custom {
            message: "Please sourround the issue title with quotes.".to_string(),
        })
    }
}
