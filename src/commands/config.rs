use crate::auth;
use crate::commands::ALLOWED_CHECK;
use crate::utils;
use crab_proc::{crab_command, crab_group};
use serenity::{
    client::Context,
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::channel::Message,
};

// #[group]
// #[commands(allow, token)]

#[crab_group(allow, token)]
struct Config;

crab_command!(allow, #[only_in(guilds)], @call_args utils::allow_handler);

crab_command!(token, #[checks(allowed)], @call auth::token);
