use crate::commands::ALLOWED_CHECK;
use crate::dota;
use crab_proc::{crab_command, crab_group};
use serenity::{
    client::Context,
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::channel::Message,
};

// #[group]
// #[commands(dota_conter, dota_last_match, dota_match, dota_profile, register_dota)]
#[crab_group(
    dota_conter,
    dota_last_match,
    dota_match,
    dota_profile,
    register_dota,
    dota_pro,
    dota_follow,
    dota_unfollow
)]
struct Dota;

crab_command!(dota_conter, #[aliases(dc)], #[checks(allowed)], @call_args dota::displ_conters);

crab_command!(dota_last_match, #[aliases(dlm)], #[checks(allowed)], @call dota::displ_last_match);

crab_command!(dota_match, #[aliases(dm)], #[checks(allowed)], @call_args dota::displ_match);

crab_command!(dota_profile, #[aliases(dp)], #[checks(allowed)], @call dota::displ_profile);

crab_command!(register_dota, #[checks(allowed)], @call_args dota::register_dota);

crab_command!(dota_pro, #[checks(allowed)], #[aliases(dpro)], @call_args dota::pro_search);

crab_command!(dota_follow, #[checks(allowed)], #[aliases(dfollow)], @call_args dota::watch_pro);

crab_command!(dota_unfollow, #[checks(allowed)], #[aliases(dunfollow)], @call_args dota::unwatch_pro);
