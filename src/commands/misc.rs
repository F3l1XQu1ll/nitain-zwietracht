use crate::commands::ALLOWED_CHECK;
use crate::error::CommandHandleResult;
use crate::{issues, voting};
use crab_proc::{crab_command, crab_group};
use serenity::{
    client::Context,
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::channel::Message,
};
use std::time::Instant;

use super::CommandContext;

// #[group]
// #[commands(issue, ping, vote)]
// struct Misc;

//crab_group!(Misc, issue, ping, vote);
#[crab_group(issue, ping, vote)]
struct Misc;

crab_command!(issue, #[checks(allowed)], @call_args issues::new_issue);

crab_command!(ping, #[checks(allowed)], @call ping_handler);

crab_command!(vote, #[aliases(voting, poll)], #[only_in(guilds)], #[checks(allowed)], @call_args voting::vote);

async fn ping_handler(ctx: &CommandContext) -> CommandHandleResult {
    let before = Instant::now();
    let mut m = ctx
        .channel_id
        .say(&ctx.ctx.http, "pong!")
        .await
        .map_err(anyhow::Error::from)?;
    let after = Instant::now();
    let content = m.content.clone();
    m.edit(ctx.ctx(), |m| {
        m.content(format!("{} - {}ms", content, (after - before).as_millis()))
    })
    .await
    .map_err(anyhow::Error::from)?;
    Ok(())
}

/*
#[command]
pub async fn register_steam(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    allowed!(ctx, msg, {
        let user_id = msg.author.id;
        if let Ok(steam_id) = args.message().parse() {
            let result = register_by_steam(ctx, user_id, steam_id).await;
            msg_send!(ctx, msg, "Registered id ".to_owned() + &result.to_string());
        } else {
            msg_send!(ctx, msg, "Please supply a valid steam id!");
        }
    })
}
*/
