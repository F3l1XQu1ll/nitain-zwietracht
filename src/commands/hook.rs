use crate::{
    commands::CommandContext, message_box::CommandHandler, utils::check_channel_allowed,
    warframe::prices::try_load_priceinfo,
};
use serenity::{
    client::Context,
    framework::standard::{macros::hook, Args, Delimiter},
    model::channel::Message,
};

#[hook]
pub async fn unknown_command(ctx: &Context, msg: &Message, unknown_command_name: &str) {
    if check_channel_allowed(ctx, msg).await {
        debug!(
            "Unknown command {:?} entered, treating as request for price info",
            unknown_command_name
        );
        let handler = CommandHandler::new(
            ctx.http.clone(),
            ctx.shard.clone(),
            msg.channel_id,
            Some(msg),
        );
        let context = CommandContext::new(
            ctx.clone(),
            msg.guild(&ctx),
            msg.channel_id,
            Some(msg.id),
            msg.author.clone(),
            Args::new(&msg.content, &[Delimiter::Single(',')]),
        );
        handler.handle(try_load_priceinfo(context).await).await;
    }
}

#[hook]
pub async fn pre_command(ctx: &Context, msg: &Message, _command_name: &str) -> bool {
    msg.channel_id.broadcast_typing(ctx).await.is_ok()
}
