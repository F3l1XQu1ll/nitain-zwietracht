use crate::commands::ALLOWED_CHECK;
use crate::error::CommandHandleResult;
use crate::msg_send;
use crate::{music, storage::manager::ContextStorageManager, utils::*};
use crab_proc::{crab_command, crab_group};
use serenity::{
    client::Context,
    framework::standard::{
        macros::{command, group},
        Args, CommandResult,
    },
    model::channel::Message,
};

use super::CommandContext;

// #[group]
// #[commands(
//     autoplay,
//     autoplay_off,
//     clearqueue,
//     join,
//     leave,
//     enable_loop,
//     lyrics,
//     nowplaying,
//     pause,
//     play,
//     queue,
//     remove_song,
//     repeat,
//     resume,
//     seek,
//     shuffle,
//     skip,
//     skipto,
//     spotify_playlist,
//     spotify_track,
//     unloop
// )]

#[crab_group(
    autoplay,
    autoplay_off,
    clearqueue,
    join,
    leave,
    enable_loop,
    lyrics,
    nowplaying,
    pause,
    play,
    queue,
    remove_song,
    repeat,
    resume,
    seek,
    shuffle,
    skip,
    skipto,
    spotify_playlist,
    spotify_track,
    toggle_autoplay,
    toggle_loop,
    unloop
)]
struct Music;

crab_command!(autoplay,
    #[aliases(autoplay_on, auto, auto_on, enable_autoplay)],
    #[only_in(guilds)],
    #[checks(allowed)],
    @call @no_slash music::player::autoplay
);

crab_command!(autoplay_off,
    #[aliases(stop_autoplay, auto_off)],
    #[only_in(guilds)],
    #[checks(allowed)],
    @call @no_slash music::player::autoplay_off
);

crab_command!(clearqueue,
    #[only_in(guilds)],
    #[aliases(clear, stop, clear_queue, clr)],
    #[checks(allowed)],
    @call music::player::stop
);

crab_command!(join,
    #[only_in(guilds)],
    #[aliases(j)],
    #[checks(allowed)],
    @call_args music::player::join
);

crab_command!(leave,
    #[only_in(guilds)],
    #[aliases(l, quit)],
    #[checks(allowed)],
    @call music::player::leave
);

crab_command!(enable_loop,
    #[aliases(enable_loop, loop_on, loop)],
    #[only_in(guilds)],
    #[checks(allowed)],
    @call_args @no_slash enable_loop_handler
);

async fn enable_loop_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available!"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), guild_id).await {
        if let Ok(args) = ctx.args().single::<String>() {
            if args == "off" {
                music::helper_loop::toggle_loop(ctx.ctx(), guild_id, channel_id, Some(false)).await;
            } else {
                music::helper_loop::toggle_loop(ctx.ctx(), guild_id, channel_id, Some(true)).await;
            }
        } else {
            music::helper_loop::toggle_loop(ctx.ctx(), guild_id, channel_id, Some(true)).await;
        }
        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(lyrics,
    #[checks(allowed)],
    @call_args music::lyrics::output_lyrics
);

crab_command!(nowplaying,
    #[only_in(guilds)],
    #[aliases(np)],
    #[checks(allowed)],
    @call nowplaying_handler
);

async fn nowplaying_handler(ctx: &CommandContext) -> CommandHandleResult {
    music::now_playing::now_playing(
        ctx.ctx(),
        ctx.guild_id()
            .ok_or_else(|| anyhow::anyhow!("Guild id not avilable!"))?,
        ctx.channel_id(),
    )
    .await;
    Ok(())
}

crab_command!(pause,
    #[only_in(guilds)],
    #[checks(allowed)],
    @call pause_handler
);

async fn pause_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available!"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), guild_id).await {
        music::toggle_pause::toggle_pause(ctx.ctx(), guild_id, channel_id, Some(true)).await;

        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(play,
    #[only_in(guilds)],
    #[aliases(p)],
    #[checks(allowed)],
    @call_args music::player::play
);

crab_command!(queue,
    #[only_in(guilds)],
    #[aliases(q)],
    #[checks(allowed)],
    @call queue_handler
);

async fn queue_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), guild_id).await {
        // Reset the page number to 1
        {
            let mut manager = ctx.storage().await;
            manager
                .update(|mut write| {
                    write
                        .config_mut()
                        .queue_page_mut()
                        .insert(*channel_id.as_u64(), 1);
                })
                .await
                .map_err(|_| anyhow::anyhow!("Could not update page number!"))?
        }
        music::queue::output_queue(ctx).await;
        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(remove_song,
    #[aliases(r, rm, remove)],
    #[only_in(guilds)],
    #[checks(allowed)],
    @call_args music::player::remove_song
);

crab_command!(repeat,
    #[only_in(guilds)],
    #[aliases(back, again)],
    #[checks(allowed)],
    @call repeat_handler
);

async fn repeat_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available!"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), guild_id).await {
        music::player::repeat(ctx.ctx(), guild_id, channel_id).await;
        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(resume,
    #[only_in(guilds)],
    #[aliases(unpause, c, cont)],
    #[checks(allowed)],
    @call resume_handler
);

async fn resume_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available!"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), ctx.guild_id().unwrap()).await {
        music::toggle_pause::toggle_pause(ctx.ctx(), guild_id, channel_id, Some(false)).await;

        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(seek,
    #[only_in(guilds)],
    #[checks(allowed)],
    @call_args music::player::seek
);

crab_command!(shuffle,
    #[only_in(guilds)],
    #[aliases(random)],
    #[checks(allowed)],
    @call shuffle_handler
);

async fn shuffle_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available!"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), ctx.guild_id().unwrap()).await {
        music::queue::shuffle_queue(ctx.ctx(), guild_id, channel_id).await;

        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(skip,
    #[only_in(guilds)],
    #[aliases(s)],
    #[checks(allowed)],
    @call_args skip_handler
);

async fn skip_handler(ctx: &CommandContext) -> CommandHandleResult {
    debug!("In skip handler");
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available"))?;
    let channel_id = ctx.channel_id();
    if is_playing(ctx.ctx(), guild_id).await {
        music::player::skip(ctx.ctx(), guild_id, channel_id, ctx.args()).await;
        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(skipto,
    #[only_in(guilds)],
    #[checks(allowed)],
    @call_args skipto_handler
);

async fn skipto_handler(ctx: &CommandContext) -> CommandHandleResult {
    if ctx.args().parse::<usize>().is_ok() {
        let guild_id = ctx
            .guild_id()
            .ok_or_else(|| anyhow::anyhow!("Guild id not available"))?;
        let channel_id = ctx.channel_id();
        if is_playing(ctx.ctx(), guild_id).await {
            music::player::skip(ctx.ctx(), guild_id, channel_id, ctx.args()).await;
            music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
        } else {
            msg_send!(ctx, "Nothing is playing right now.")
        }
    } else {
        msg_send!(ctx, "No valid queue index supplied.")
    }
    Ok(())
}

crab_command!(spotify_playlist,
    #[aliases(sp)],
    #[checks(allowed)],
    @call_args spotify_playlist_handler
);

async fn spotify_playlist_handler(ctx: &CommandContext) -> CommandHandleResult {
    music::spotify::load_msg(ctx.clone(), rspotify::model::SearchType::Playlist).await;
    Ok(())
}

crab_command!(spotify_track,
    #[aliases(st)],
    #[checks(allowed)],
    @call_args spotify_track_handler
);

async fn spotify_track_handler(ctx: &CommandContext) -> CommandHandleResult {
    music::spotify::load_msg(ctx.clone(), rspotify::model::SearchType::Track).await;
    Ok(())
}

crab_command!(unloop,
    #[aliases(disable_loop, noloop, no_loop, loop_off)],
    #[only_in(guilds)],
    #[checks(allowed)],
    @call @no_slash unloop_handler
);

async fn unloop_handler(ctx: &CommandContext) -> CommandHandleResult {
    let guild_id = ctx
        .guild_id()
        .ok_or_else(|| anyhow::anyhow!("Guild id not available"))?;
    let channel_id = ctx.channel_id;
    if is_playing(ctx.ctx(), guild_id).await {
        music::helper_loop::toggle_loop(ctx.ctx(), guild_id, channel_id, Some(false)).await;
        music::now_playing::now_playing(ctx.ctx(), guild_id, channel_id).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
    Ok(())
}

crab_command!(toggle_loop, #[only_in(guilds)], #[checks(allowed)], @call toggle_loop_handler);
async fn toggle_loop_handler(ctx: &CommandContext) -> CommandHandleResult {
    music::helper_loop::toggle_loop(ctx.ctx(), ctx.guild_id().unwrap(), ctx.channel_id, None).await;
    Ok(())
}

crab_command!(toggle_autoplay, #[only_in(guilds)], #[checks(allowed)], @call toggle_autoplay_handler);
async fn toggle_autoplay_handler(ctx: &CommandContext) -> CommandHandleResult {
    let context = ctx.ctx();
    let storage_manager = context.storage().await;
    let guild_data = {
        let read = storage_manager.read().await;
        read.kv()
            .get(ctx.guild_id().unwrap().as_u64())
            .cloned()
            .unwrap_or_default()
    };
    let state = guild_data.autoplay_state;
    if state {
        if let Err(why) = music::player::autoplay_off(ctx).await {
            debug!("Error deactivating autoplay: {:?}", why);
        }
    } else {
        music::player::autoplay::autoplay(ctx).await;
    }
    Ok(())
}
