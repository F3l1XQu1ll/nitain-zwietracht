use serenity::{
    client::{bridge::gateway::ShardMessenger, Context},
    framework::standard::{
        macros::check, Args, Command, CommandGroup, CommandResult, Delimiter, Reason,
    },
    http::Http,
    model::{
        channel::{Message, MessageReference, ReactionType},
        guild::Guild,
        id::{ChannelId, GuildId, MessageId},
        prelude::{
            command::CommandOptionType,
            interaction::{Interaction, InteractionResponseType},
            User,
        },
    },
};

use crate::{
    error::{ErrorContext, LogError},
    utils,
};

pub mod config;
pub mod dota;
pub mod help;
pub mod hook;
pub mod misc;
pub mod music;
pub mod warframe;

static COMMAND_GROUPS: &[&CrabGroup] = &[
    &config::CONFIG_CRAB_GROUP,
    &dota::DOTA_CRAB_GROUP,
    &misc::MISC_CRAB_GROUP,
    &music::MUSIC_CRAB_GROUP,
    &warframe::WARFRAME_CRAB_GROUP,
];

pub type CrabCommandFn =
    for<'fut> fn(_: &'fut CommandContext) -> futures::future::BoxFuture<'fut, CommandResult>;

/// Wrapper around crab commands and serenity commands for slashy commands
pub struct KniveBox {
    serenity_command: &'static Command,
    crab_command: CrabCommandFn,
    slash_args_enabled: bool,
    slash_enabled: bool,
}

pub struct CrabGroup {
    #[allow(unused)]
    group: &'static CommandGroup,
    slash_commands: &'static [&'static KniveBox],
}

#[derive(Clone)]
pub struct CommandContext {
    pub ctx: Context,
    pub guild: Option<Guild>,
    pub channel_id: ChannelId,
    pub message_id: Option<MessageId>,
    pub user: User,
    pub args: Args,
}

impl CommandContext {
    pub fn new(
        ctx: Context,
        guild: Option<Guild>,
        channel_id: ChannelId,
        message_id: Option<MessageId>,
        user: User,
        args: Args,
    ) -> Self {
        Self {
            ctx,
            guild,
            channel_id,
            message_id,
            user,
            args,
        }
    }

    pub fn ref_msg(&self) -> Option<MessageReference> {
        self.message_id
            .map(|message_id| (self.channel_id, message_id).into())
    }

    pub fn ctx(&self) -> &Context {
        &self.ctx
    }

    pub fn args(&self) -> Args {
        self.args.clone()
    }

    pub fn user(&self) -> &User {
        &self.user
    }

    pub fn channel_id(&self) -> ChannelId {
        self.channel_id
    }

    pub fn guild(&self) -> Option<Guild> {
        self.guild.clone()
    }

    pub fn guild_id(&self) -> Option<GuildId> {
        self.guild.clone().map(|g| g.id)
    }
}

impl AsRef<ShardMessenger> for CommandContext {
    fn as_ref(&self) -> &ShardMessenger {
        self.ctx.as_ref()
    }
}

impl AsRef<Http> for CommandContext {
    fn as_ref(&self) -> &Http {
        self.ctx.as_ref()
    }
}

#[check]
#[display_in_help(false)]
pub async fn allowed(ctx: &Context, msg: &Message) -> Result<(), Reason> {
    let allowed = utils::check_channel_allowed(ctx, msg).await;
    if allowed {
        Ok(())
    } else {
        if msg
            .react(&ctx, ReactionType::Unicode("🚫".to_string()))
            .await
            .is_err()
        {
            error!("Failed to react to the failed command");
        }
        Err(Reason::User("Channel not allowed".to_string()))
    }
}

pub async fn register_slash_commands(ctx: Context, guilds: Vec<GuildId>) {
    for guild in &guilds {
        info!("Registering commands for guild: {guild:?}");
        let ctx = ctx.clone();
        guild
            .set_application_commands(&ctx, |create_commands| {
                for group in COMMAND_GROUPS {
                    let knives = group.slash_commands;
                    for knive in knives {
                        if !knive.slash_enabled {
                            continue;
                        }
                        create_commands.create_application_command(|c| {
                            let name = knive.serenity_command.options.names[0];
                            let desc = knive.serenity_command.options.desc;
                            let args = knive.slash_args_enabled;
                            debug!("name: {}", name);
                            c.name(name);
                            c.description({
                                if let Some(command_description) = desc {
                                    command_description
                                } else {
                                    name
                                }
                            });
                            if args {
                                c.create_option(|o| {
                                    o.name("args")
                                        .description("Arguments")
                                        // .default_option(true)
                                        .kind(CommandOptionType::String);
                                    o
                                });
                                debug!("Registered args for command: {}", name);
                            }
                            c
                        });
                    }
                }
                create_commands
            })
            .await
            .context("Failed to register slash commands")
            .handle();
    }
    info!("Done registering commands!");
}

pub async fn handle_interaction_create(ctx: Context, interaction: Interaction) {
    if let Interaction::ApplicationCommand(command) = interaction {
        let command_name = command.data.name.clone();
        let mut to_call_command = None;
        'outer: for group in COMMAND_GROUPS {
            for command in group.slash_commands {
                if command.serenity_command.options.names[0] == command_name {
                    to_call_command = Some(command);
                    break 'outer;
                }
            }
        }
        if let Some(to_call_command) = to_call_command {
            command
                .create_interaction_response(&ctx, |r| {
                    r.kind(InteractionResponseType::DeferredChannelMessageWithSource)
                    // r.interaction_response_data(|d| {
                    //     d.create_embed(|e| {
                    //         e.description("Loading …");
                    //         e.color(Color::from_hex("4444FF"));
                    //         e
                    //     })
                    // })
                })
                .await
                .context("Failed to send interaction response for slash command!")
                .handle();
            let command_context = CommandContext::new(
                ctx.clone(),
                match command.guild_id {
                    Some(id) => id.to_guild_cached(&ctx),
                    None => None,
                },
                command.channel_id,
                None,
                command.user.clone(),
                command
                    .data
                    .options
                    .get(0)
                    .and_then(|opt| opt.value.clone())
                    .map(|val| {
                        Args::new(val.as_str().unwrap(), &[Delimiter::Single(',')])
                            .quoted()
                            .clone()
                    })
                    .unwrap_or_else(|| Args::new("", &[Delimiter::Single(',')])),
            );
            (to_call_command.crab_command)(&command_context)
                .await
                .handle();
            command
                .edit_original_interaction_response(&ctx, |r| {
                    r.content("Done :white_check_mark:")
                    // r.flags(InteractionApplicationCommandCallbackDataFlags::EPHEMERAL)
                })
                .await
                .context("Failed to send anoyment (required slash command response)")
                .handle();
        }
    }
}
