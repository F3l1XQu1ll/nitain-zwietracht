use serenity::{
    client::Context,
    framework::standard::{
        help_commands, macros::help, Args, CommandGroup, CommandOptions, CommandResult, HelpOptions,
    },
    model::{channel::Message, id::UserId},
};
use std::collections::HashSet;

#[help("help", "commands", "cmds", "h ")] // I DARE YOU REMOVE THE SPACE AFTER THE 'h'! (#58)
#[available_text = "You can use it in"]
#[max_levenshtein_distance(3)]
#[aliases_label = "You can also use"]
#[dm_only_text = "DMs only"]
#[guild_only_text = "Guilds only"]
#[dm_and_guild_text = "both Guilds and DMs"]
#[no_help_available_text = "I don't know this command :("]
#[individual_command_tip = "**The prefix of the bot is `+`.**\n\n[Click here to invite me to your server!](https://discord.com/api/oauth2/authorize?client_id=859044414009114624&permissions=8&scope=applications.commands%20bot)\n\nYou can check for prices of a Warframe-item with `+[your item to search]`.\nIf you want to search multiple items:\n`+[your 1st item to search], [your 2nd item to search], ...`\n\nJoin a voice channel and listen to music with `+p [link/song name]`!\n\nWant me to explain a command or find aliases? Type `+help [command name]`\n\n**__Bot Commands:__**"]
#[strikethrough_commands_tip_in_dm = ""]
#[strikethrough_commands_tip_in_guild = ""]
#[lacking_role = "Nothing"]
#[lacking_permissions = "Nothing"]
#[lacking_ownership = "Nothing"]
#[lacking_conditions = "Nothing"]
#[wrong_channel = "Nothing"]
#[embed_error_colour = "#b00020"]
#[embed_success_colour = "#40ff40"]
pub async fn my_help(
    ctx: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    if super::allowed(
        ctx,
        msg,
        &mut Args::new("", &[]),
        &CommandOptions::default(),
    )
    .await
    .is_ok()
    {
        let _ = help_commands::with_embeds(ctx, msg, args, help_options, groups, owners).await;
    }
    Ok(())
}
