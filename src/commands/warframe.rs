use crate::commands::ALLOWED_CHECK;
use crate::warframe;
use crab_proc::{crab_command, crab_group};
use serenity::framework::standard::Args;
use serenity::{
    client::Context,
    framework::standard::{
        macros::{command, group},
        CommandResult,
    },
    model::channel::Message,
};

// #[group]
// #[commands(arbitration, baro, cetus, deimos, events, fissures, fortuna, sortie)]
#[crab_group(arbitration, baro, cetus, deimos, events, fissures, fortuna, sortie)]
struct Warframe;

crab_command!(arbitration, #[aliases(arbi)], #[checks(allowed)], @call warframe::world::arbitration_fetch);

crab_command!(baro, #[aliases(b)], #[checks(allowed)], @call warframe::world::output_baro);

crab_command!(cetus, #[checks(allowed)], @call warframe::world::cetus);

crab_command!(deimos, #[checks(allowed)], @call warframe::world::deimos);

crab_command!(fortuna, #[checks(allowed)], @call warframe::world::fortuna);

crab_command!(events, #[checks(allowed)], #[aliases(event, e)], @call warframe::event::output_events);

crab_command!(fissures, #[checks(allowed)], #[aliases(fissure, f)], @call warframe::world::fissure_output);

crab_command!(sortie, #[checks(allowed)], @call warframe::sortie::output_sortie);
