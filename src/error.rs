use std::fmt::{Debug, Display};

use thiserror::Error;

pub type CommandHandleResult = Result<(), CommandHandleError>;

pub trait LogError {
    fn handle(self);
}

/// Wrapper for anyhows [`Context`](anyhow::Context) trait.
/// As we make heavy use of serenitys `Context`, this avoids the ambiguous names.
pub trait ErrorContext<T, E> {
    fn context<C>(self, context: C) -> Result<T, anyhow::Error>
    where
        C: Display + Send + Sync + 'static;
    fn with_context<C, F>(self, f: F) -> Result<T, anyhow::Error>
    where
        C: Display + Send + Sync + 'static,
        F: FnOnce() -> C;
}

#[derive(Error, Debug)]
pub enum CommandHandleError {
    #[error(transparent)]
    Internal(#[from] anyhow::Error),
    #[error("Error: {message}")]
    Custom { message: String },
}

#[derive(Error, Debug)]
pub enum DotaError {
    #[error("Could not parse hero name")]
    NameArgsParseError,
    #[error("Could not parse match id")]
    MatchIDArgsParseError,
    #[error("Could not retrieve heroes")]
    HeroRetrievalError,
    #[error("Unknown hero")]
    HeroUnknownError,
    #[error("Could not find counters for hero: {hero:#?}")]
    CountersNotFound { hero: String },
    #[error("User not registered")]
    UserNotRegistered,
    #[error("Could not load matches")]
    NoMatchesFound,
    #[error("Could not load profile info")]
    ProfileLoadError,
    #[error("Could not parse dota profile id")]
    ProfileIDArgsParseError,
    #[error("Could not load pro list")]
    ProPlayerListRetrievalError,
    #[error("Could not load match")]
    MatchLoadError,
    #[error("User is not following any pro")]
    EmptyWatchingError,
    #[error("No valid pro name to watch submitted")]
    WatchInvalidProError,
    #[error("Name not in pro list")]
    NotAProError { name: String },
}

impl From<DotaError> for CommandHandleError {
    fn from(e: DotaError) -> Self {
        Self::Custom {
            message: match e {
                DotaError::NameArgsParseError => "Could not parse this hero's name!".to_string(),
                DotaError::HeroRetrievalError => ":warning: Could not retrieve heroes!".to_string(),
                DotaError::HeroUnknownError => ":warning: Unknown hero!".to_string(),
                DotaError::CountersNotFound { hero } => {
                    "Could not find any conters for ".to_owned() + &hero
                }
                DotaError::UserNotRegistered => {
                    "Please register your steam id first! Hint: use `+register_dota DOTA_ID"
                        .to_string()
                }
                DotaError::NoMatchesFound => "Could not retrieve match data!".to_string(),
                DotaError::MatchIDArgsParseError => "Could not parse the match id!".to_string(),
                DotaError::ProfileLoadError => "Could not load this profile!".to_string(),
                DotaError::ProfileIDArgsParseError => "Please head to opendota.com, log in and use this command again using your id.\n(Can be found as number in the url)".to_string(),
                DotaError::ProPlayerListRetrievalError => "Could not load the list of pro-players!".to_string(),
                DotaError::MatchLoadError => "Could not load match!".to_string(),
                DotaError::EmptyWatchingError => "Please search for a player or watch (+dota_follow) one first!".to_string(),
                DotaError::WatchInvalidProError => "Please specify a valid pro player name!".to_string(),
                DotaError::NotAProError { name } => format!("The name {:#} is not considered a pro player!", name),
            },
        }
    }
}

impl<T, E: Debug> LogError for Result<T, E> {
    fn handle(self) {
        if let Err(e) = self {
            error!("{e:#?}");
        }
    }
}

impl<U, E, T: anyhow::Context<U, E>> ErrorContext<U, E> for T {
    fn context<C>(self, context: C) -> Result<U, anyhow::Error>
    where
        C: Display + Send + Sync + 'static,
    {
        anyhow::Context::context(self, context)
    }

    fn with_context<C, F>(self, f: F) -> Result<U, anyhow::Error>
    where
        C: Display + Send + Sync + 'static,
        F: FnOnce() -> C,
    {
        anyhow::Context::with_context(self, f)
    }
}
