use crate::message_box::CrabMessageBuilder;

pub trait Template {
    fn new<T>(data: T) -> Self;
    fn template(&self, builder: &mut CrabMessageBuilder) -> &mut CrabMessageBuilder;
}
