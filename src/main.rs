#![deny(clippy::disallowed_methods)]

use crate::{
    action_queue::{ActionQueue, ContextActionQueue},
    commands::{config::*, dota::*, help::*, hook::*, misc::*, music::*, warframe::*},
    controller::CrabController,
    storage::crab_data::CrabData,
};
use commands::{handle_interaction_create, register_slash_commands};
use rocket_lib::{config::Shutdown, fs::FileServer, Config};
use rocket_okapi::swagger_ui::{make_swagger_ui, SwaggerUIConfig};
use serenity::{
    async_trait,
    client::{bridge::gateway::ShardManager, Client, Context, EventHandler},
    framework::StandardFramework,
    model::{channel::Message, gateway::Ready, id::GuildId, prelude::interaction::Interaction},
    prelude::{GatewayIntents, RwLock, TypeMapKey},
};
use songbird::SerenityInit;
use std::{
    env,
    net::{IpAddr, Ipv4Addr},
    sync::{atomic::AtomicBool, Arc},
};
use storage::{db::DBStorage, manager::ContextStorageManager};
use tokio::sync::Mutex;
use tracing_subscriber::{fmt::Layer, layer::SubscriberExt, util::SubscriberInitExt};
use tracing_subscriber::{fmt::MakeWriter, EnvFilter};

mod action_queue;
mod auth;
mod commands;
mod controller;
mod dota;
mod error;
mod issues;
mod message_box;
mod music;
mod rocket;
mod storage;
mod template;
mod utils;
mod voting;
mod warframe;

#[macro_use]
extern crate rocket as rocket_lib;

#[macro_use]
extern crate anyhow;

#[macro_use]
mod macros;

struct ShardManagerContainer;
impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, _ctx: Context, msg: Message) {
        if msg.content.to_lowercase().starts_with('+') {
            info!(
                "Received messge {:?} at {:?} from user {:?} in {:?}",
                msg.content,
                chrono::Local::now(),
                msg.author,
                msg.channel_id
            );
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        use serenity::model::gateway::Activity;
        use serenity::model::user::OnlineStatus;

        let activity = Activity::playing("+help");
        let status = OnlineStatus::Online;

        ctx.set_presence(Some(activity), status).await;
        info!("{} is connected!", ready.user.name);
    }

    async fn cache_ready(&self, ctx: Context, guilds: Vec<GuildId>) {
        register_slash_commands(ctx.clone(), guilds.clone()).await;
        startup_join(ctx.clone(), guilds).await;
        register_shutdown_hook(ctx).await;
    }

    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        handle_interaction_create(ctx, interaction).await;
    }
}

async fn startup_join(ctx: Context, guilds: Vec<GuildId>) {
    info!("Resuming …");
    let storage_manager = ctx.storage().await;

    info!("resuming {} guilds", guilds.len());
    // Status update
    let reachable_info_channels = {
        storage_manager
            .read()
            .await
            .kv()
            .values()
            .filter_map(|s| s.player_interaction)
            .collect::<Vec<_>>()
    };
    for channel_id in &reachable_info_channels {
        msg_send!(ctx, channel_id, "Starting, please be patient :rocket:")
    }
    for guild_id in guilds {
        info!("resuming guild with id {guild_id}");
        let server_storage = {
            storage_manager
                .read()
                .await
                .kv()
                .get(guild_id.as_u64())
                .cloned()
                .unwrap_or_default()
        };
        if let Some(channel_id) = server_storage.playing_in {
            info!("found registered channel {channel_id}");
            if let Some(manager) = songbird::get(&ctx).await {
                info!("joining …");
                let channel = ctx.http.get_channel(channel_id).await.unwrap();
                let guild = ctx.http.get_guild(*guild_id.as_u64()).await.unwrap();

                info!("{}", guild.name);
                info!(
                    "{} ({:?})",
                    channel.clone().guild().unwrap().name(),
                    channel.clone().guild().unwrap().kind
                );

                let (handler_lock, join_result) = manager.join(guild.id, channel.id()).await;

                if let Err(e) = join_result {
                    error!("{e}");
                    drop(handler_lock);
                    continue;
                } else {
                    // Register action for this guild to play titles from the queue stored in CrabData
                    // StorageManager holds an Arc<RwLock<CrabData>> which is used to access the queue
                    // We register the action here regardless of the shared queue being empty or not as we joined manually
                    let action_queue = ctx.action_queue().await;
                    let mut write = action_queue.write().await;
                    let mut storage_clone = storage_manager.clone();
                    storage_clone
                        .update(|mut write| {
                            write
                                .kv_mut()
                                .entry(*guild_id.as_u64())
                                .or_default()
                                .first_launch = true;
                        })
                        .await
                        .expect("Could not set first_launch state");
                    let ctx_clone = ctx.clone();
                    write
                        .register_action(
                            100,
                            false,
                            music::queue::QueueEnqueueNewAction {
                                ctx: ctx_clone,
                                guild_id,
                            },
                        )
                        .await;
                    info!("Added action queue for guild {}", guild_id);
                }
                info!("… done");
            }
        }
        info!("done resumimg guild");
    }
    for channel_id in reachable_info_channels {
        msg_send!(ctx, channel_id, "… Done :white_check_mark:")
    }
    info!("guilds resumed");
}

async fn register_shutdown_hook(ctx: Context) {
    //let signals = Signals::new(&[SIGINT, SIGTERM, SIGHUP, SIGQUIT]).unwrap();
    //tokio::spawn(async move {
    //let mut signals: Signals = signals;
    //while let Some(signal) = signals.next().await {
    //match signal {
    //    SIGINT | SIGTERM | SIGHUP | SIGQUIT => {
    ctrlc_async::set_async_handler(async move {
        info!("Shutdown event caught! Shutting down ... we have 30 seconds ...");
        if let Some(manager) = songbird::get(&ctx).await {
            let mut storage = ctx.storage().await;
            let storage_clone = storage.clone();
            let guilds_u64 = {
                let read = storage_clone.read().await;
                let kv = read.kv();
                kv.keys().copied().collect::<Vec<_>>()
            };
            // iterate available guilds to store the current tracks position
            for guild_u64 in guilds_u64 {
                if let Some(handler_lock) = manager.get(guild_u64) {
                    if let Some(track) = handler_lock.lock().await.queue().current() {
                        if let Ok(info) = track.get_info().await {
                            let position = info.position;
                            debug!("setting track position to {position:#?} for guild {guild_u64}");
                            if let Err(e) = storage
                                .update(|mut write| {
                                    write.kv_mut().entry(guild_u64).or_default().track_position =
                                        Some(position);
                                })
                                .await
                            {
                                error!("Failed to update track position! {e:#?}");
                            }
                        } else {
                            warn!("Could not get track info!");
                        }
                    }
                }
            }
            // Commit storage
            if let Err(e) = storage.commit().await {
                error!("Failed to update track position! {e:#?}");
            } else {
                info!("Successfully stored playing state!");
                let data = ctx.data.read().await;
                let shard_manager = data.get::<ShardManagerContainer>().unwrap();
                shard_manager.lock().await.shutdown_all().await;
                {
                    // Set flag to allow shutdown request
                    ctx.data
                        .read()
                        .await
                        .get::<ShutdownFlag>()
                        .unwrap()
                        .0
                        .store(true, std::sync::atomic::Ordering::Release);
                    let read = ctx.data.read().await;
                    let rocket_info = read.get::<RocketInfo>().unwrap();
                    let client = reqwest::Client::builder().build().unwrap();
                    client
                        .get(format!(
                            "http://{}:{}/shutdown",
                            rocket_info.address, rocket_info.port
                        ))
                        .send()
                        .await
                        .unwrap();
                }
                //process::exit(0);
            }
        } else {
            error!("Could not get songbird manager at shutdown");
        }
    })
    .expect("Failed to register shutdown handler");
    //_ => warn!("Caught unexpected signal"),

    //}
    //}
    //});
}

#[derive(Clone)]
/// We can not implement [`Write`](std::io::Write) for extern types
/// ([`Arc`](std::sync::Arc), [`RwLock`](std::sync::RwLock)),
/// so we create a struct as a wrapper for our buffer
pub struct LockedByteBuf(Arc<std::sync::RwLock<Vec<u8>>>);

impl<'a> std::io::Write for &'a LockedByteBuf {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        // write to our buffer (Vec<u8>)
        self.0.write().unwrap().write(buf)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        // same here, not sure if flushing realy does something here, though
        self.0.write().unwrap().flush()
    }
}

#[derive(Clone)]
/// Wrapper struct to implement MakeWriter on.
///
/// Holds a [`LockedByteBuf`](crate::LockedByteBuf),
/// that we use to create a shared buffer for our logs.
pub struct LogBuffer(LockedByteBuf);
impl<'a> MakeWriter<'a> for LogBuffer {
    type Writer = &'a LockedByteBuf;
    fn make_writer(&'a self) -> Self::Writer {
        &self.0
    }
}

/// Flag to allow or deny shutown requests to rocket
#[derive(Clone)]
pub struct ShutdownFlag(Arc<AtomicBool>);
impl TypeMapKey for ShutdownFlag {
    type Value = Self;
}

/// Struct storing info about the rocket server
///
/// For readonly access from crab data
pub struct RocketInfo {
    port: u16,
    address: IpAddr,
}
impl TypeMapKey for RocketInfo {
    type Value = Self;
}

#[tokio::main]
async fn main() {
    // Create our shared buffer for logging
    // Note the fancy tail
    let log_buffer = LogBuffer(LockedByteBuf(Arc::new(std::sync::RwLock::new(Vec::new()))));
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .compact()
        .with_line_number(true)
        .finish()
        // create a second layer with slightly different settings and our buffer as writer
        // cloning the writer will clone the internal Arc -> Shared memory
        // the other one goes into rocket, or anywhere else.
        .with(
            Layer::default()
                .json()
                .flatten_event(true)
                .with_target(false)
                .without_time()
                .with_writer(log_buffer.clone()),
        )
        .init();

    // Variable flag on application shutdown to allow shutdown requests
    let shutdown_flag = ShutdownFlag(Arc::new(AtomicBool::new(false)));

    // Configure the client with your Discord bot token in the environment.
    let application_id_env = "APPLICATION_ID";
    let discord_token_env = "DISCORD_TOKEN";

    #[cfg(feature = "env_override")]
    {
        let discord_token = include_str!("../discord_token.txt");
        env::set_var(discord_token_env, discord_token);

        let application_id = include_str!("../application_id.txt");
        env::set_var(application_id_env, application_id);

        let key_env = "YT_API_KEY";
        let key = include_str!("../youtube_api_key.txt");
        env::set_var(key_env, key);

        let musixmatch_token_env = "MUSIXMATCH_TOKEN";
        let musixmatch_token = include_str!("../musixmatch_token.txt");
        env::set_var(musixmatch_token_env, musixmatch_token);

        let genius_token_env = "GENIUS_TOKEN";
        let genius_token = include_str!("../genius_token.txt");
        env::set_var(genius_token_env, genius_token);
    }

    // Configure the client with your Discord bot token in the environment.
    let token = env::var(discord_token_env).expect("Expected a token in the environment");

    // Set gateway intents, which decides what events the bot will be notified about
    let intents = GatewayIntents::all();

    let id: u64 = env::var(application_id_env)
        .expect("Expected an application id in the environment")
        .trim()
        .parse()
        .expect("Application ID is not a valid ID");

    let framework = StandardFramework::new()
        .configure(|c| c.prefix("+"))
        .unrecognised_command(unknown_command)
        .before(pre_command)
        .help(&MY_HELP)
        .group(&MUSIC_GROUP)
        .group(&WARFRAME_GROUP)
        .group(&DOTA_GROUP)
        .group(&MISC_GROUP)
        .group(&CONFIG_GROUP);

    let mut client = Client::builder(&token, intents)
        .event_handler(Handler)
        .application_id(id)
        .framework(framework)
        .register_songbird()
        .await
        .expect("Err creating client");

    let db_storage = DBStorage::new().await.unwrap();
    let crab_data = Arc::new(RwLock::new(
        /*if cfg!(feature = "storage_file") {
            storage::load_storage::<CrabData>("data.json".to_string())
                .await
                .unwrap_or_default()
        } else*/
        {
            if let Err(e) = db_storage.setup_table().await {
                eprintln!("{e}");
            }
            db_storage
                .load_storage::<CrabData>()
                .await
                .unwrap_or_default()
        },
    ));
    {
        let mut data = client.data.write().await;
        data.insert::<CrabData>(crab_data.clone());
        let mut action_queue = ActionQueue::new(10); // 10 milliseconds
        action_queue.launch().await;
        data.insert::<ActionQueue>(Arc::new(RwLock::new(action_queue)));
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<ShutdownFlag>(shutdown_flag.clone());
    }

    let http = client.cache_and_http.http.clone();
    let controller = CrabController::new(crab_data.clone(), {
        client
            .data
            .read()
            .await
            .get::<songbird::serenity::SongbirdKey>()
            .cloned()
            .expect("Could not retrieve Songbird from typemap")
    });

    /*
    let data = Arc::clone(&client.data);
    tokio::spawn(async move {
        loop {
            let crab_data = {
                let data = data.read().await;
                let lock = data
                    .get::<CrabData>()
                    .expect("Expected CrabData in TypeMap.");
                let read = lock.read().await;
                read.clone()
            };
            if cfg!(feature = "storage_file") {
                storage::dump_storage("data.json".to_string(), crab_data).await;
            } else if let Err(e) = db_storage.dump_storage(crab_data).await {
                eprintln!("{e}");
            }
            //eprintln!("Data synced!");
            tokio::time::sleep(Duration::from_secs(10)).await;
        }
    });
    */

    //
    // Rocket setup
    //
    info!("Launching rocket!");
    // Get ROCKET_PORT from environment
    let port: u16 = env::var("ROCKET_PORT")
        .unwrap_or_else(|_| "8000".to_string())
        .trim()
        .replace('\\', "")
        .parse()
        .unwrap_or(8000);
    // Get ROCKET_ADDRESS from environment
    let address: IpAddr = env::var("ROCKET_ADRESS")
        .unwrap_or_else(|_| "0.0.0.0".to_string())
        .trim()
        .replace('\\', "")
        .parse()
        .unwrap_or(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)));
    let rocket = rocket_lib::build()
        .mount("/static", FileServer::from("static/"))
        .mount(
            "/",
            rocket_lib::routes![rocket::ping::ping, rocket::control::shutdown,],
        )
        .mount(
            "/v1",
            rocket_okapi::openapi_get_routes![
                rocket::api::nitain,
                rocket::api::world,
                rocket::api::data,
                rocket::api::logs,
                rocket::api::authenticate,
                rocket::api::player,
                rocket::api::player_pause,
                rocket::api::user_available_players,
                rocket::api::queue,
            ],
        )
        .mount("/v1", rocket_cors::catch_all_options_routes())
        .mount(
            "/v1/swagger-ui",
            make_swagger_ui(&SwaggerUIConfig {
                url: "../openapi.json".to_owned(),
                ..Default::default()
            }),
        )
        .manage(crab_data)
        .manage(log_buffer)
        .manage(shutdown_flag)
        .manage(http)
        .manage(controller)
        .manage(rocket::cors()) // DO NOT REMOVE THIS
        .attach(rocket::cors()) // THIS (CORS) HAS A BUG THAT REQUIRES MANAGED CORS AS WORKAROUND
        .configure(Config {
            shutdown: Shutdown {
                mercy: 10,
                force: true,
                ctrlc: false,
                ..Shutdown::default()
            },
            port,
            address,
            ..Config::default()
        });

    client
        .data
        .write()
        .await
        .insert::<RocketInfo>(RocketInfo { port, address });

    // Avoid blocking this thread as rocket has not been launched yet
    tokio::spawn(async move {
        let _ = client
            .start()
            .await
            .map_err(|why| println!("Client ended: {:?}", why));
    });

    let _ = rocket
        .launch()
        .await
        .map_err(|why| eprint!("rocket::launch:{why}"));
}
