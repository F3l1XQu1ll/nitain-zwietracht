#![deny(clippy::disallowed_methods)]

use std::fmt::Debug;

use crate::{
    commands::CommandContext, error::CommandHandleResult, storage::manager::ContextStorageManager,
};
use anyhow::Result as AnyhowResult;
use serenity::{
    client::Context,
    model::{
        channel::Message,
        id::{ChannelId, GuildId},
    },
};

pub trait IntoChannelId {
    fn into_channel_id(self) -> ChannelId;
}

impl IntoChannelId for Message {
    fn into_channel_id(self) -> ChannelId {
        self.channel_id
    }
}

impl IntoChannelId for &Message {
    fn into_channel_id(self) -> ChannelId {
        self.channel_id
    }
}

impl IntoChannelId for ChannelId {
    fn into_channel_id(self) -> ChannelId {
        self
    }
}

impl IntoChannelId for &ChannelId {
    fn into_channel_id(self) -> ChannelId {
        *self
    }
}

pub fn channel_id(into_id: impl IntoChannelId) -> ChannelId {
    into_id.into_channel_id()
}

pub async fn check_channel_allowed(ctx: &Context, msg: &Message) -> bool {
    if let Some(guild_id) = msg.guild_id {
        let manager = ctx.storage().await;
        let read = manager.read().await;
        if *read
            .config()
            .allowed_guilds()
            .get(guild_id.as_u64())
            .unwrap_or(&false)
        {
            // guild has selected allowed channels
            *read
                .config()
                .allowed_channels()
                .get(msg.channel_id.as_u64())
                .unwrap_or(&false)
        } else {
            // Guild is new and there are no restrictions yet
            true
        }
    // direct messages are always allowed
    } else {
        true
    }
}

pub async fn set_channel_allowed(ctx: &CommandContext, allowed: bool) {
    let mut storage_manager = ctx.storage().await;
    storage_manager
        .update(|mut write| {
            write.config_mut().allow_channel(ctx.channel_id(), allowed);
        })
        .await
        .expect("Could not commit write.");
    if allowed {
        storage_manager
            .update(|mut write| {
                write
                    .config_mut()
                    .allow_guild(ctx.guild_id().unwrap(), true);
            })
            .await
            .expect("Could not commit write.");
    }
}

pub async fn set_guild_allowed(ctx: &Context, guild_id: GuildId, allowed: bool) {
    let mut storage_manager = ctx.storage().await;
    storage_manager
        .update(|mut write| {
            write.config_mut().allow_guild(guild_id, allowed);
        })
        .await
        .expect("Could not commit write.");
}

pub async fn allow_handler(ctx: &CommandContext) -> CommandHandleResult {
    match ctx.args.rest() {
        "all" => {
            set_guild_allowed(
                ctx.ctx(),
                ctx.guild_id()
                    .ok_or_else(|| anyhow::anyhow!("Command executed from non-guild channel!"))?,
                false,
            )
            .await
        }
        _ => {
            set_channel_allowed(
                ctx,
                match ctx.args.rest() {
                    "true" => true,
                    "false" => false,
                    _ => true,
                },
            )
            .await;
        }
    }
    Ok(())
}

/// Checks that a message successfully sent; if not, then logs why to stdout.
pub fn check_msg<T, E: Debug>(result: Result<T, E>) {
    if let Err(why) = result {
        println!("Error sending message: {:?}", why);
    }
}

/// Parses the supplied URL and makes an asynchronous HTTP GET request. Reads HTTP response body into a String.
pub async fn do_fetch<U: reqwest::IntoUrl>(url: U) -> AnyhowResult<String> {
    let client = reqwest::Client::new();
    let request = client.get(url).header("Accept-Language", "en").build()?;
    Ok(client.execute(request).await?.text().await?)
}

/// Checks if there is something playing
pub async fn is_playing(ctx: &Context, guild_id: GuildId) -> bool {
    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    // Check if there is a voice chat connection.
    if manager.get(guild_id).is_some() {
        !ctx.storage()
            .await
            .read()
            .await
            .kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
            .queue
            .is_empty()
    } else {
        false
    }
}

pub trait Ellipsize {
    /// Reduces the length of a given [String](String)
    /// to the given length `limit`, including ' …'-suffix.
    ///
    /// Warning: the '…' character has a length of 3, not one!
    ///
    /// Returns the part of self that was cut off,
    /// or None if self did not exceed the limit.
    fn ellipsize(&mut self, limit: usize) -> Option<String>;
}

impl Ellipsize for String {
    fn ellipsize(&mut self, limit: usize) -> Option<String> {
        if self.len() > limit {
            let ellipse = " …";
            let suffix = self.split_off(limit - ellipse.len());
            let ellipsized = self.trim_end().to_owned() + ellipse;
            assert!(ellipsized.len() <= limit);
            *self = ellipsized;
            Some(suffix.trim_start().to_string())
        } else {
            None
        }
    }
}

#[test]
pub fn test_string_ellipsize() {
    let mut string = String::from("Lorem ipsum dolor sit amet.");
    let ellipsized = string.ellipsize(15);
    assert_eq!(string.as_str(), "Lorem ipsum …");
    assert_eq!(ellipsized.unwrap().as_str(), "dolor sit amet.");
}
