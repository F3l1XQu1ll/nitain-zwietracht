#[macro_export]
macro_rules! msg_send {
    ($c:expr, $i: expr, $s:expr $(,)?) => {
        // $crate::utils::check_msg($crate::utils::channel_id($i).say(&$c.http, $s).await)
        $crate::utils::check_msg(
            $crate::message_box::CrabMessage::build(
                $c.shard.clone(),
                $c.http.clone(),
                $crate::utils::channel_id($i),
            )
            .title($s)
            .message()
            .await,
        )
    };
    ($c:expr, $s:expr $(,)?) => {
        // $crate::utils::check_msg($crate::utils::channel_id($i).say(&$c.http, $s).await)
        $crate::utils::check_msg(
            $crate::message_box::CrabMessage::build(
                $c.ctx().shard.clone(),
                $c.ctx().http.clone(),
                $c.channel_id(),
            )
            .title($s)
            .message()
            .await,
        )
    };
}

#[macro_export]
macro_rules! maybe_result {
    ($ctx:expr, $channel:expr,  $msg:expr, $maybe:expr) => {
        let command_result: Box<dyn std::any::Any + Send> = Box::new($maybe);
        if let Ok(result) = command_result.downcast::<$crate::error::CommandHandleResult>() {
            debug!("Command returned CommandHandleResult");
            let handler = $crate::message_box::CommandHandler::new(
                $ctx.http.clone(),
                $ctx.shard.clone(),
                $channel,
                $msg,
            );
            handler.handle(*result).await;
        }
    };
}

#[macro_export]
/// Synopsis: crab_command!(`name`, #\[`meta`\], #\[…\], (@call|@call_clone|@call_args|@call_args_clone) `path`)
/// `path` is required to be async and return either [`CommandHandleResult`](crate::error::CommandHandleResult) or `()`
macro_rules! crab_command {
    ($name:ident, $(#[$meta:meta]),*, @call $callback:path) => {
        #[command]
        $(#[$meta])*
        pub async fn $name(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
            let context = $crate::commands::CommandContext::new(
                ctx.clone(),
                msg.guild(&ctx).await,
                msg.channel_id,
                Some(msg.id),
                msg.author.clone(),
                args,
            );
            paste::paste!{
                [<$name _com_ctx>](&context).await
            }
        }
        paste::paste!{
            pub static [<$name:upper _SLASH_ARGS_ENABLED>]: bool = false;
            pub fn [<$name _com_ctx>]<'fut>(ctx: &'fut$crate::commands::CommandContext) -> futures::future::BoxFuture<'fut, CommandResult> {
                use futures::future::FutureExt;
                async move {
                    let res: CommandResult = {
                        $crate::maybe_result!(ctx.ctx(), ctx.channel_id(), ctx.ref_msg(), $callback(&ctx).await);
                        Ok(())
                    };
                    res
                }.boxed()
            }
        }
    };
    ($name:ident, $(#[$meta:meta]),*, @call_args $callback:path) => {
        #[command]
        $(#[$meta])*
        pub async fn $name(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
            let context = $crate::commands::CommandContext::new(
                ctx.clone(),
                msg.guild(&ctx).await,
                msg.channel_id,
                Some(msg.id),
                msg.author.clone(),
                args,
            );
            paste::paste!{
                [<$name _com_ctx>](&context).await
            }
        }
        paste::paste!{
            pub static [<$name:upper _SLASH_ARGS_ENABLED>]: bool = true;
            pub fn [<$name _com_ctx>]<'fut>(ctx: &'fut$crate::commands::CommandContext) -> futures::future::BoxFuture<'fut, CommandResult> {
                use futures::future::FutureExt;
                async move {
                    let res: CommandResult = {
                        $crate::maybe_result!(ctx.ctx(), ctx.channel_id(), ctx.ref_msg(), $callback(&ctx).await);
                        Ok(())
                    };
                    res
                }.boxed()
            }
        }
    };
}

/*
#[macro_export]
macro_rules! crab_group {
    ($group:ident, $($command:ident),+) => {
        paste::paste! {
            pub static [<$group:upper _CRAB_GROUP>]: $crate::commands::CrabGroup  = $crate::commands::CrabGroup::new(
                [<$group:upper _GROUP>],
                &[

                    $(
                        [<$command _com_ctx>],
                    )+
                ],
            );
        }
    }
}
*/

/*#[macro_export]
macro_rules! allowed {
    ($c:expr, $m:expr, $cmd:block) => {
        if $crate::utils::check_channel_allowed($c, $m).await {
            $cmd
            Ok(())
        } else {
            Ok(())
        }
    };
}*/
