use super::{api::WFApi, *};
use crate::{commands::CommandContext, error::CommandHandleResult, message_box::CrabMessage};
use anyhow::Context;
use chrono::Utc;

pub async fn output_sortie(ctx: &CommandContext) -> CommandHandleResult {
    // get Sortie data
    let api = WFApi::new();
    let sortie = api
        .world()
        .await
        .context("Failed to load world data!")?
        .sortie();
    let mut sortie_vector = Vec::new();
    //calculate timeleft
    let d1 = Utc::now();
    let d2 = sortie.expiry.unwrap();
    let duration = d2.signed_duration_since(d1);
    let timeleft = world::eval_duration(duration);
    //get all data for sortie
    let mut x = 1;
    for mission in sortie.variants {
        let mut enemy_level = "";
        match x {
            1 => enemy_level = "50-60",
            2 => enemy_level = "65-80",
            3 => enemy_level = "80-100",
            _ => {}
        }
        sortie_vector.push((
            mission.location,
            "● Enemy level: ".to_string()
                + enemy_level
                + "\n● Mission type: "
                + &mission.name
                + "\n● "
                + &mission.modifier
                + "\n➔ "
                + &mission.description,
            false,
        ));
        x += 1;
    }
    //send the message
    CrabMessage::from_ctx(ctx)
        .embed(|e| {
            e.title("Current sortie".to_string());
            e.fields(sortie_vector);
            e.footer(|f| {
                f.text("Time left: ".to_string() + timeleft.as_str());

                f
            });
            e
        })
        .reference(ctx.ref_msg())
        .message()
        .await?;
    Ok(())
}
