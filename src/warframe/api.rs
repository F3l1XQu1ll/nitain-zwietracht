use crate::utils::do_fetch;

pub use crab_model::warframe::*;

#[async_trait]
pub trait InsertMissionTypeFromNodeStr {
    async fn insert_mission_type(&mut self) -> anyhow::Result<()>;
}

pub struct WFApi {}

impl WFApi {
    /// Initialize a new WFApi instance
    pub fn new() -> Self {
        Self {}
    }

    /// Fetch warframestat.us api
    async fn load(&self) -> anyhow::Result<String> {
        do_fetch(WFAPI_WORLD_URL.to_string()).await
    }

    /// Search for a (missuion) Node
    pub async fn load_node(node: &str) -> anyhow::Result<WFNode> {
        let json_str = do_fetch(WFAPI_NODES_URL.to_string() + &urlencoding::encode(node)).await?;
        let json_value: serde_json::Value = serde_json::from_str(&json_str)?;
        let node_value = json_value[0]["nodes"][0].clone();
        let node = serde_json::from_value::<WFNode>(node_value)?;
        Ok(node)
    }

    /// API request for worldstate
    pub async fn world(&self) -> anyhow::Result<WFWorld> {
        let world = serde_json::from_str::<WFWorld>(self.load().await?.as_str())?;
        Ok(world)
    }
}

macro_rules! impl_InsertMissionTypeFromNodeStr {
    (for $($t:ty),+) => {
        $(#[async_trait]
            impl InsertMissionTypeFromNodeStr for $t {
                /// fill the `mission_type` field of self with the corresponding mission type of the corresponding node
                async fn insert_mission_type(&mut self) -> anyhow::Result<()> {
                    let node_str = self.node.as_str();
                    let node = WFApi::load_node(node_str).await?;
                    self.mission_type = Some(node.mission_type);
                    Ok(())
                }
            }
        )*
    };
}

impl_InsertMissionTypeFromNodeStr!(for WFArbitration);

#[tokio::test]
async fn test_wf_api_world() {
    let api = WFApi::new();
    let world = api.world().await;
    dbg!(&world);
    assert!(world.is_ok());
}
