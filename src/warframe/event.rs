use super::{api::WFApi, *};
use crate::{commands::CommandContext, error::CommandHandleResult, message_box::CrabMessage};
use anyhow::Context;
use chrono::Utc;

pub async fn output_events(ctx: &CommandContext) -> CommandHandleResult {
    //get all data for events
    let events = WFApi::new()
        .world()
        .await
        .context("Failed to load world state")?
        .events();
    let mut event_vector = Vec::new();
    for event in events {
        let name = &event.description;
        let description = &event.tooltip;
        //now calculate the time
        let d1 = Utc::now();
        let d2 = event.expiry.unwrap();
        let duration = d2.signed_duration_since(d1);
        let timeleft = world::eval_duration(duration);
        event_vector.push((
            "● ".to_string() + name,
            description.to_string() + "\nTime left: " + timeleft.as_str(),
            false,
        ));
    }
    //send the message
    CrabMessage::from_ctx(ctx)
        .embed(|e| {
            e.title("Current events in Warframe".to_string());
            e.fields(event_vector)
        })
        .reference(ctx.ref_msg())
        .message()
        .await?;
    Ok(())
}
