use chrono::{Duration, Utc};

use crate::{
    commands::CommandContext,
    error::{CommandHandleResult, LogError},
    message_box::CrabMessage,
};

use super::api::{InsertMissionTypeFromNodeStr, WFApi, WFCycleActive, WFFissure, WFSyndicate};

pub fn eval_duration(duration: Duration) -> String {
    let seconds = duration.num_seconds() % 60;
    let minutes = duration.num_minutes() % 60;
    let hours = duration.num_hours() % 24;
    let days = duration.num_days();
    let mut timeleft = "".to_string();
    if days != 0 {
        timeleft += &(days.to_string() + "d ");
    }
    if hours != 0 {
        timeleft += &(hours.to_string() + "h ");
    }
    if minutes != 0 {
        timeleft += &(minutes.to_string() + "m ");
    }
    if seconds != 0 {
        timeleft += &(seconds.to_string() + "s");
    }
    timeleft = timeleft.trim().to_string();
    timeleft
}

/// Load fissure data
pub async fn fissures() -> Vec<WFFissure> {
    let mut fissures = WFApi::new()
        .world()
        .await
        .expect("Failed to load worldstate")
        .fissures();

    fissures.sort_by(|f1, f2| f1.tier_num.cmp(&f2.tier_num));
    fissures
}

/// Answer fissures
pub async fn fissure_output(ctx: &CommandContext) {
    let fissures = fissures().await;
    let mut fissures_fields: Vec<(String, String, bool)> = Vec::new();
    let mut storms_fields: Vec<(String, String, bool)> = Vec::new();
    let mut time;
    let link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/void-traces.png";
    let d1 = Utc::now();
    for each in fissures {
        let duration = each.expiry.unwrap().signed_duration_since(d1);
        time = eval_duration(duration);
        //  let u = "Tier: ".to_string() + tier.clone() + "\n" + "Type: " + missiontype.clone() + "\n" + "Time left: " + time.clone().as_str();
        let data = (
            each.node,
            ("Tier: ".to_string()
                + &each.tier
                + "\n"
                + "Type: "
                + &each.mission_type
                + "\n"
                + "Time left: "
                + time.as_str()),
            true,
        );
        if each.is_storm {
            storms_fields.push(data);
        } else {
            fissures_fields.push(data);
        }

        //v.push((location, tier, missiontype, time.as_str()));
        //("Kiste (Ceres)","Meso","Mobile Defense","1h 23m 4s")
    }

    let world_link = "https://void-crab.herokuapp.com/world";
    CrabMessage::from_ctx(ctx)
        .rename("Fissures")
        .title("Current fissure missions")
        .fields(fissures_fields)
        .image(link)
        .link(world_link)
        .page("Void Storms")
        .title("Current void-storm missions")
        .fields(storms_fields)
        .image(link)
        .link(world_link)
        .message()
        .await
        .handle();
}

/// Load state for baro and return info for answer
pub async fn baro_kiteer() -> Vec<(String, String, bool)> {
    let baro_info = WFApi::new()
        .world()
        .await
        .expect("Failed to load world state")
        .baro();
    let mut v: Vec<(String, String, bool)> = Vec::new();
    let location = baro_info.location;

    if !baro_info.active {
        let duration = baro_info
            .activation
            .unwrap()
            .signed_duration_since(Utc::now());
        let timeleft = eval_duration(duration);
        v.push(("Baro will arrive in:".to_string(), timeleft, false));
        v.push(("Location".to_string(), location.unwrap(), true));
    } else if baro_info.active {
        let duration = baro_info.expiry.unwrap().signed_duration_since(Utc::now());
        let timeleft = eval_duration(duration);
        v.push(("Location".to_string(), location.unwrap(), true));
        v.push(("Baro will depart in:".to_string(), timeleft, true));
        v.push(("\u{200B}".to_string(), ("\u{200B}").to_string(), true));
        for item in baro_info.inventory {
            let itemname = item.item.to_string();
            let ducatprice = item.ducats.to_string();
            let creditprice = item.credits.to_string();
            v.push((
                itemname,
                "Ducats: ".to_string()
                    + ducatprice.as_str()
                    + "\n"
                    + "Credits: "
                    + creditprice.as_str(),
                true,
            ));
        }
    }
    v
}

pub async fn output_baro(ctx: &CommandContext) -> CommandHandleResult {
    let strings = baro_kiteer().await;
    let link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/baro-ki%27teer-glyph.png";
    let mut long_title = "".to_string();
    for _i in 0..256 {
        long_title = (long_title + " ").to_string();
    }

    if strings.len() <= 25 {
        CrabMessage::from_ctx(ctx)
            .title("Baro Ki'Teer")
            .image(link)
            .fields(strings)
            .message()
            .await?;
    } else {
        let mut builder = CrabMessage::from_ctx(ctx);
        builder.rename("Baro Ki'Teer").title("Baro Ki'Teer");
        for (index, chunk) in strings.chunks(15).enumerate() {
            let bla = chunk.to_owned();
            if index != 0 {
                builder
                    .page(format!("Baro Ki'Teer, page {}", index + 1))
                    .title(format!("Baro Ki'Teer, page {}", index + 1));
            }
            builder.fields(bla);
            builder.image(link);
        }
        builder.message().await?;
    }
    Ok(())
}

/// Load and display info on current arbitration(s)
pub async fn arbitration_fetch(ctx: &CommandContext) -> CommandHandleResult {
    let mut arbitration = WFApi::new().world().await?.arbitration();
    arbitration.insert_mission_type().await?;
    let mut embed_fields: Vec<(String, String, bool)> = Vec::new();
    let d1 = Utc::now();
    let d2 = arbitration.expiry.unwrap();
    let duration = d2.signed_duration_since(d1);
    let timeleft = eval_duration(duration);
    embed_fields.push((
        "Mission type".to_string(),
        arbitration.mission_type.unwrap(),
        false,
    ));
    embed_fields.push(("Location".to_string(), arbitration.node, false));
    embed_fields.push(("Enemy".to_string(), arbitration.enemy.to_string(), false));
    embed_fields.push(("Time".to_string(), timeleft + " left", false));

    crate::message_box::send_msg(ctx.clone(), "Arbitration", embed_fields, "https://static.wikia.nocookie.net/warframe/images/8/86/VitusEmblem.png/revision/latest/scale-to-width-down/120?cb=20210327220148",  Some("https://void-crab.herokuapp.com/world".to_string())).await;
    Ok(())
}

/// Generate bounty info for output from world data
pub async fn get_bounties(
    world: String,
    syndicates: Vec<WFSyndicate>,
) -> Vec<(String, String, bool)> {
    /* let json = (do_fetch("https://api.warframestat.us/pc"))
        .await
        .unwrap_or_default();
    let parsed_json = json::parse(json.as_str()).unwrap_or_else(|_| json::object! {}); */
    let mut bounties = Vec::new();
    let mut world_syndicate = "";

    if world == "cetus" {
        world_syndicate = "Ostrons"
    } else if world == "fortuna" {
        world_syndicate = "Solaris United"
    } else if world == "deimos" {
        world_syndicate = "Entrati"
    }
    for syndicate in syndicates {
        if syndicate.syndicate == world_syndicate {
            // let mut timeleft: String;
            /* let d1 = Utc::now();
            let d2 = Utc
                .datetime_from_str(
                    syndicate["expiry"].as_str().unwrap(),
                    "%Y-%m-%dT%H:%M:%S.%3fZ",
                )
                .unwrap();
            let duration = d2.signed_duration_since(d1);
            timeleft = eval_duration(duration); */
            // timeleft.push_str("\nRewards:");

            for job in &syndicate.jobs {
                let bounty_name = job.r#type.clone();
                if bounty_name.contains("Isolation") {
                    continue;
                } else {
                    let mut timeleft = "Level: ".to_string();

                    timeleft.push_str(job.enemy_levels[0].to_string().as_str());
                    timeleft.push('-');
                    timeleft.push_str(job.enemy_levels[1].to_string().as_str());
                    timeleft.push_str("\n----------------\nRewards:");

                    let mut rewards = job.rewards.clone();
                    rewards.sort_unstable();
                    for bla in rewards {
                        timeleft.push_str("\n- ");
                        timeleft.push_str(&bla);
                    }
                    bounties.push((bounty_name, timeleft.clone(), true));
                }
            }
        }
    }

    bounties
}

/// Answer worldstate including timers and bounties
pub async fn world_output(ctx: &CommandContext, world_name: String) {
    let world_info = WFApi::new()
        .world()
        .await
        .expect("Failed to load world state");
    let world = match world_name.as_str() {
        "cetus" => world_info.cycles().cetus,
        "fortuna" => world_info.cycles().fortuna,
        "deimos" => world_info.cycles().necralisk,
        _ => unreachable!(),
    };
    let state = world.active;
    let duration = world.expiry.unwrap().signed_duration_since(Utc::now());
    let timeleft = eval_duration(duration);
    let mut link = "";
    let mut world_output_name = "";
    if world_name == "cetus" {
        world_output_name = "Plains of Eidolon";
        link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/plains-of-eidolon-scene.png";
    }
    if world_name == "deimos" {
        world_output_name = "Cambion Drift";
        link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/deimos-cambion-drift-scene.png";
    }
    if world_name == "fortuna" {
        world_output_name = "Orb Vallis";
        link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/orb-vallis-scene.png";
    }
    let opposite_state = match &state {
        WFCycleActive::Day => WFCycleActive::Night,
        WFCycleActive::Night => WFCycleActive::Day,
        WFCycleActive::Cold => WFCycleActive::Warm,
        WFCycleActive::Warm => WFCycleActive::Cold,
        WFCycleActive::Fass => WFCycleActive::Vome,
        WFCycleActive::Vome => WFCycleActive::Fass,
    };
    let bounties = get_bounties(world_name.clone(), world_info.syndicates()).await;
    let mut strings = vec![
        ("Current state:".to_string(), state.to_string(), true),
        (
            "Time left:".to_string(),
            (timeleft + " to " + &opposite_state.to_string()),
            false,
        ),
    ];
    for bounty in bounties {
        strings.push(bounty);
    }

    crate::message_box::send_msg(
        ctx.clone(),
        world_output_name,
        strings,
        link,
        Some("https://void-crab.herokuapp.com/world".to_string()),
    )
    .await;
}

macro_rules! world_handler {
    () => {};
    ($world:ident, $($tail:tt)*) => {
        pub async fn $world(ctx: &CommandContext) -> $crate::error::CommandHandleResult {
            world_output(ctx, stringify!($world).to_string()).await;
            Ok(())
        }
        world_handler!($($tail)*);
    };
}

world_handler! {
    cetus,
    deimos,
    fortuna,
}
