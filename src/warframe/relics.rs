use crate::utils::do_fetch;
use select::{document::Document, predicate::Name};

pub async fn get_relic_wiki(item: String) -> String {
    do_fetch("https://warframe.fandom.com/wiki/".to_string() + item.as_str())
        .await
        .unwrap_or_default()
}

pub async fn get_relic_contents(document_str: String) -> Vec<std::string::String> {
    let mut grabbed_items = Vec::new();

    let document = Document::from(document_str.as_str());
    //grabbed_items.push("https://warframe.fandom.com/wiki/".to_string() + item.as_str());
    //grabbed_items.push("bla".to_string());

    for node in document.find(Name("td")) {
        if node.attr("style").unwrap_or_default() == "padding:0 5px 0 0; height: 32px;" {
            //grabbed_items.push(("yes").to_string());
            //until now the code is working, it finds 6 nodes
            //println!("last child: '{:?}'", node.last_child().unwrap().text());
            let last_a_child = node.find(Name("a")).last().unwrap();
            grabbed_items.push(last_a_child.text());
        }
    }
    //grabbed_items.push((grabbed_items.len()).to_string());
    grabbed_items
}

pub async fn relic_content_ducats(document_str: String) -> Vec<std::string::String> {
    let mut grabbed_ducats = Vec::new();
    let document = Document::from(document_str.as_str());
    for node in document.find(Name("span")) {
        if node.attr("class").unwrap_or_default() == "mobile-hidden" {
            grabbed_ducats.push(node.text());
        }
    }

    grabbed_ducats
}
