use super::*;
use crate::{
    commands::CommandContext, error::CommandHandleResult, message_box::CrabMessage, utils::do_fetch,
};
use inflections::Inflect;

pub async fn load_market_prices_json(item: String) -> String {
    do_fetch("https://api.warframe.market/v1/items/".to_string() + item.as_str() + "/orders")
        .await
        .unwrap_or_default()
}

/// Extract price information from warframe.market
///
/// Use [load_market_prices_json] to obtain the price information for a specific item.
pub async fn scrape_market_prices(json: String, sell: bool) -> Option<String> {
    //println!("{}", json);
    let parsed_json = json::parse(json.as_str()).unwrap_or_else(|_| json::object! {});
    let mut prices = Vec::new();
    for order in parsed_json["payload"]["orders"].members() {
        if order["user"]["status"].as_str().unwrap_or_default() == "ingame" {
            match order["mod_rank"].as_i32().unwrap_or(-1) {
                -1 => {
                    if order["order_type"].as_str().unwrap_or_default() == "sell" && sell
                        || order["order_type"].as_str().unwrap_or_default() == "buy" && !sell
                    {
                        let price = order["platinum"].as_f32().unwrap_or(-1.0);
                        prices.push(price);
                    }
                }
                _ => {
                    if order["mod_rank"].as_i32().unwrap_or_default() == 0
                        && (order["order_type"].as_str().unwrap_or_default() == "sell" && sell
                            || order["order_type"].as_str().unwrap_or_default() == "buy" && !sell)
                    {
                        let price = order["platinum"].as_f32().unwrap_or(-1.0);
                        prices.push(price);
                    }
                }
            }
        }
    }
    prices.sort_by(|a, b| a.partial_cmp(b).unwrap());

    if prices.is_empty() {
        return None;
    }

    let lowest_price = prices[0];
    prices.sort_by(|a, b| b.partial_cmp(a).unwrap());
    let highest_price = prices[0];
    let mut price_sum: f32 = -1.0;
    for price in prices.clone() {
        price_sum += price;
    }
    if prices.is_empty() {
        price_sum = 0.0;
    } else {
        price_sum /= prices.len() as f32;
    }
    Some(
        price_sum.round().to_string()
            + "%"
            + lowest_price.round().to_string().as_str()
            + "%"
            + highest_price.round().to_string().as_str(),
    )
}

pub async fn ducat_price_check(item: String) -> i32 {
    let json = (do_fetch("https://api.warframe.market/v1/items/".to_string() + item.as_str()))
        .await
        .unwrap_or_default();
    let parsed_json = json::parse(json.as_str()).unwrap_or_else(|_| json::object! {});
    let mut ducats = 0;
    for set_item_name in parsed_json["payload"]["item"]["items_in_set"].members() {
        if set_item_name["url_name"].as_str().unwrap_or_default() == item {
            ducats = set_item_name["ducats"].as_i32().unwrap_or(0);
        }
    }
    ducats
}

pub async fn try_load_priceinfo(ctx: CommandContext) -> CommandHandleResult {
    let args = ctx.args();
    let items: Vec<&str> = args.raw().collect::<Vec<&str>>();
    for item in items {
        let mut item_in_list = item
            .trim()
            .to_lowercase()
            .replace("bp", "blueprint")
            .replace('+', "")
            .trim()
            .replace(' ', "_")
            .to_string();
        if item_in_list
            .to_lowercase()
            .trim()
            .ends_with(|c: char| c.is_ascii_digit())
        {
            item_in_list += "_relic";
        }
        if item_in_list.to_lowercase().trim().ends_with("prime") {
            item_in_list += "_set";
        }
        if item_in_list.to_lowercase().trim().ends_with("neuroptic") {
            item_in_list += "s";
        }
        let market_json = load_market_prices_json(item_in_list.clone()).await;
        let sell_price_string = scrape_market_prices(market_json.clone(), true)
            .await
            .unwrap_or_default();
        let sell_price_vec = sell_price_string.split('%').collect::<Vec<&str>>();
        let mut sell_price = sell_price_vec[0].to_string() + " platinum";
        let mut lowest_sell_price = sell_price_vec.get(1).unwrap_or(&"").to_string() + " platinum";
        let buy_price_string = scrape_market_prices(market_json.clone(), false)
            .await
            .unwrap_or_default();
        let buy_price_vec = buy_price_string.split('%').collect::<Vec<&str>>();
        let mut buy_price = buy_price_vec[0].to_string() + " platinum";
        let mut highest_buy_price = buy_price_vec.get(2).unwrap_or(&"").to_string() + " platinum";
        let mut ducat_price = ducat_price_check(item_in_list.clone()).await;
        if lowest_sell_price == "".to_string() + " platinum" {
            lowest_sell_price = "No sell prices available.".to_string();
            sell_price = "No sell prices available.".to_string();
        }
        if highest_buy_price == "".to_string() + " platinum" {
            highest_buy_price = "No buy prices available.".to_string();
            buy_price = "No buy prices available.".to_string();
        }
        if lowest_sell_price == *"No sell prices available."
            && highest_buy_price == *"No buy prices available."
        {
            // Try with "blueprint" and overwrite old price data
            item_in_list += "_blueprint";
            let market_json = load_market_prices_json(item_in_list.clone()).await;
            let sell_price_string = scrape_market_prices(market_json.clone(), true)
                .await
                .unwrap_or_default();
            let sell_price_vec = sell_price_string.split('%').collect::<Vec<&str>>();
            sell_price = sell_price_vec[0].to_string() + " platinum";
            lowest_sell_price = sell_price_vec.get(1).unwrap_or(&"").to_string() + " platinum";
            let buy_price_string = scrape_market_prices(market_json.clone(), false)
                .await
                .unwrap_or_default();
            let buy_price_vec = buy_price_string.split('%').collect::<Vec<&str>>();
            buy_price = buy_price_vec[0].to_string() + " platinum";
            highest_buy_price = buy_price_vec.get(2).unwrap_or(&"").to_string() + " platinum";
            ducat_price = ducat_price_check(item_in_list.clone()).await;
            if lowest_sell_price == "".to_string() + " platinum" {
                lowest_sell_price = "No sell prices available.".to_string();
                sell_price = "No sell prices available.".to_string();
            }
            if highest_buy_price == "".to_string() + " platinum" {
                highest_buy_price = "No buy prices available.".to_string();
                buy_price = "No buy prices available.".to_string();
            }
        }
        if lowest_sell_price == *"No sell prices available."
            && highest_buy_price == *"No buy prices available."
        {
            CrabMessage::from_ctx(&ctx)
                .title(
                    "Sorry, I can't find any prices for: ".to_string()
                        + item_in_list
                            .replace("_blueprint", "")
                            .replace('_', " ")
                            .to_train_case()
                            .replace('-', " ")
                            .as_str(),
                )
                .reference(ctx.ref_msg())
                .message()
                .await?;
        } else if item_in_list.to_lowercase().contains("relic") {
            let relic_string = item_in_list
                .clone()
                .to_uppercase()
                .replace("_RELIC", " ")
                .trim()
                .replace("EO", "eo")
                .replace("ITH", "ith")
                .replace("XI", "xi")
                .replace("ESO", "eso")
                .replace("EQUIEM", "equiem")
                .to_string();

            //let relic_wiki = get_relic_wiki(relic_string.clone()).await;
            let relic_wiki = relics::get_relic_wiki(relic_string.clone()).await;
            let relic_content = relics::get_relic_contents(relic_wiki.clone()).await;
            let relic_content_ducats = relics::relic_content_ducats(relic_wiki.clone()).await;
            let relic_strings_for_link: Vec<&str> = relic_string.split('_').collect();
            let link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/"
                .to_string()
                + relic_strings_for_link[0].to_lowercase().as_str()
                + "-intact.png";
            let strings = vec![
                (
                    "average sell price:".to_string(),
                    sell_price.to_string(),
                    true,
                ),
                (
                    "lowest sell price:".to_string(),
                    lowest_sell_price.to_string(),
                    true,
                ),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                (
                    "average buy price:".to_string(),
                    buy_price.to_string(),
                    true,
                ),
                (
                    "highest buy price:".to_string(),
                    highest_buy_price.to_string(),
                    true,
                ),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                (
                    "ducat price:".to_string(),
                    ducat_price.to_string() + " ducats",
                    true,
                ),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                (
                    relic_content[0].clone(),
                    "Rarity: Common\nDucats: ".to_string() + relic_content_ducats[0].as_str(),
                    true,
                ),
                (
                    relic_content[1].clone(),
                    "Rarity: Common\nDucats: ".to_string() + relic_content_ducats[1].as_str(),
                    true,
                ),
                (
                    relic_content[2].clone(),
                    "Rarity: Common\nDucats: ".to_string() + relic_content_ducats[2].as_str(),
                    true,
                ),
                (
                    relic_content[3].clone(),
                    "Rarity: Uncommon\nDucats: ".to_string() + relic_content_ducats[3].as_str(),
                    true,
                ),
                (
                    relic_content[4].clone(),
                    "Rarity: Uncommon\nDucats: ".to_string() + relic_content_ducats[4].as_str(),
                    true,
                ),
                (
                    relic_content[5].clone(),
                    "Rarity: Rare\nDucats: ".to_string() + relic_content_ducats[5].as_str(),
                    true,
                ),
            ];

            crate::message_box::send_msg(
                ctx.clone(),
                item_in_list
                    .replace('_', " ")
                    .to_train_case()
                    .replace('-', " ")
                    .as_str(),
                strings,
                link.as_str(),
                Some("https://warframe.market/items/".to_string() + &item_in_list),
            )
            .await;
        } else {
            let mut primestring = "";
            let link;
            let strings_for_link: Vec<&str> = item_in_list.split('_').collect();
            if item_in_list.contains("set") {
                if item_in_list.contains("prime") {
                    primestring = "-prime";
                }
                link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/"
                    .to_string()
                    + strings_for_link[0]
                    + primestring + ".png";
            } else {
                if item_in_list.contains("prime") {
                    primestring = "prime-";
                }
                link = "https://raw.githubusercontent.com/wfcd/warframe-items/development/data/img/"
                    .to_string()
                    + primestring
                    + strings_for_link.last().cloned().unwrap() + ".png";
            }

            let strings = vec![
                (
                    "average sell price:".to_string(),
                    sell_price.to_string(),
                    true,
                ),
                (
                    "lowest sell price:".to_string(),
                    lowest_sell_price.to_string(),
                    true,
                ),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                (
                    "average buy price:".to_string(),
                    buy_price.to_string(),
                    true,
                ),
                (
                    "highest buy price:".to_string(),
                    highest_buy_price.to_string(),
                    true,
                ),
                ("\u{200B}".to_string(), ("\u{200B}").to_string(), true),
                (
                    "ducat price:".to_string(),
                    ducat_price.to_string() + " ducats",
                    false,
                ),
            ];

            crate::message_box::send_msg(
                ctx.clone(),
                item_in_list
                    .replace('_', " ")
                    .to_train_case()
                    .replace('-', " ")
                    .as_str(),
                strings,
                link.as_str(),
                Some("https://warframe.market/items/".to_string() + &item_in_list),
            )
            .await;
        }
    }
    Ok(())
}
