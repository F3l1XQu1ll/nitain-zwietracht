use super::db::DBStorage;
use crate::{commands::CommandContext, CrabData};
use anyhow::Result;
use serenity::client::Context;
use std::sync::Arc;
use tokio::sync::{RwLock, RwLockReadGuard, RwLockWriteGuard};

#[async_trait]
pub trait ContextStorageManager {
    async fn storage<'a>(&'a self) -> StorageManager;
}

#[derive(Clone)]
pub struct StorageManager {
    crab_data_lock: Arc<RwLock<CrabData>>,
}

impl StorageManager {
    pub async fn new(ctx: &Context) -> Self {
        let context_typemap_lock = ctx.data.read().await;
        let crab_data_lock = context_typemap_lock
            .get::<CrabData>()
            .expect("Expected CrabData in typemap.")
            .clone();
        Self { crab_data_lock }
    }

    async fn write(&mut self) -> RwLockWriteGuard<'_, CrabData> {
        self.crab_data_lock.write().await
    }

    pub async fn update<F, T>(&mut self, mut update_fn: F) -> Result<T, anyhow::Error>
    where
        F: FnMut(RwLockWriteGuard<'_, CrabData>) -> T,
    {
        let write_lock = self.write().await;
        Ok(update_fn(write_lock))
        //self.commit().await
    }

    pub async fn read(&self) -> RwLockReadGuard<'_, CrabData> {
        self.crab_data_lock.read().await
    }

    pub async fn commit(&mut self) -> Result<()> {
        let conn = DBStorage::new().await?;
        let data = self.read().await.clone();
        conn.dump_storage(data).await?;
        Ok(())
    }
}

#[async_trait]
impl ContextStorageManager for Context {
    async fn storage<'a>(&'a self) -> StorageManager {
        StorageManager::new(self).await
    }
}

#[async_trait]
impl ContextStorageManager for CommandContext {
    async fn storage<'a>(&'a self) -> StorageManager {
        self.ctx().storage().await
    }
}
