#[macro_export]
macro_rules! commit {
    ($manager:expr, $write_use: block) => {{
        $write_use
        $manager.commit().await
    }};
}
