use serde::Serialize;
use std::path::Path;
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt},
};

#[allow(dead_code)]
pub async fn load_storage<T>(location: String) -> Option<T>
where
    T: serde::de::DeserializeOwned,
{
    let path = Path::new(&location); // FIXME Use appdata and not release for that?
    if path.exists() {
        if let Ok(mut file) = File::open(path).await {
            let mut content = String::new();
            file.read_to_string(&mut content).await.unwrap_or_default();
            let content_clone = content.clone();
            let serde_res = serde_json::from_str::<T>(content_clone.as_ref());
            if let Ok(serde_res) = serde_res {
                return Some(serde_res);
            } else {
                return None;
            }
        }
    }
    None
}

#[allow(dead_code)]
pub async fn dump_storage<U>(location: String, data: U)
where
    U: Sized + Serialize,
{
    if let Ok(json) = serde_json::to_string(&data) {
        let path = Path::new(&location);
        if let Ok(mut file) = File::create(path).await {
            if let Err(e) = file.write(json.as_bytes()).await {
                eprintln!("{:?}", e);
            }
        }
    }
}
