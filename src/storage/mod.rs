use serde::Serialize;

pub mod crab_data;
pub mod db;
mod files;
mod macros;
pub mod manager;

#[allow(dead_code)]
#[deprecated]
pub async fn load_storage<T>(location: String) -> Option<T>
where
    T: serde::de::DeserializeOwned,
{
    files::load_storage(location).await
}

#[deprecated]
#[allow(dead_code)]
pub async fn dump_storage<U>(location: String, data: U)
where
    U: Sized + Serialize,
{
    files::dump_storage(location, data).await
}
