use crate::{auth::CrabToken, music::queue::LenAndSwap};
use serde::{Deserialize, Serialize};
use serenity::{
    model::id::{ChannelId, GuildId, MessageId, UserId},
    prelude::TypeMapKey,
};
use std::{
    borrow::BorrowMut,
    collections::{vec_deque::Drain, HashMap, VecDeque},
    ops::RangeBounds,
    sync::{atomic::AtomicU64, Arc},
    time::Duration,
};
use tokio::sync::RwLock;

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
#[serde(default)]
pub struct TrackEntry {
    pub title: String,
    pub artist: String,
    pub url: Option<String>,
    pub enqueued: bool,
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
#[serde(default)]
/// Wrapper for a TrackEntry with an unique id.
pub struct UniqueTrackEntry {
    // Readonly!
    id: u64,
    pub title: String,
    pub artist: String,
    pub url: Option<String>,
    pub enqueued: bool,
}

pub trait TrackEntryAsUnique {
    fn into_unique(self, id_source: Arc<AtomicU64>) -> UniqueTrackEntry;
}

#[derive(Clone, Default, Serialize, Deserialize, Debug)]
#[serde(from = "DirtyQueue")]
pub struct CrabQueue {
    id_counter: Arc<AtomicU64>,
    tracks: VecDeque<UniqueTrackEntry>,
    played_tracks: Vec<UniqueTrackEntry>,
}

#[derive(Default, Deserialize)]
#[serde(default)]
/// A version of CrabQueue with tracks that may not be unique yet.
///
/// Reason:
/// Serde has no verify or finalize functionality (yet).
/// See https://github.com/serde-rs/serde/issues/642
struct DirtyQueue {
    id_counter: Arc<AtomicU64>,
    tracks: VecDeque<UniqueTrackEntry>,
    played_tracks: Vec<UniqueTrackEntry>,
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
#[serde(default)]
pub struct ServerStorage {
    pub autoplay_state: bool,
    pub autoplay_list: Vec<String>,
    /// Stores the text channel where the last play command came from
    pub player_interaction: Option<serenity::model::id::ChannelId>,
    pub loop_state: bool,
    pub paused_state: bool,
    /// Stores the voice channel where the bot is playing
    pub playing_in: Option<u64>,
    /// Songs in queue, this is **not** songbirds queue!
    pub queue: CrabQueue,
    /// This is the first time the server is handled by the Action Queue
    pub first_launch: bool,
    /// Stores the position of the currently playing song. Only populated on shutdonw!
    pub track_position: Option<Duration>,
}

#[derive(Default, Clone, Serialize, Deserialize)]
#[serde(default)]
pub struct CrabConfig {
    allowed_channels: HashMap<u64, bool>,
    // For guilds with allowed_channels
    allowed_guilds: HashMap<u64, bool>,
    dota_uids: HashMap<u64, u64>,
    // list of pro accounts a user is following
    dota_watching: HashMap<UserId, Vec<u64>>,
    // Buttons message id by channel id
    buttons: HashMap<u64, u64>,
    // Queue page by channel id
    queue_page: HashMap<u64, usize>,
    // Queue message id by channel id
    queue_message: HashMap<u64, u64>,
    tokens: HashMap<u64, CrabToken>,
}

#[derive(Default, Clone, Serialize, Deserialize)]
#[serde(default)]
pub struct CrabData {
    kv: HashMap<u64, ServerStorage>,
    config: CrabConfig,
}

impl TypeMapKey for CrabData {
    type Value = Arc<RwLock<CrabData>>;
}

impl CrabData {
    pub fn config(&self) -> &CrabConfig {
        &self.config
    }

    pub fn config_mut(&mut self) -> &mut CrabConfig {
        self.config.borrow_mut()
    }

    /// Get a reference to the crab data's kv.
    pub fn kv(&self) -> &HashMap<u64, ServerStorage> {
        &self.kv
    }

    /// Get a mutable reference to the crab data's kv.
    pub fn kv_mut(&mut self) -> &mut HashMap<u64, ServerStorage> {
        &mut self.kv
    }
}

impl CrabConfig {
    pub fn allowed_channels(&self) -> &HashMap<u64, bool> {
        &self.allowed_channels
    }

    pub fn allowed_guilds(&self) -> &HashMap<u64, bool> {
        &self.allowed_guilds
    }

    pub fn allow_channel(&mut self, channel_id: ChannelId, allowed: bool) -> Option<bool> {
        self.allowed_channels.insert(*channel_id.as_u64(), allowed)
    }

    pub fn allow_guild(&mut self, guild_id: GuildId, allowed: bool) -> Option<bool> {
        self.allowed_guilds.insert(*guild_id.as_u64(), allowed)
    }

    pub fn buttons(&self) -> &HashMap<u64, u64> {
        &self.buttons
    }

    pub fn insert_button(&mut self, channel_id: ChannelId, message_id: MessageId) -> Option<u64> {
        self.buttons
            .insert(*channel_id.as_u64(), *message_id.as_u64())
    }

    pub fn dota_uids(&self) -> &HashMap<u64, u64> {
        &self.dota_uids
    }

    pub fn dota_uids_mut(&mut self) -> &mut HashMap<u64, u64> {
        self.dota_uids.borrow_mut()
    }

    pub fn queue_page(&self) -> &HashMap<u64, usize> {
        &self.queue_page
    }

    pub fn queue_page_mut(&mut self) -> &mut HashMap<u64, usize> {
        self.queue_page.borrow_mut()
    }

    pub fn queue_message(&self) -> &HashMap<u64, u64> {
        &self.queue_message
    }

    /// Get a mutable reference to the crab config's queue message.
    pub fn queue_message_mut(&mut self) -> &mut HashMap<u64, u64> {
        &mut self.queue_message
    }

    /// Get a reference to the crab config's tokens.
    pub fn tokens(&self) -> &HashMap<u64, CrabToken> {
        &self.tokens
    }

    /// Get a mutable reference to the crab config's tokens.
    pub fn tokens_mut(&mut self) -> &mut HashMap<u64, CrabToken> {
        &mut self.tokens
    }

    /// Get a reference to the crab config's dota watch list.
    pub fn dota_watching(&self) -> &HashMap<UserId, Vec<u64>> {
        &self.dota_watching
    }

    /// Get a mutable reference to the crab config's dota watch list.
    pub fn dota_watching_mut(&mut self) -> &mut HashMap<UserId, Vec<u64>> {
        &mut self.dota_watching
    }
}

impl CrabQueue {
    pub fn clear(&mut self) {
        self.tracks.clear();
        self.played_tracks.clear();
    }

    /// Insert next track into track history and return this track if available.
    pub fn next(&mut self) -> Option<UniqueTrackEntry> {
        let next = self.tracks.pop_front();
        if let Some(mut next) = next {
            next.enqueued = false;
            self.played_tracks.push(next.clone());
            Some(next)
        } else {
            None
        }
    }

    /// Sets the previous track as next track and returns this track if available.
    pub fn prev(&mut self) -> Option<UniqueTrackEntry> {
        if let Some(current) = self.tracks.front_mut() {
            current.enqueued = false;
        }
        let prev = self.played_tracks.pop();
        if let Some(prev) = prev {
            self.tracks.push_front(prev.clone());
            Some(prev)
        } else {
            None
        }
    }

    /// Get the first track in the upcomming track list if available.
    pub fn front_mut(&mut self) -> Option<&mut UniqueTrackEntry> {
        self.tracks.front_mut()
    }

    pub fn is_empty(&self) -> bool {
        self.tracks.is_empty()
    }

    pub fn extend<U: TrackEntryAsUnique, T: IntoIterator<Item = U>>(&mut self, iter: T) {
        let id_source = self.id_source().clone();
        self.tracks.extend(
            iter.into_iter()
                .map(|entry| entry.into_unique(id_source.clone())),
        )
    }

    pub fn remove(&mut self, index: usize) -> Option<UniqueTrackEntry> {
        self.tracks.remove(index)
    }

    pub fn len(&self) -> usize {
        self.tracks.len()
    }

    pub fn push_back<U: TrackEntryAsUnique>(&mut self, item: U) {
        self.tracks
            .push_back(item.into_unique(self.id_source().clone()))
    }

    pub fn back(&self) -> Option<&UniqueTrackEntry> {
        self.tracks.back()
    }

    pub fn swap(&mut self, i: usize, j: usize) {
        self.tracks.swap(i, j)
    }

    pub fn drain<R: RangeBounds<usize>>(&mut self, range: R) -> Drain<UniqueTrackEntry> {
        self.tracks.drain(range)
    }

    pub fn tracks_vec(&self) -> Vec<TrackEntry> {
        self.tracks.iter().map(TrackEntry::from).collect::<Vec<_>>()
    }

    pub fn tracks_vec_unique(&self) -> Vec<&UniqueTrackEntry> {
        self.tracks.iter().collect::<Vec<_>>()
    }

    pub fn played_tracks(&self) -> &Vec<UniqueTrackEntry> {
        &self.played_tracks
    }

    pub fn id_source(&self) -> &Arc<AtomicU64> {
        &self.id_counter
    }

    pub fn retain<F: FnMut(&UniqueTrackEntry) -> bool>(&mut self, f: F) {
        self.tracks.retain(f);
    }

    /// Helper for initialisation finalisation after deserialisation
    pub fn make_tracks_unique(&mut self) {
        let id_source = self.id_source().clone();

        self.tracks
            .iter_mut()
            .chain(self.played_tracks.iter_mut())
            .for_each(|track| {
                if track.id() == 0 {
                    track.make_unique(id_source.clone());
                }
            });
    }

    pub fn swap_by_ids(&mut self, id: u64, other: u64) -> anyhow::Result<()> {
        let (index, track) = self
            .track_by_id(id)
            .ok_or_else(|| anyhow!("Track with id {id} not found!"))?;
        track.enqueued = false;
        let (other_index, other_track) = self
            .track_by_id(other)
            .ok_or_else(|| anyhow!("Other track with id {other} not found!"))?;
        other_track.enqueued = false;
        self.swap(index, other_index);
        Ok(())
    }

    pub fn track_by_id(&mut self, id: u64) -> Option<(usize, &mut UniqueTrackEntry)> {
        self.tracks
            .iter_mut()
            .enumerate()
            .find(|(_, track)| track.id() == id)
    }
}

impl LenAndSwap for CrabQueue {
    fn len(&self) -> usize {
        self.len()
    }

    fn swap(&mut self, i: usize, j: usize) {
        self.swap(i, j)
    }
}

impl From<DirtyQueue> for CrabQueue {
    fn from(source: DirtyQueue) -> Self {
        let mut new = Self {
            id_counter: source.id_counter,
            tracks: source.tracks,
            played_tracks: source.played_tracks,
        };
        new.make_tracks_unique();
        new
    }
}

impl UniqueTrackEntry {
    pub fn new(id_source: Arc<AtomicU64>, entry: TrackEntry) -> Self {
        Self {
            id: Self::unique_id(id_source),
            title: entry.title,
            artist: entry.artist,
            url: entry.url,
            enqueued: entry.enqueued,
        }
    }

    pub fn id(&self) -> u64 {
        self.id
    }

    /// Make id unique in case it was not yet. This is *for now* the case if the Entry was build build by serde.
    pub fn make_unique(&mut self, id_source: Arc<AtomicU64>) -> Option<u64> {
        if self.id == 0 {
            self.id = Self::unique_id(id_source);
            Some(self.id())
        } else {
            None
        }
    }

    fn unique_id(id_source: Arc<AtomicU64>) -> u64 {
        id_source.fetch_add(1, std::sync::atomic::Ordering::AcqRel) + 1
    }
}

impl From<UniqueTrackEntry> for TrackEntry {
    fn from(source: UniqueTrackEntry) -> Self {
        TrackEntry {
            title: source.title,
            artist: source.artist,
            url: source.url,
            enqueued: source.enqueued,
        }
    }
}

impl From<&UniqueTrackEntry> for TrackEntry {
    fn from(source: &UniqueTrackEntry) -> Self {
        TrackEntry {
            title: source.title.clone(),
            artist: source.artist.clone(),
            url: source.url.clone(),
            enqueued: source.enqueued,
        }
    }
}

impl TrackEntryAsUnique for TrackEntry {
    fn into_unique(self, id_source: Arc<AtomicU64>) -> UniqueTrackEntry {
        UniqueTrackEntry::new(id_source, self)
    }
}

impl TrackEntryAsUnique for UniqueTrackEntry {
    fn into_unique(self, _id_source: Arc<AtomicU64>) -> UniqueTrackEntry {
        self
    }
}
