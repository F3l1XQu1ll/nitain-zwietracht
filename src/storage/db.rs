use anyhow::Result;
use openssl::ssl::{SslConnector, SslMethod, SslVerifyMode};
use postgres_openssl::MakeTlsConnector;
use serde::Serialize;

fn instance() -> Option<String> {
    if let Ok(instance) = std::env::var("INSTANCE") {
        debug!("INSTANCE {instance:#?}");
        Some(instance)
    } else {
        warn!("INSTANCE env variable not set!");
        None
    }
}
pub struct DBStorage {
    client: tokio_postgres::Client,
}

impl DBStorage {
    pub async fn new() -> Result<Self> {
        let db_url = std::env::var("DATABASE_URL")?;
        let mut connector_builder = SslConnector::builder(SslMethod::tls()).unwrap();
        connector_builder.set_verify(SslVerifyMode::NONE);

        let connector = MakeTlsConnector::new(connector_builder.build());
        let (client, connection) =
            tokio_postgres::connect(&(db_url + "?sslmode=require"), connector).await?;

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                error!("connection error: {}", e);
            } else {
                debug!("Connection established.");
            }
        });

        Ok(Self { client })
    }

    pub async fn setup_table(&self) -> Result<()> {
        self.client
            .batch_execute(
                "CREATE TABLE IF NOT EXISTS storage (instance TEXT PRIMARY KEY, data TEXT)",
            )
            .await?;
        info!("Storage table created.");
        Ok(())
    }

    pub async fn load_storage<T>(&self) -> Result<T>
    where
        T: serde::de::DeserializeOwned,
    {
        let res = self
            .client
            .query(
                "SELECT data FROM storage WHERE instance=$1",
                &[&instance()
                    .unwrap_or_else(|| "testing".to_string())
                    .to_owned()],
            )
            .await?;
        if !res.is_empty() {
            let data: String = res[0].get(0);
            //info!("Data loaded from db storage: {data:#?}");
            Ok(serde_json::from_str::<T>(&data)?)
        } else {
            Err(anyhow::Error::msg("DB query returned empty result"))
        }
    }

    pub async fn dump_storage<U>(&self, data: U) -> Result<()>
    where
        U: Sized + Serialize,
    {
        let json = serde_json::to_string(&data)?;
        self.client
            .execute(
                "INSERT INTO storage (instance, data) VALUES ($1, $2) ON CONFLICT (instance) DO UPDATE SET data=$2",
                &[&instance().unwrap_or_else(|| "testing".to_string()).to_owned(), &json],
            )
            .await?;
        Ok(())
    }
}
