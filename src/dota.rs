use crate::{
    commands::CommandContext,
    error::{CommandHandleResult, DotaError, ErrorContext, LogError},
    message_box::{send_msg, CommandHandler, CrabMessage},
    msg_send,
    storage::manager::ContextStorageManager,
    utils::*,
};
use anyhow::Result;
use futures::StreamExt;
use serde::Deserialize;
use serenity::{client::Context, model::id::UserId};
use std::{collections::HashMap, time::Duration};

#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct MMREstimate {
    estimate: u32,
}

//#[allow(dead_code)]
#[derive(Debug, Deserialize)]
struct Profile {
    account_id: u64,
    personaname: String,
    name: String,
    // plus: Option<bool>,
    // cheese: i32,
    // steamid: String,
    avatar: String,
    // avatarmedium: String,
    // avatarfull: String,
    // profileurl: String,
    // last_login: String,
    // loccountrycode: String,
    // is_contributor: bool,
}

//#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct DotaProfile {
    // tracked_until: String,
    // solo_competitive_rank: Option<String>,
    // competitive_rank: Option<String>,
    // rank_tier: Option<i32>,
    // leaderboard_rank: Option<i32>,
    // mmr_estimate: MMREstimate,
    profile: Profile,
}

/*
#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct DotaSearch {
    account_id: u64,
    avatarfull: String,
    personaname: String,
    last_match_time: Option<String>,
    similarity: f32,
}
*/

#[derive(Debug, Deserialize, Default)]
pub struct DotaWL {
    win: i32,
    lose: i32,
}

//#[allow(dead_code)]
#[derive(Debug, Deserialize)]
pub struct DotaRecentMatch {
    match_id: u64,
    //player_slot: i32,
    radiant_win: bool,
    duration: u64,
    //game_mode: u32,
    //lobby_type: u32,
    hero_id: u64,
    //start_time: u64,
    //version: Option<u32>,
    kills: i32,
    deaths: i32,
    assists: i32,
    //skill: Option<u32>,
    //xp_per_min: f32,
    //gold_per_min: f32,
    //hero_damage: u32,
    //hero_healing: u32,
    //last_hits: i32,
    //lane: Option<i32>,
    //lane_role: Option<i32>,
    //is_roaming: Option<bool>,
    //cluster: u64,
    //leaver_status: u32,
    //party_size: u32,
}

#[derive(Debug, Deserialize)]
pub struct DotaPurchase {
    time: i64,
    key: String,
}

#[derive(Debug, Deserialize)]
pub struct DotaPlayer {
    player_slot: i32,
    hero_id: u64,
    kills: i32,
    deaths: i32,
    assists: i32,
    purchase_log: Vec<DotaPurchase>,
    #[serde(rename = "isRadiant")]
    is_radiant: bool,
}

#[derive(Debug, Deserialize)]
pub struct DotaMatch {
    match_id: u64,
    replay_url: String,
    dire_score: i32,
    radiant_score: i32,
    radiant_win: bool,
    players: Vec<DotaPlayer>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct DotaHero {
    id: u64,
    name: String,
    localized_name: String,
}

#[derive(Debug, Deserialize)]
pub struct DotaKillersRow {
    //match_id: u64,
    //killed_by: String,
    killed_by: HashMap<String, i32>,
}

#[derive(Debug, Deserialize)]
pub struct DotaKillersResponse {
    rows: Vec<DotaKillersRow>,
}

#[derive(Debug, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum DotaItemQuality {
    Artifact,
    Common,
    Component,
    Consumable,
    Epic,
    Rare,
    SecretShop,
    Unknown,
}

impl Default for DotaItemQuality {
    fn default() -> Self {
        Self::Unknown
    }
}

#[derive(Debug, Deserialize)]
pub struct DotaItem {
    dname: Option<String>,
    #[serde(default)]
    qual: DotaItemQuality,
}

/// Register dota user id in HashMap
pub async fn register_dota(ctx: &CommandContext) -> CommandHandleResult {
    let user_id = ctx.user.id;
    let dota_id = ctx
        .args()
        .parse()
        .map_err(|_| DotaError::ProfileIDArgsParseError)?;

    let mut storage_manager = ctx.storage().await;

    storage_manager
        .update(|mut write| {
            write
                .config_mut()
                .dota_uids_mut()
                .insert(*user_id.as_u64(), dota_id);
        })
        .await
        .expect("Failed to register dota user");
    msg_send!(ctx, "Registered id ".to_owned() + &dota_id.to_string());
    Ok(())
}

/// Get dota user id by discord user id
pub async fn get_dota(ctx: &Context, user_id: UserId) -> Option<u64> {
    let steam = {
        let storage_manager = ctx.storage().await;
        let read = storage_manager.read().await;
        read.config().dota_uids().get(user_id.as_u64()).cloned()
    };

    steam
}

/// Load dota profile by id
pub async fn get_dota_profile(dota_id: u64) -> Option<DotaProfile> {
    if let Ok(profile_raw) = dota_request("players/".to_owned() + &dota_id.to_string()).await {
        //eprintln!("{:?}", profile_raw);
        match serde_json::from_str::<DotaProfile>(&profile_raw) {
            Ok(profile) => Some(profile),
            Err(e) => {
                eprintln!("{:?}; Profile={:?}", e, profile_raw);
                None
            }
        }
    } else {
        None
    }
}

/*
/// Search for a dota profile by steam name
pub async fn search_dota_profile(steam_id: String) -> Option<DotaSearch> {
    if let Ok(search_raw) = dota_request("search?q=".to_owned() + &steam_id.to_string()).await {
        //eprintln!("{:?}", search_raw);
        match serde_json::from_str::<Vec<DotaSearch>>(&search_raw) {
            Ok(profiles) => {
                let mut matching_profile = None;
                for profile in profiles {
                    if profile.similarity > 0.6 && profile.last_match_time.is_some() {
                        matching_profile = Some(profile);
                        break;
                    }
                }
                matching_profile
            }
            Err(e) => {
                eprintln!("{:?}", e);
                None
            }
        }
    } else {
        None
    }
}
*/

/*
/// Register find dota id by steam and register dota id
pub async fn register_by_steam(ctx: &Context, user_id: UserId, steam_id: String) -> u64 {
    if let Some(profile) = search_dota_profile(steam_id).await {
        register_dota(ctx, user_id, profile.account_id).await;
        profile.account_id
    } else {
        0
    }
}
*/

/// Display dota profile as message
pub async fn displ_profile(ctx: &CommandContext) -> CommandHandleResult {
    let user_id = ctx.user.id;
    let dota_id = get_dota(ctx.ctx(), user_id)
        .await
        .ok_or(DotaError::UserNotRegistered)?;
    let profile = get_dota_profile(dota_id)
        .await
        .ok_or(DotaError::ProfileLoadError)?;
    let wl = get_wl(profile.profile.account_id).await.unwrap_or_default();
    let strings = vec![
        ("Name: ", profile.profile.personaname, true),
        ("Wins: ", wl.win.to_string(), true),
        ("Loses: ", wl.lose.to_string(), true),
        (
            "<OPENDOTA/> ",
            "https://www.opendota.com/players/".to_owned()
                + &profile.profile.account_id.to_string()
                + "/overview",
            true,
        ),
    ]
    .iter()
    .map(|s| (s.0.to_string(), s.1.clone(), s.2))
    .collect::<Vec<(String, String, bool)>>();
    send_msg(
        ctx.clone(),
        "Profile",
        strings,
        &profile.profile.avatar,
        None,
    )
    .await;
    Ok(())
}

pub async fn get_wl(dota_id: u64) -> Option<DotaWL> {
    if let Ok(wl_raw) = dota_request("players/".to_owned() + &dota_id.to_string() + "/wl").await {
        match serde_json::from_str::<DotaWL>(&wl_raw) {
            Ok(wl) => Some(wl),
            Err(e) => {
                eprintln!("{:?}", e);
                None
            }
        }
    } else {
        None
    }
}

pub async fn get_recent_matches(dota_id: u64) -> Option<Vec<DotaRecentMatch>> {
    if let Ok(recent_raw) =
        dota_request("players/".to_owned() + &dota_id.to_string() + "/recentMatches").await
    {
        match serde_json::from_str::<Vec<DotaRecentMatch>>(&recent_raw) {
            Ok(matches) => Some(matches),
            Err(e) => {
                eprintln!("{:?}; raw: {:?}", e, recent_raw);
                None
            }
        }
    } else {
        None
    }
}

pub async fn displ_last_match(ctx: &CommandContext) -> CommandHandleResult {
    let dota_id = get_dota(ctx.ctx(), ctx.user.id)
        .await
        .ok_or(DotaError::UserNotRegistered)?;
    let recent_matches = get_recent_matches(dota_id)
        .await
        .ok_or(DotaError::NoMatchesFound)?;
    let dota_heroes = get_heroes().await.ok_or(DotaError::HeroRetrievalError)?;
    let last_match = &recent_matches[0];
    let duration = chrono::Duration::from_std(Duration::from_secs(last_match.duration)).unwrap();
    let strings = vec![
        ("ID: ", last_match.match_id.to_string(), true),
        (
            "Winner: ",
            match last_match.radiant_win {
                true => "Radiant".to_string(),
                false => "Dire".to_string(),
            },
            true,
        ),
        (
            "Duration: ",
            format!(
                "{}:{}",
                duration.num_minutes(),
                (duration.num_seconds() / 60) % 60
            ),
            true,
        ),
        (
            "Played as:",
            kda(
                &dota_heroes,
                last_match.hero_id,
                last_match.kills,
                last_match.deaths,
                last_match.assists,
            )
            .await,
            true,
        ),
        (
            "<OPENDOTA/>",
            "https://www.opendota.com/matches/".to_owned() + &last_match.match_id.to_string(),
            true,
        ),
    ]
    .iter()
    .map(|s| (s.0.to_string(), s.1.clone(), s.2))
    .collect::<Vec<(String, String, bool)>>();
    send_msg(ctx.clone(), "Last Match", strings, "", None).await;
    Ok(())
}

pub async fn get_match(match_id: u64) -> Option<DotaMatch> {
    if let Ok(match_raw) = dota_request("matches/".to_owned() + &match_id.to_string()).await {
        match serde_json::from_str::<DotaMatch>(&match_raw) {
            Ok(dota_match) => Some(dota_match),
            Err(e) => {
                eprintln!("{:?}; raw: {:?}", e, match_raw);
                None
            }
        }
    } else {
        None
    }
}

pub async fn get_heroes() -> Option<Vec<DotaHero>> {
    if let Ok(heroes_raw) = dota_request("heroes".to_owned()).await {
        match serde_json::from_str::<Vec<DotaHero>>(&heroes_raw) {
            Ok(dota_heroes) => Some(dota_heroes),
            Err(e) => {
                eprintln!("{:?}; raw: {:?}", e, heroes_raw);
                None
            }
        }
    } else {
        None
    }
}

pub async fn displ_match(ctx: &CommandContext) -> CommandHandleResult {
    let match_id = ctx
        .args()
        .parse()
        .map_err(|_| DotaError::MatchIDArgsParseError)?;
    let dota_heroes = get_heroes().await.ok_or(DotaError::HeroRetrievalError)?;
    let dota_match = get_match(match_id).await.ok_or(DotaError::NoMatchesFound)?;
    let mut kdas_r = Vec::new();
    let mut kdas_d = Vec::new();
    for player in dota_match.players {
        let kda = kda(
            &dota_heroes,
            player.hero_id,
            player.kills,
            player.deaths,
            player.assists,
        )
        .await;
        //eprintln!("{:?}", kda);
        if player.player_slot < 5 {
            kdas_r.push(kda);
        } else {
            kdas_d.push(kda);
        }
    }
    let kdsas_r_str = kdas_r.join("\n");
    let kdsas_d_str = kdas_d.join("\n");
    let strings = vec![
        ("ID:", dota_match.match_id.to_string(), true),
        ("Replay:", dota_match.replay_url, false),
        //("\u{200B}", "\u{200B}".to_string(), true),
        (
            "Radiant:",
            dota_match.radiant_score.to_string()
                + if dota_match.radiant_win {
                    " (Winner)"
                } else {
                    ""
                },
            true,
        ),
        ("\u{200B}", ":".to_string(), true),
        (
            "Dire:",
            dota_match.dire_score.to_string()
                + if !dota_match.radiant_win {
                    " (Winner)"
                } else {
                    ""
                },
            true,
        ),
        ("\u{200B}", kdsas_r_str, true),
        ("\u{200B}", "\u{200B}".to_string(), true),
        ("\u{200B}", kdsas_d_str, true),
        (
            "<OPENDOTA/>",
            "https://www.opendota.com/matches/".to_owned() + &dota_match.match_id.to_string(),
            true,
        ),
    ]
    .iter()
    .map(|s| (s.0.to_string(), s.1.clone(), s.2))
    .collect::<Vec<(String, String, bool)>>();
    send_msg(ctx.clone(), "Requested Match", strings, "", None).await;
    Ok(())
}

pub async fn get_killers(hero_id: u64) -> Option<DotaKillersResponse> {
    if let Ok(killers_raw) = dota_request(
        "explorer?sql=".to_owned() + percent_encode(
            format!(
                "SELECT match_id, killed_by FROM player_matches WHERE hero_id={} and is_roaming is not null and level>20 ORDER BY deaths desc LIMIT 20",
                hero_id
            ).as_str()
        ).await.as_str()
    ).await{
        eprintln!("{:?}", killers_raw);
        match serde_json::from_str::<DotaKillersResponse>(&killers_raw) {
            Ok(killers) => Some(killers),
            Err(e) => {eprintln!("{:?}; raw: {:?}", e, killers_raw); None}
        }
    } else {
        None
    }
}

pub async fn get_conters(hero_id: u64) -> Option<HashMap<String, i32>> {
    let killers = get_killers(hero_id).await?;
    let rows = killers.rows;
    let mut common_counters: HashMap<String, i32> = HashMap::new();
    for row in rows {
        let murderers = row.killed_by;
        //if let Ok(murderers_json) = json::parse(&murderers) {
        //for (murderer, kills) in murderers_json.entries() {
        for (murderer, kills) in murderers.iter() {
            if common_counters.contains_key(murderer) {
                let prev_kills = common_counters.get(murderer).cloned().unwrap();
                //common_counters.insert(murderer.to_string(), prev_kills + kills.as_i32().unwrap());
                common_counters.insert(murderer.to_string(), prev_kills + kills);
            } else {
                //common_counters.insert(murderer.to_string(), kills.as_i32().unwrap());
                common_counters.insert(murderer.to_string(), *kills);
            }
        }
        //}
    }
    Some(common_counters)
}

pub async fn get_top_conters(hero_id: u64) -> Option<Vec<(String, i32)>> {
    let conters = get_conters(hero_id).await?;
    eprintln!("{:?}", conters);
    let mut conters_vec: Vec<(String, i32)> = conters.into_iter().collect();
    conters_vec.sort_by_key(|k| k.1);
    conters_vec.reverse();
    if conters_vec.len() > 5 {
        Some(conters_vec[0..5].to_vec())
    } else {
        Some(conters_vec)
    }
}

pub async fn displ_conters(ctx: &CommandContext) -> CommandHandleResult {
    let hero = ctx
        .args()
        .parse()
        .map_err(|_| DotaError::NameArgsParseError)?;
    let heroes = get_heroes().await.ok_or(DotaError::HeroRetrievalError)?;
    let dota_hero = hero_by_lname(&heroes, hero)
        .await
        .ok_or(DotaError::HeroUnknownError)?;

    let id = dota_hero.id;
    let top_conters = get_top_conters(id)
        .await
        .ok_or(DotaError::CountersNotFound {
            hero: dota_hero.localized_name.clone(),
        })?;
    let strings = vec![(
        "Assumed conters:".to_string(),
        top_conters
            .into_iter()
            .map(|c| {
                if let Some(hero) = hero_by_name(&heroes, c.0) {
                    hero.localized_name
                } else {
                    ":warning:".to_string()
                }
            })
            .collect::<Vec<String>>()
            .join("\n"),
        true,
    )];
    send_msg(
        ctx.clone(),
        &("Conters for ".to_owned() + &dota_hero.localized_name),
        strings,
        "",
        None,
    )
    .await;
    Ok(())
}

pub async fn pro_search(ctx: &CommandContext) -> CommandHandleResult {
    let query = ctx.args.current();
    debug!("query: {query:#?}");
    let pros = load_pro_players().await.map_err(|e| {
        error!("{e:#?}");
        DotaError::ProPlayerListRetrievalError
    })?;

    if let Some(query) = query {
        // Search
        output_pro_last_matches(ctx, pros, query).await?;
    } else if let Some(watching) = ctx
        .storage()
        .await
        .read()
        .await
        .config()
        .dota_watching()
        .get(&ctx.user.id)
    {
        if watching.is_empty() {
            return Err(DotaError::EmptyWatchingError.into());
        } else {
            let watching_stream = futures::stream::iter(watching.clone());
            let profiles_opts: Vec<Option<DotaProfile>> = watching_stream
                .map(get_dota_profile)
                .buffer_unordered(5)
                .collect()
                .await;
            let profiles: Vec<_> = profiles_opts.iter().filter_map(|p| p.as_ref()).collect();
            let message = CrabMessage::from_ctx(&ctx.clone())
                .description("Please select a player:")
                .action_row(
                    serenity::builder::CreateActionRow::default()
                        .create_select_menu(|m| {
                            m.options(|o| {
                                for player in profiles {
                                    o.create_option(|c| {
                                        c.label(player.profile.name.clone())
                                            .value(player.profile.name.clone())
                                    });
                                }
                                o
                            });
                            m.custom_id("selector-followed-pro-player")
                        })
                        .clone(),
                )
                .reference(ctx.ref_msg())
                .message()
                .await?;
            let ctx = ctx.clone();
            tokio::spawn(async move {
                if let Some(interaction) = message
                    .message
                    .await_component_interaction(&ctx.ctx.shard)
                    .timeout(Duration::from_secs(60 * 3))
                    .await
                {
                    interaction
                        .create_interaction_response(ctx.ctx(), |r| {
                            r.interaction_response_data(|d| d.content("Done :white_check_mark:"))
                        })
                        .await
                        .context("Failed to send annoyance (in dota)")
                        .handle();
                    let handler = CommandHandler::new(
                        ctx.ctx.http.clone(),
                        ctx.ctx.shard.clone(),
                        ctx.channel_id,
                        ctx.ref_msg(),
                    );
                    handler.send_typing().await;
                    handler
                        .handle(
                            output_pro_last_matches(&ctx, pros, &interaction.data.values[0]).await,
                        )
                        .await;
                }
            });
        }
    } else {
        return Err(DotaError::EmptyWatchingError.into());
    }

    Ok(())
}

pub async fn watch_pro(ctx: &CommandContext) -> CommandHandleResult {
    for profile in check_load_pros(ctx).await? {
        debug!("working on profile: {profile}");
        ctx.storage()
            .await
            .update(|mut write| {
                let entry = write
                    .config_mut()
                    .dota_watching_mut()
                    .entry(ctx.user().id)
                    .or_default();
                if !entry.contains(&profile) {
                    entry.push(profile);
                }
            })
            .await
            .context("Failed to update watching list!")?;
    }
    let following = ctx
        .storage()
        .await
        .read()
        .await
        .config()
        .dota_watching()
        .get(&ctx.user().id)
        .map(|entry| entry.len())
        .unwrap_or(0);
    CrabMessage::from_ctx(ctx)
        .description(format!("You are now following {} pro players", following))
        .message()
        .await?;
    Ok(())
}

pub async fn unwatch_pro(ctx: &CommandContext) -> CommandHandleResult {
    for profile in check_load_pros(ctx).await? {
        ctx.storage()
            .await
            .update(|mut write| {
                write
                    .config_mut()
                    .dota_watching_mut()
                    .entry(ctx.user().id)
                    .or_default()
                    .retain(|watching| *watching != profile);
            })
            .await
            .context("Failed to update watching list!")?;
    }
    let following = ctx
        .storage()
        .await
        .read()
        .await
        .config()
        .dota_watching()
        .get(&ctx.user().id)
        .map(|entry| entry.len())
        .unwrap_or(0);
    CrabMessage::from_ctx(ctx)
        .description(format!("You are now following {} pro players", following))
        .message()
        .await?;
    Ok(())
}

async fn check_load_pros(ctx: &CommandContext) -> anyhow::Result<Vec<u64>> {
    let mut args = ctx.args();
    if args.current().is_none() {
        Err(DotaError::WatchInvalidProError.into())
    } else {
        let mut profiles = Vec::new();
        let pro_players = load_pro_players().await.context("Loading pros failed")?;
        debug!("Loaded pros");
        while let Some(player_name) = args.current() {
            debug!("arg: {player_name}");
            let profile = pro_players
                .iter()
                .find(|player| player.name.to_lowercase() == player_name.to_lowercase())
                .map(|profile| profile.account_id)
                .ok_or(DotaError::NotAProError {
                    name: player_name.to_string(),
                })?;
            profiles.push(profile);
            args = args.advance().clone();
        }
        Ok(profiles)
    }
}

async fn output_pro_last_matches(
    ctx: &CommandContext,
    pros: Vec<Profile>,
    query: &str,
) -> CommandHandleResult {
    // Find player in list of pro players
    let player = pros
        .iter()
        .find(|pro| pro.name.to_lowercase() == query.to_lowercase())
        .ok_or(DotaError::NotAProError {
            name: query.to_string(),
        })?;
    // find recend matches for this player
    let recent_matches = get_recent_matches(player.account_id)
        .await
        .ok_or(DotaError::NoMatchesFound)?;

    let heros = get_heroes().await.ok_or(DotaError::HeroRetrievalError)?;

    let mut matches_info = Vec::new();

    let items = load_items().await?;

    // Iterate recent matches
    for (index, recent_match) in recent_matches.iter().enumerate() {
        let hero = recent_match.hero_id;
        // Generate players kda for this match
        let kda_str = kda(
            &heros,
            hero,
            recent_match.kills,
            recent_match.deaths,
            recent_match.assists,
        )
        .await;

        // load the full match data corresponding for this match
        let real_match = get_match(recent_match.match_id)
            .await
            .ok_or(DotaError::MatchLoadError)?;

        // find the info for this player
        let play = real_match
            .players
            .iter()
            .find(|player| player.hero_id == hero)
            .ok_or_else(|| anyhow!("Could not find player in his match wtf?!"))?;

        let mut radiant_kdas = Vec::new();
        let mut dire_kdas = Vec::new();

        for other_play in &real_match.players {
            // skip the info for our player
            if other_play.hero_id != play.hero_id {
                let other_player_kda = kda(
                    &heros,
                    other_play.hero_id,
                    other_play.kills,
                    other_play.deaths,
                    other_play.assists,
                )
                .await;
                if other_play.is_radiant {
                    radiant_kdas.push(other_player_kda);
                } else {
                    dire_kdas.push(other_player_kda);
                }
            }
        }

        // find starting items, aka items bought at a negative time
        let start_inventory_keys = play
            .purchase_log
            .iter()
            .filter(|item| item.time <= 0)
            .map(|item| item.key.clone())
            .collect::<Vec<_>>();

        // Map of items (names) and the count of these items
        let mut start_inventory_entries = HashMap::new();
        for key in start_inventory_keys {
            let item = &items.iter().find(|item| item.0 == &key).unwrap();
            let item_name = item.1.dname.clone().unwrap_or_else(|| item.0.clone());

            let count = start_inventory_entries.entry(item_name).or_insert(0);
            *count += 1;
        }

        // List of starting items in the format 'ITEMNAME xCOUNT'
        let mut start_inventory = Vec::new();
        for (item, count) in start_inventory_entries {
            start_inventory.push(format!("`{:<22}` x{:<2}", item, count));
        }

        // String, based on if the players team won this match
        let win = match real_match.radiant_win == play.is_radiant {
            true => "Victory",
            false => "Loss",
        };

        // Result title
        let result_title = format!("{}'s Match #{} ({})", player.name, index + 1, win,);

        // General match data
        let (team_kdas, enemy_kdas) = if play.is_radiant {
            (radiant_kdas, dire_kdas)
        } else {
            (dire_kdas, radiant_kdas)
        };
        let play_info = ("Played".to_string(), kda_str, true);
        let team_info = (
            "Team".to_string(),
            format!("*Played with:*\n{}", team_kdas.join(",\n\n"),),
            true,
        );
        let enemy_info = (
            "Enemy".to_string(),
            format!("*Against:*\n{}", enemy_kdas.join(",\n\n")),
            true,
        );

        // Field for starting items
        let match_starting_items = (
            "Starting items".to_string(),
            start_inventory.join(",\n"),
            true,
        );

        // find all items that were not starting items
        let mut other_items = play
            .purchase_log
            .iter()
            .filter(|item| item.time > 0)
            .filter_map(|buy| {
                items
                    .iter()
                    .find(|item| item.0 == &buy.key)
                    .map(|item| (buy, item))
            })
            .filter(|(_, (_, item))| {
                item.qual != DotaItemQuality::Consumable && item.qual != DotaItemQuality::Unknown
            })
            .map(|(buy, (key, item))| (key, buy, item))
            .collect::<Vec<_>>();

        // sort items by buy time
        other_items.sort_by_key(|(_, buy, _)| buy.time / 60);

        // Field for other items
        let other_items_fmt = other_items
            .iter()
            .map(|(key, buy, item)| {
                format!(
                    "`{:<22}` ({:<2}m)",
                    item.dname.clone().unwrap_or_else(|| key.to_string()),
                    buy.time / 60
                )
            })
            .collect::<Vec<_>>();
        let other_items_field = ("Items".to_string(), other_items_fmt.join("\n"), true);

        // Collect this matches fields
        matches_info.push((
            result_title,
            format!("https://www.opendota.com/matches/{}", real_match.match_id),
            vec![
                play_info,
                team_info,
                enemy_info,
                match_starting_items,
                other_items_field,
            ],
            format!("Match ID: `{}`", real_match.match_id),
        ));
        if index == 4 {
            break;
        }
    }
    let mut builder = CrabMessage::from_ctx(ctx);
    for (index, (title, url, dota_match, id)) in matches_info.into_iter().enumerate() {
        if index == 0 {
            builder.rename(format!("Match #{}", index + 1));
        } else {
            builder.page(format!("Match #{}", index + 1));
        }
        builder
            .title(title)
            .link(url)
            .fields(dota_match)
            .footer_text(id);
    }
    builder.reference(ctx.ref_msg()).message().await?;
    Ok(())
}

async fn load_items() -> anyhow::Result<HashMap<String, DotaItem>> {
    let param = "constants/items".to_string();
    let items_raw = dota_request(param).await?;
    let items = serde_json::from_str(&items_raw)?;
    Ok(items)
}

async fn load_pro_players() -> anyhow::Result<Vec<Profile>> {
    let param = "proPlayers".to_string();
    let players_raw = dota_request(param).await?;
    let players: Vec<Profile> = serde_json::from_str(&players_raw)?;
    Ok(players)
}

async fn percent_encode(string: &str) -> String {
    urlencoding::encode(string).to_owned().to_string()
}

async fn dota_request(param: String) -> Result<String> {
    let base_url = "https://api.opendota.com/api/";
    let url = base_url.to_owned() + &param;
    do_fetch(url).await
}

async fn hero_by_id(heroes: &[DotaHero], id: u64) -> Option<DotaHero> {
    heroes.iter().find(|h| h.id == id).cloned()
}

async fn hero_by_lname(heroes: &[DotaHero], name: String) -> Option<DotaHero> {
    let exact_hero = heroes.iter().find(|h| h.localized_name == name).cloned();
    if exact_hero.is_none() {
        heroes
            .iter()
            .find(|h| {
                let split = h.localized_name.split_whitespace();
                let abrv = split
                    .map(|s| s.chars().next().unwrap().to_string())
                    .collect::<Vec<String>>()
                    .join("");
                abrv.to_lowercase() == name.to_lowercase()
            })
            .cloned()
    } else {
        exact_hero
    }
}

fn hero_by_name(heroes: &[DotaHero], name: String) -> Option<DotaHero> {
    heroes.iter().find(|h| h.name == name).cloned()
}

async fn kda(heroes: &[DotaHero], hero_id: u64, kills: i32, deaths: i32, assists: i32) -> String {
    format!(
        "`{:<20}\n{:>14}/{:>2}/{:>2}`",
        hero_by_id(heroes, hero_id)
            .await
            .map(|hero| hero.localized_name)
            .unwrap_or_else(|| ":warning:".to_string()),
        kills,
        deaths,
        assists
    )
}
