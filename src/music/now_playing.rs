use super::*;
use crate::{
    action_queue::{ActionContinue, ContextActionQueue, CrabAction},
    message_box::{CrabMessage, FromHex},
    msg_send,
    storage::manager::ContextStorageManager,
    utils,
};
use anyhow::Result;
use serenity::{
    builder::{CreateActionRow, CreateButton},
    client::Context,
    framework::standard::{Args, Delimiter},
    model::{
        channel::{Message, ReactionType},
        id::{ChannelId, GuildId},
        prelude::component::ButtonStyle,
    },
    utils::Colour,
};
use songbird::input::Metadata;
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    str::FromStr,
    time::Duration,
};
use tokio::time::sleep;

#[derive(Debug)]
struct ParseComponentError(String);

impl Display for ParseComponentError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "Failed to parse {} as component", self.0)
    }
}

impl StdError for ParseComponentError {}

#[derive(Debug)]
enum PlayerButtons {
    Repeat,
    PlayPause,
    LoopUnloop,
    Skip,
    Shuffle,
}

impl Display for PlayerButtons {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Self::Repeat => write!(f, "repeat"),
            Self::PlayPause => write!(f, "playpause"),
            Self::LoopUnloop => write!(f, "loopunloop"),
            Self::Skip => write!(f, "skip"),
            Self::Shuffle => write!(f, "shuffle"),
        }
    }
}

impl PlayerButtons {
    async fn handle_interaction(&self, ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
        match self {
            PlayerButtons::Repeat => player::repeat(ctx, guild_id, channel_id).await,
            PlayerButtons::PlayPause => {
                toggle_pause::toggle_pause(ctx, guild_id, channel_id, None).await
            }
            PlayerButtons::LoopUnloop => {
                helper_loop::toggle_loop(ctx, guild_id, channel_id, None).await
            }
            PlayerButtons::Skip => {
                let args = Args::new(" ", &[Delimiter::Single(' ')]);
                player::skip(ctx, guild_id, channel_id, args).await
            }
            PlayerButtons::Shuffle => queue::shuffle_queue(ctx, guild_id, channel_id).await,
        }
    }

    fn emoji(&self) -> &str {
        match self {
            Self::PlayPause => "⏯️",
            Self::LoopUnloop => "🔁",
            Self::Skip => "⏭️",
            Self::Repeat => "⏮️",
            Self::Shuffle => "🔀",
        }
    }

    fn button(&self) -> CreateButton {
        let mut b = CreateButton::default();
        b.custom_id(self.to_string());
        b.emoji(ReactionType::Unicode(self.emoji().to_string()));
        b.style(ButtonStyle::Secondary);
        b
    }

    fn action_row() -> CreateActionRow {
        let mut ar = CreateActionRow::default();
        // We can add up to 5 buttons per action row
        ar.add_button(PlayerButtons::LoopUnloop.button());
        ar.add_button(PlayerButtons::Repeat.button());
        ar.add_button(PlayerButtons::PlayPause.button());
        ar.add_button(PlayerButtons::Skip.button());
        ar.add_button(PlayerButtons::Shuffle.button());
        ar
    }
}

impl FromStr for PlayerButtons {
    type Err = ParseComponentError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "repeat" => Ok(PlayerButtons::Repeat),
            "playpause" => Ok(PlayerButtons::PlayPause),
            "loopunloop" => Ok(PlayerButtons::LoopUnloop),
            "skip" => Ok(PlayerButtons::Skip),
            "shuffle" => Ok(PlayerButtons::Shuffle),
            _ => Err(ParseComponentError(s.to_string())),
        }
    }
}

pub async fn now_playing(ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
    let ctx = ctx.clone();
    tokio::spawn(async move {
        sleep(Duration::from_millis(10 * 100 * 2)).await;
        // Delete old buttons in this channel
        {
            let manager = ctx.storage().await;
            let read = manager.read().await;
            // Get id of old now_playing message from storage
            if let Some(last_known_message_id) = read.config().buttons().get(channel_id.as_u64()) {
                // Try to find the corresponding message
                if let Ok(message) = channel_id.message(&ctx, *last_known_message_id).await {
                    debug!("Removing old buttons {channel_id:#?}/{last_known_message_id:#?}");
                    // Try to delete the message
                    if let Err(e) = message.delete(&ctx).await {
                        error!("Failed to remove old buttons {channel_id:#?}/{last_known_message_id:#?}: {e:#?}")
                    } else {
                        debug!("Old buttons removed");
                    }
                } else {
                    // The message was not found, we assume it was deleted by another thread
                    debug!("Could not get message for this id, message might have been deleted.");
                }
            }
        }
        // Get OutputData
        let data = loop {
            if utils::is_playing(&ctx, guild_id).await {
                if let Some(data) = get_metadata(&ctx, guild_id).await {
                    break data;
                } else {
                    sleep(Duration::from_millis(100)).await;
                    continue;
                }
            } else {
                debug!("Got no metadata!");
                msg_send!(ctx, channel_id, "Nothing is playing right now");
                return;
            }
        };

        let m = send_new_and_register(&ctx, channel_id, data).await;
        debug!("New message sent");
        //let res = update_loop(ctx1, msg1, m.clone());
        {
            let action_queue = ctx.action_queue().await;
            let mut write = action_queue.write().await;
            debug!("Got action queue write lock");
            write
                .register_action(
                    200,
                    false,
                    UpdateNowPlayingLoop {
                        ctx: ctx.clone(),
                        guild_id,
                        channel_id,
                        m: m.clone(),
                    },
                )
                .await;
            debug!("Registered action update loop");
            write
                .register_action(
                    0,
                    false,
                    InteractionLoop {
                        ctx,
                        guild_id,
                        channel_id,
                        m: m.clone(),
                    },
                )
                .await;
            debug!("Registered interaction loop");
        }
    });
    debug!("Actions registered");
}

struct InteractionLoop {
    ctx: Context,
    guild_id: GuildId,
    channel_id: ChannelId,
    m: Message,
}

#[async_trait]
impl CrabAction for InteractionLoop {
    async fn action(&mut self) -> ActionContinue {
        let ctx = self.ctx.clone();
        let guild_id = self.guild_id;
        let channel_id = self.channel_id;
        let m = self.m.clone();
        tokio::spawn(async move {
            // The message will only be sent once. If the loop repeats, another interaction will be awaited.
            loop {
                let mut m = m.clone();
                // Wait for interaction
                let cib = m
                    .await_component_interaction(&ctx)
                    // Stop waiting after 3 min
                    .timeout(Duration::from_secs(3 * 60))
                    .await;
                // Handle interaction
                if let Some(mci) = cib {
                    let option = PlayerButtons::from_str(&mci.data.custom_id).unwrap();
                    // Delete the old buttons
                    m.delete(&ctx).await.unwrap();
                    // Handle the interaction
                    option.handle_interaction(&ctx, guild_id, channel_id).await;
                    // Get OutputData
                    let data = loop {
                        if utils::is_playing(&ctx, guild_id).await {
                            if let Some(data) = get_metadata(&ctx, guild_id).await {
                                break data;
                            } else {
                                sleep(Duration::from_millis(100)).await;
                                continue;
                            }
                        } else {
                            debug!("Got no metadata!");
                            msg_send!(ctx, channel_id, "Nothing is playing right now");
                            return;
                        }
                    };
                    // Send new buttons
                    m = send_new_and_register(&ctx, channel_id, data).await;

                    let action_queue = ctx.action_queue().await;
                    let mut write = action_queue.write().await;
                    debug!("Got action queue write lock");
                    write
                        .register_action(
                            200,
                            false,
                            UpdateNowPlayingLoop {
                                ctx: ctx.clone(),
                                guild_id,
                                channel_id,
                                m: m.clone(),
                            },
                        )
                        .await;
                    debug!("Registered action update loop");
                    write
                        .register_action(
                            0,
                            false,
                            InteractionLoop {
                                ctx: ctx.clone(),
                                guild_id,
                                channel_id,
                                m: m.clone(),
                            },
                        )
                        .await;
                    debug!("Registered interaction loop");

                    debug!("Actions registered");
                }
                // Interaction has been handled, or there was no interaction.
                // Get guild data
                let storage_manager = ctx.storage().await;
                let last_btn_id = {
                    let read = storage_manager.read().await;
                    read.config()
                        .buttons()
                        .get(channel_id.as_u64())
                        .cloned()
                        .unwrap_or_default()
                };
                // Check if last message for this channel does not equal the last message we have sent from
                // this thread. In this case an other thread has been spawned for this channel.
                if last_btn_id != *m.id.as_u64() {
                    // Break loop and let the task die
                    if let Err(e) = m.delete(ctx).await {
                        warn!("Failed to delete old message {:#?}: {e:#?}; might have been deleted by another thread", m.id);
                    }
                    break;
                }
            }
        });
        ActionContinue(false)
    }

    fn name(&self) -> String {
        "UpdateLoop".to_string() + &self.m.id.as_u64().to_string()
    }
}

struct UpdateNowPlayingLoop {
    ctx: Context,
    guild_id: GuildId,
    channel_id: ChannelId,
    m: Message,
}

#[async_trait]
impl CrabAction for UpdateNowPlayingLoop {
    async fn action(&mut self) -> ActionContinue {
        let ctx1 = &self.ctx;
        let guild_id = self.guild_id;
        let channel_id = self.channel_id;
        let m1 = &mut self.m;
        // Get OutputData
        let data = if let Some(data) = get_metadata(ctx1, guild_id).await {
            data
        } else if utils::is_playing(ctx1, guild_id).await {
            sleep(Duration::from_millis(100)).await;
            return ActionContinue(true);
        } else {
            debug!("Not playing!");
            if let Some(message_id) = ctx1
                .storage()
                .await
                .read()
                .await
                .config()
                .buttons()
                .get(channel_id.as_u64())
            {
                if let Ok(message) = channel_id.message(ctx1, *message_id).await {
                    if let Err(e) = message.delete(ctx1).await {
                        warn!(
                            "Failed to delete message {message_id:#?} in channel {channel_id:#?}: {e:#?}"
                        );
                    }
                }
            }
            return ActionContinue(false);
        };
        let metadata = data.metadata.clone();
        let max_duration = if let Some(duration) = metadata.duration {
            duration
        } else {
            Duration::from_secs(0)
        };
        let position = if data.position > max_duration {
            max_duration
        } else {
            data.position
        };
        // If we did not break, update the message without panic and wait again at next iteration
        if let Err(e) = m1
            .edit(&ctx1, |msg| {
                msg.embed(|e| build_now_playing_message(e, data, metadata, position, max_duration));
                // msg.components(|c| c.add_action_row(PlayerButtons::action_row()));
                msg
            })
            .await
        {
            warn!("Failed to update message {e:#?}");
            return ActionContinue(false);
        }
        // Interaction has been handled, or there was no interaction.
        // Get guild data
        let storage_manager = ctx1.storage().await;
        let last_btn_id = {
            let read = storage_manager.read().await;
            read.config()
                .buttons()
                .get(channel_id.as_u64())
                .cloned()
                .unwrap_or_default()
        };
        // Check if last message for this channel does not equal the last message we have sent from
        // this thread. In this case an other thread has been spawned for this channel.
        if last_btn_id != *m1.id.as_u64() {
            debug!("last_btn_id {last_btn_id} != {}", m1.id.as_u64());
            // Break loop and let the task die
            return ActionContinue(false);
        }
        ActionContinue(true)
    }

    fn name(&self) -> String {
        "InteractionLoop".to_string() + &self.m.id.as_u64().to_string()
    }
}
async fn bool_to_str(bool: bool) -> String {
    if bool {
        "on".to_string()
    } else {
        "off".to_string()
    }
}

struct OutputData {
    metadata: Metadata,
    autoplay_state: String,
    loop_state: String,
    play_state: String,
    position: Duration,
    timeline: String,
    queue_length: String,
}

async fn get_metadata(ctx: &Context, guild_id: GuildId) -> Option<OutputData> {
    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    // Get guild data
    let storage_manager = ctx.storage().await;
    let guild_data = {
        let read = storage_manager.read().await;
        read.kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
    };
    // Prepare guild data for output
    let autoplay_state = bool_to_str(guild_data.autoplay_state).await;
    let loop_state = bool_to_str(guild_data.loop_state).await;
    let play_state = if guild_data.paused_state {
        "Paused:\n".to_string()
    } else {
        "Now playing:\n".to_string()
    };
    // Check for voice connection
    if let Some(handler_lock) = manager.get(guild_id) {
        let handler = handler_lock.lock().await;
        let queue = handler.queue();
        // Check if something is playing
        if let Some(track) = queue.current() {
            // Get queue data

            // get length of actual queue
            let shared_queue = guild_data.queue;
            let len = shared_queue.len();
            let queue_length = if len == 1 {
                len.to_string() + " song"
            } else {
                len.to_string() + " songs"
            };

            // get metadata of current song
            let metadata = track.metadata().clone();
            let time_position = track.get_info().await.unwrap().position;
            let ratio = if let Some(duration) = metadata.duration {
                time_position.as_secs_f64() / duration.as_secs_f64()
            } else {
                0.0
            };
            let position = time_position;
            // Create timeline
            let mut timeline = String::new();
            // 41 because the String will get as long as "https://www.youtube.com/watch?v="
            let max = 41;
            // Set position of the circle on the timeline
            let mut time_position = (ratio * max as f64).round() as usize;
            // Avoid a timeline without a radio_button
            if time_position >= 41 {
                time_position = 40
            }
            for i in 0..max {
                if i == time_position {
                    timeline += ":radio_button:";
                } else {
                    timeline.push('-');
                }
            }
            // Return OutputData
            Some(OutputData {
                metadata,
                autoplay_state,
                loop_state,
                play_state,
                position,
                timeline,
                queue_length,
            })
        // There is nothing playing
        } else {
            None
        }
    // There is no voice connection
    } else {
        debug!("No voice connection");
        None
    }
}

async fn send_new_and_register(ctx: &Context, channel_id: ChannelId, data: OutputData) -> Message {
    let metadata = data.metadata.clone();
    let max_duration = if let Some(duration) = metadata.duration {
        duration
    } else {
        Duration::from_secs(0)
    };
    let position = if data.position > max_duration {
        max_duration
    } else {
        data.position
    };
    // Send message for buttons if this task has just been launched
    let m = CrabMessage::build(ctx.shard.clone(), ctx.http.clone(), channel_id)
        .embed(|e| build_now_playing_message(e, data, metadata, position, max_duration))
        .action_row(PlayerButtons::action_row())
        .message()
        .await
        .unwrap()
        .message;
    // Register new message id
    let mut storage_manager = ctx.storage().await;
    {
        storage_manager
            .update(|mut write| {
                write.config_mut().insert_button(channel_id, m.id);
            })
            .await
            .expect("Failed to insert new button message id");
    }
    m
}

fn build_now_playing_message(
    e: &mut serenity::builder::CreateEmbed,
    data: OutputData,
    metadata: Metadata,
    position: Duration,
    max_duration: Duration,
) -> &mut serenity::builder::CreateEmbed {
    e.title(format!("__{}__", data.play_state.clone()));
    e.description(format!(
        "**[{}]({})**\n\n{}\n{} / {}",
        metadata
            .title
            // Fix title appearing as spoiler text, by sneaking some zero-width-space in.
            // Yes, backslash escape works too but this is cooler.
            .map(|title| title.replace("||", "|\u{200B}|"))
            .unwrap(),
        metadata.source_url.unwrap(),
        data.timeline,
        duration_from_std(position),
        duration_from_std(max_duration)
    ));
    if let Some(thumbnail) = metadata.thumbnail {
        e.thumbnail(thumbnail);
    }
    e.color(Colour::from_hex("40FF40"));
    e.footer(|f| {
        f.text(format!(
            "Autoplay: {} | Loop: {} | {} in queue",
            data.autoplay_state.clone(),
            data.loop_state.clone(),
            data.queue_length
        ))
    });
    e
}

fn duration_from_std(duration: Duration) -> String {
    let timeleft;
    if duration.as_secs() < 60 {
        let seconds = fix_zeros(duration.as_secs().to_string());
        timeleft = "00:".to_string() + seconds.as_str();
    } else if duration.as_secs() >= 60 && duration.as_secs() <= 3600 {
        let seconds = fix_zeros((duration.as_secs() % 60).to_string());
        let minutes = fix_zeros((duration.as_secs() / 60).to_string());
        timeleft = minutes + ":" + seconds.as_str();
    } else {
        let seconds = fix_zeros((duration.as_secs() % 60).to_string());
        let minutes = fix_zeros(((duration.as_secs() / 60) % 60).to_string());
        let hours = fix_zeros((duration.as_secs() / 3600).to_string());
        timeleft = hours + ":" + minutes.as_str() + ":" + seconds.as_str();
    }
    timeleft
}

fn fix_zeros(mut string: String) -> String {
    if string.len() == 1 {
        string = "0".to_string() + string.as_str()
    }
    string
}
