use crate::storage::manager::ContextStorageManager;
use serenity::{client::Context, model::id::GuildId};
use songbird::Call;
use std::sync::Arc;
use tokio::sync::Mutex;

pub mod autoplay;
pub mod helper_loop;
pub mod helper_player;
pub mod join_to_play;
pub mod lyrics;
pub mod now_playing;
pub mod player;
pub mod playlist;
pub mod queue;
pub mod spotify;
pub mod toggle_pause;

// Store the queue in storage
pub async fn store_playing(ctx: &Context, guild_id: GuildId, handler_lock: Arc<Mutex<Call>>) {
    let handler = handler_lock.lock().await;
    let queue = handler.queue();
    let tracks = queue.current_queue();
    let _urls = tracks
        .into_iter()
        .filter_map(|track_handle| track_handle.metadata().source_url.clone())
        .collect::<Vec<String>>();
    if let Some(channel) = handler.current_channel() {
        let mut manager = ctx.storage().await;
        manager
            .update(|mut write| {
                let mut guild_data = write
                    .kv()
                    .get(guild_id.as_u64())
                    .cloned()
                    .unwrap_or_default();
                //guild_data.queue = urls.clone();

                guild_data.playing_in = Some(channel.0);
                write.kv_mut().insert(*guild_id.as_u64(), guild_data);
            })
            .await
            .expect("Failed to update queue");
        debug!("Stored current voice channel: {channel:#?}");
    } else {
        error!("Failed to store queue: We are playing right … right?!");
    }
}
