use crate::{action_queue::ContextActionQueue, commands::CommandContext, msg_send, music};

pub async fn join_to_play(ctx: &CommandContext) {
    let guild = ctx.guild().unwrap();
    let guild_id = guild.id;

    let channel_id = guild
        .voice_states
        .get(&ctx.user().id)
        .and_then(|voice_state| voice_state.channel_id);

    if channel_id.is_none() {
        msg_send!(ctx, "Please use +join *channel* or be in a voice channel yourself before using this command!")
    } else {
        let connect_to = channel_id.unwrap();
        let manager = songbird::get(ctx.ctx())
            .await
            .expect("Songbird Voice client placed in at initialisation.")
            .clone();
        let (_handler_lock, success) = manager.join(guild_id, connect_to).await;
        // Register action for this guild to play titles from the queue stored in CrabData
        // This is done here (and in player::join) to ensure that the task is running if a song is beeing queued by player::play
        let action_queue = ctx.ctx().action_queue().await;
        let mut write = action_queue.write().await;
        let ctx_clone = ctx.ctx().clone();
        write
            .register_action(
                100,
                false,
                music::queue::QueueEnqueueNewAction {
                    ctx: ctx_clone,
                    guild_id,
                },
            )
            .await;

        if let Ok(_channel) = success {
            msg_send!(ctx, "Joined your channel!")
        } else {
            match connect_to.to_string().as_str() {
                "" => msg_send!(ctx, "Error joining the channel"),
                &_ => (),
            }
        }
    }
}
