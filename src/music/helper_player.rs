use crate::storage::crab_data::TrackEntry;
use songbird::{input::Restartable, Call};
use std::{
    io::Write,
    process::{Command, Stdio},
    str::from_utf8,
    sync::Arc,
};
use tokio::sync::Mutex;

/// Load a youtube video with youtube-dl and enqeue it into songbirds source
///
/// # Arguments:
///
/// * `url` – A valid youtube url or search terms.
///
/// # Returns:
/// `Ok` if the operation was successful, otherwise `Err` (eg. the url was not valid).
pub async fn play_title(url: String, handler_lock: Arc<Mutex<Call>>) -> Result<(), anyhow::Error> {
    let source = if !url.starts_with("http") {
        match Restartable::ytdl_search(url, true).await {
            Ok(source) => source,
            Err(why) => {
                println!("Err starting source: {:?}", why);
                return Err(anyhow::Error::new(why));
            }
        }
    } else {
        // Here, we use lazy restartable sources to make sure that we don't pay
        // for decoding, playback on tracks which aren't actually live yet.
        match Restartable::ytdl(url, true).await {
            Ok(source) => source,
            Err(why) => {
                println!("Err starting source: {:?}", why);
                return Err(anyhow::Error::new(why));
            }
        }
    };

    let mut handler = handler_lock.lock().await;
    handler.enqueue_source(source.into());
    Ok(())
}

/// Get video info using youtube-dl. This should also work for non-url queries (eg. search).
pub async fn youtube_info(mut url: String) -> Option<TrackEntry> {
    // Fix searching for queries like foo:bar, 11:11
    if url.contains(':') && !url.starts_with("http") {
        url = "ytsearch:".to_string() + &url;
    }
    let mut command = Command::new("youtube-dlc")
        .arg("-j")
        .arg("--default-search=ytsearch")
        .arg("-a-")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("failed to execute process");
    {
        let stdin = command.stdin.as_mut().unwrap();
        stdin.write_all(url.as_bytes()).unwrap();
        stdin.flush().expect("Failed to write to std-in");
    }

    let cmd_res = command.wait_with_output().unwrap();
    let stdout = cmd_res.stdout;
    let stderr = cmd_res.stderr;
    let output = from_utf8(&stdout).unwrap();
    let output_err = from_utf8(&stderr).unwrap();
    debug!("{output_err}");

    let parsed_json = json::parse(output).unwrap_or_else(|e| {
        warn!("Failed to parse json: {:?}", e);
        json::object! {}
    });

    if let Some(title) = parsed_json["title"].as_str() {
        let artist = parsed_json["uploader"].as_str().unwrap_or("").to_string();
        let original_url = parsed_json["webpage_url"].as_str().unwrap().to_string();
        Some(TrackEntry {
            title: title.to_string(),
            artist,
            url: Some(original_url),
            enqueued: false,
        })
    } else {
        None
    }
}
