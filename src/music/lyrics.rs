use crate::{
    commands::CommandContext,
    message_box::{CrabMessage, FromHex},
    msg_send,
    utils::do_fetch,
};
use anyhow::Result;
use regex::Regex;
use reqwest::Client;
use select::{document::Document, predicate::Attr};
use serenity::utils::Colour;
use std::env;
use urlencoding::encode;

pub struct LyricsData {
    track_name: String,
    artist_name: String,
    thumbnail: String,
    lyrics: String,
    lyrics_link: String,
}

pub async fn musixmatch_get(query: String) -> Result<String> {
    let musix_client = Client::default();
    let response = musix_client
        .get(query)
        .header("Accept", "application/json")
        .send()
        .await?
        .text()
        .await?;
    Ok(response)
}

pub async fn get_lyrics(song_data: String) -> Option<LyricsData> {
    debug!("{song_data}");
    // Get API key for Genius API
    let api_key = env::var("GENIUS_TOKEN").unwrap();
    // Rewrite non-alphanumeric characters for the link
    let en_name = encode(&song_data);
    // Build the link for Genius
    let link = format!(
        "https://api.genius.com/search?q={0}&access_token={1}",
        en_name, api_key
    );
    // Initialize output data
    let lyrics_link;
    let thumbnail;
    let track_name;
    let artist_name;
    // Search lyrics on genius
    let parsed_json =
        json::parse(do_fetch(link).await.unwrap().as_str()).unwrap_or_else(|_| json::object! {});
    let new_parsed_json;
    // Check if there are lyrics on genius
    if parsed_json["response"]["hits"][0]["result"]["url"]
        .as_str()
        .is_some()
    {
        // Update output data
        lyrics_link = parsed_json["response"]["hits"][0]["result"]["url"]
            .as_str()
            .unwrap();
        thumbnail = parsed_json["response"]["hits"][0]["result"]["song_art_image_thumbnail_url"]
            .as_str()
            .unwrap();
        track_name = parsed_json["response"]["hits"][0]["result"]["title"]
            .as_str()
            .unwrap();
        artist_name = parsed_json["response"]["hits"][0]["result"]["artist_names"]
            .as_str()
            .unwrap();
    // If there are no lyrics for this song name then fix the song name by searching on musixmatch.
    } else {
        // Get API key for Musixmatch API
        let musix_api_key = env::var("MUSIXMATCH_TOKEN").unwrap();
        // Build the link for Musixmatch
        let musix_search_url = format!(
            "https://api.musixmatch.com/ws/1.1/matcher.track.get?q_track={0}&apikey={1}",
            en_name, musix_api_key
        );
        // Check for new song data on Musixmatch
        if let Ok(musix_search_body) = musixmatch_get(musix_search_url).await {
            let musix_parsed_json =
                json::parse(musix_search_body.as_str()).unwrap_or_else(|_| json::object! {});
            // Get song name and artist name from Musixmatch
            if let Some(name) = musix_parsed_json["message"]["body"]["track"]["track_name"].as_str()
            {
                let musix_track_name = encode(name);
                let musix_artist_name = encode(
                    musix_parsed_json["message"]["body"]["track"]["artist_name"]
                        .as_str()
                        .unwrap(),
                );
                // Build a new link for Genius
                let new_link = format!(
                    "https://api.genius.com/search?q={0}&access_token={1}",
                    musix_artist_name.to_string() + " " + &musix_track_name,
                    api_key
                );
                // Search lyrics on genius with new song name
                new_parsed_json = json::parse(do_fetch(new_link).await.unwrap().as_str())
                    .unwrap_or_else(|_| json::object! {});
                if new_parsed_json["response"]["hits"][0]["result"]["url"]
                    .as_str()
                    .is_some()
                {
                    // Update output data
                    lyrics_link = new_parsed_json["response"]["hits"][0]["result"]["url"]
                        .as_str()
                        .unwrap();
                    thumbnail = new_parsed_json["response"]["hits"][0]["result"]
                        ["song_art_image_thumbnail_url"]
                        .as_str()
                        .unwrap();
                    track_name = new_parsed_json["response"]["hits"][0]["result"]["title"]
                        .as_str()
                        .unwrap();
                    artist_name = new_parsed_json["response"]["hits"][0]["result"]["artist_names"]
                        .as_str()
                        .unwrap();
                // If there is nothing found on Genius and Musixmatch, send a message and return.
                } else {
                    return None;
                }
            // If there is nothing found on Musixmatch, send a message and return.
            } else {
                return None;
            }
        // If there is nothing found on Musixmatch, send a message and return.
        } else {
            return None;
        }
    }
    // If there are lyrics, then parse them to a String.
    let fetch_data = do_fetch(lyrics_link.to_string()).await.unwrap();
    let mut html = String::new();
    {
        let document = Document::from(fetch_data.as_str());
        for node in document.find(Attr("data-lyrics-container", "true")) {
            html += node.html().as_str();
        }
    }
    // Format lyrics by replacing HTML elements with Discord markdown
    let mut lyrics = html
        .replace(
            "<div data-lyrics-container=\"true\" class=\"Lyrics__Container-sc-1ynbvzw-6 lgZgEN\">",
            " ",
        )
        .replace("<br>", "\n")
        .replace("</div>", "\n")
        .replace("<b>", "**")
        .replace("</b>", "**")
        .replace("<i>", "*")
        .replace("</i>", "*")
        .replace("&amp;", "&");

    // Remove all other HTML elements to get lyrics alone
    let re = Regex::new("<[^>]*>").unwrap();
    lyrics = re.replace_all(&lyrics, "").to_string();
    Some(LyricsData {
        track_name: track_name.to_string(),
        artist_name: artist_name.to_string(),
        thumbnail: thumbnail.to_string(),
        lyrics,
        lyrics_link: lyrics_link.to_string(),
    })
}

pub async fn output_lyrics(ctx: &CommandContext) {
    let mut data;
    // Get args from message
    let args = ctx.args();
    let search_words: &str = args.rest().trim();
    // If there are args, use them as data.
    if !search_words.is_empty() {
        let song_data = search_words.to_string();
        // Search lyrics with args as data
        if let Some(lyrics_data) = get_lyrics(song_data.clone()).await {
            data = lyrics_data
        } else {
            msg_send!(ctx, format!("Couldn't find lyrics for \"`{song_data}`\"."));
            return;
        }
    // If there are no args, get metadata from the currently playing song in the queue.
    } else {
        let guild_id = ctx.guild().unwrap().id;
        let manager = songbird::get(ctx.ctx())
            .await
            .expect("Songbird Voice client placed in at initialisation.")
            .clone();
        // Check if there is a voice chat connection
        if let Some(handler_lock) = manager.get(guild_id) {
            let handler = handler_lock.lock().await;
            // Check if there is something playing
            if handler.queue().current().is_none() {
                msg_send!(ctx, "Nothing is playing right now.");
                return;
            } else {
                // Get the queue
                let queue = handler.queue();
                // Get the metadata from the currently playing song
                let metadata = queue.current().unwrap().metadata().clone();
                // Get the title of the song
                let mut title = metadata.title.clone().unwrap();
                // Fix the title of the song by removing everything between brackets
                while let Some(start) = title.find('(') {
                    if let Some(end) = title.find(')') {
                        title.replace_range(start..end + 1, "");
                    }
                }
                while let Some(start) = title.find('[') {
                    if let Some(end) = title.find(']') {
                        title.replace_range(start..end + 1, "");
                    }
                }
                while let Some(start) = title.find('【') {
                    if let Some(end) = title.find('】') {
                        title.replace_range(start..end + 1, "");
                    }
                }
                // Filter the title
                title = title
                    .to_lowercase()
                    .replace("official", "")
                    .replace("music", "")
                    .replace("video", "")
                    .replace("hd", "")
                    .replace('「', " ")
                    .replace('」', " ")
                    .replace('・', "")
                    .replace('"', "")
                    .replace('&', "")
                    .replace('/', "")
                    .replace('„', "")
                    .replace('”', "")
                    .replace(',', "")
                    .replace('.', "")
                    .replace('\'', "")
                    .replace('!', "")
                    .replace(" x ", " ")
                    .replace('-', "")
                    .replace('–', "")
                    .replace('_', "")
                    .replace('.', "")
                    .replace('~', "")
                    .replace(':', "")
                    .replace("feat", "")
                    .replace("mv", "")
                    .replace("lyrics", "")
                    .split("ver")
                    .collect::<Vec<&str>>()[0]
                    .split("ft")
                    .collect::<Vec<&str>>()[0]
                    .split('|')
                    .collect::<Vec<&str>>()[0]
                    .trim()
                    .to_string();
                while title.contains("  ") {
                    title = title.replace("  ", " ");
                }
                // Remove all characters from other languages
                let regex = Regex::new(r"([^a-zA-Z\d\s!-¿’еß„“「」【】・”–])").unwrap();
                // title = regex.replace_all(&title, "").to_string();
                debug!("searching lyrics for... {title}");
                // Get the lyrics
                if let Some(lyrics_data) = get_lyrics(title.clone()).await {
                    data = lyrics_data;
                    // Find characters from other languages in the lyrics
                    if regex.is_match(&data.lyrics) {
                        let mut unknown = String::new();
                        for capture in regex.captures(&data.lyrics).unwrap().iter() {
                            unknown += capture.unwrap().as_str()
                        }
                        debug!("found characters from other languages in the lyrics: {unknown}");
                        title += " romanized";
                        debug!("searching lyrics for... {title}");
                        // Get romanized lyrics
                        if let Some(lyrics_data) = get_lyrics(title.clone()).await {
                            data = lyrics_data
                        }
                    }
                } else {
                    msg_send!(ctx, format!("Couldn't find lyrics for \"`{title}`\"."));
                    return;
                }
            }
        } else {
            msg_send!(ctx, "Nothing is playing right now.");
            return;
        }
    }
    // Send the message containing lyrics
    if let Err(why) = CrabMessage::from_ctx(ctx)
        .embed(|e| {
            e.title("Lyrics for \"".to_string() + &data.track_name + "\" by " + &data.artist_name);
            e.description(data.lyrics.trim());
            e.thumbnail(data.thumbnail);
            e.url(data.lyrics_link);
            e.color(Colour::from_hex("40FF40"));
            e
        })
        .reference(ctx.ref_msg())
        .message()
        .await
    {
        error!("Error sending message: {:?}", why);
    }
}
