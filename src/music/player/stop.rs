use crate::{msg_send, music::store_playing, storage::manager::ContextStorageManager};
use serenity::{
    client::Context,
    model::id::{ChannelId, GuildId},
};

pub async fn stop(ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    if let Some(call) = manager.get(guild_id) {
        {
            let handler = call.lock().await;
            let queue = handler.queue();
            queue.stop();
        }

        let mut storage_manager = ctx.storage().await;
        let mut queue = storage_manager
            .read()
            .await
            .kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
            .queue;
        queue.clear();
        storage_manager
            .update(|mut write| {
                write.kv_mut().entry(*guild_id.as_u64()).or_default().queue = queue.clone();
            })
            .await
            .expect("Could not clear queue");

        // Store the queue in storage
        store_playing(ctx, guild_id, call).await;
        msg_send!(ctx, channel_id, "Queue cleared.")
    } else {
        msg_send!(ctx, channel_id, "Not in a voice channel to play in.")
    }
}
