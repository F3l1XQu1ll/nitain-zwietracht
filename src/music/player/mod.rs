pub mod autoplay;
mod join;
mod leave;
mod play;
mod remove_song;
mod repeat;
mod seek;
mod skip;
mod stop;

use serenity::{
    client::Context,
    framework::standard::Args,
    model::id::{ChannelId, GuildId},
};

use crate::{commands::CommandContext, error::CommandHandleResult};

pub async fn join(ctx: &CommandContext) {
    join::join(ctx).await
}

pub async fn leave(ctx: &CommandContext) {
    leave::leave(ctx).await
}

pub async fn play(ctx: &CommandContext) {
    play::play(ctx).await
}

pub async fn skip(ctx: &Context, guild_id: GuildId, channel_id: ChannelId, args: Args) {
    skip::skip(ctx, guild_id, channel_id, args).await
}

pub async fn stop(ctx: &CommandContext) {
    stop::stop(ctx.ctx(), ctx.guild().unwrap().id, ctx.channel_id()).await
}

pub async fn autoplay(ctx: &CommandContext) {
    autoplay::autoplay(ctx).await
}

pub async fn autoplay_off(ctx: &CommandContext) -> CommandHandleResult {
    autoplay::autoplay_off(
        ctx.ctx(),
        ctx.guild()
            .ok_or_else(|| anyhow::anyhow!("No guild id available!"))?
            .id,
        ctx.channel_id(),
    )
    .await;
    Ok(())
}

pub async fn remove_song(ctx: &CommandContext) {
    remove_song::remove_song(ctx).await
}

pub async fn seek(ctx: &CommandContext) {
    seek::seek(ctx).await
}

pub async fn repeat(ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
    repeat::repeat(ctx, guild_id, channel_id).await
}
