use crate::{
    commands::CommandContext,
    msg_send,
    music::{now_playing, store_playing},
    storage::manager::ContextStorageManager,
};

pub async fn remove_song(ctx: &CommandContext) {
    let guild_id = ctx.guild().unwrap().id;
    let manager = songbird::get(ctx.ctx())
        .await
        .expect("Could not retrieve Songbird voice client.");

    if let Some(call) = manager.get(guild_id) {
        let remove_index: usize = match ctx.args().single::<usize>() {
            Ok(t) => t,
            Err(_) => {
                msg_send!(ctx, "No valid queue index supplied.");
                return;
            }
        };

        let mut storage_manager = ctx.storage().await;
        let mut queue = storage_manager
            .read()
            .await
            .kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
            .queue;
        if queue.is_empty() {
            msg_send!(ctx, "The queue is empty!")
        } else if queue.len() < remove_index || remove_index < 1 {
            msg_send!(ctx, "There is no song at this index!")
        } else if remove_index == 1 {
            msg_send!(ctx, "Can't remove currently playing song!")
        } else {
            /*
                        let handler = call.lock().await;
                        handler.queue().modify_queue(|v| {
                            v.remove(remove_index - 1);
                        });
                        drop(handler);
            */
            queue.remove(remove_index - 1);
            storage_manager
                .update(|mut write| {
                    write.kv_mut().entry(*guild_id.as_u64()).or_default().queue = queue.clone();
                })
                .await
                .expect("Could not remove track");
            msg_send!(ctx, &format!("Removed track #{}!", remove_index));
            now_playing::now_playing(ctx.ctx(), guild_id, ctx.channel_id).await;
        }

        store_playing(ctx.ctx(), guild_id, call).await;
    } else {
        msg_send!(ctx, "Not in a voice channel!")
    }
}
