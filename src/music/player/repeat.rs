use crate::{
    error::{ErrorContext, LogError},
    msg_send,
    storage::manager::ContextStorageManager,
};
use serenity::{
    client::Context,
    model::id::{ChannelId, GuildId},
};
use std::time::Duration;

pub async fn repeat(ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
    let manager = songbird::get(ctx)
        .await
        .expect("Could not retrieve Songbird voice client.");

    if let Some(call) = manager.get(guild_id) {
        let handler = call.lock().await;
        let track = handler
            .queue()
            .current()
            .expect("Failed to fetch handle for current track.");
        drop(handler);

        // check if track has played longer than 1 second
        if track
            .get_info()
            .await
            .map(|info| info.position > Duration::from_secs(5))
            .unwrap_or(false)
        {
            debug!(
                "Track position {:?}",
                track.get_info().await.unwrap().position
            );
            track
                .seek_time(Duration::from_secs(0))
                .expect("Failed to seek on track.");

            msg_send!(ctx, channel_id, "Track repeated.")
        } else {
            let mut crab_data = ctx.storage().await;
            let res = crab_data
                .update(|mut write| {
                    write
                        .kv_mut()
                        .entry(*guild_id.as_u64())
                        .or_default()
                        .queue
                        .prev()
                })
                .await
                .expect("Failed to update queue");

            if res.is_some() {
                call.lock()
                    .await
                    .queue()
                    .skip()
                    .context("Failed to skip current track")
                    .handle();
            }
        }
    } else {
        msg_send!(ctx, channel_id, "Not in a voice channel!")
    }
}
