use crate::{commands::CommandContext, msg_send, music::now_playing};
use serenity::framework::standard::{Args, Delimiter};
use std::time::Duration;

pub async fn seek(ctx: &CommandContext) {
    let guild_id = ctx.guild().unwrap().id;
    let manager = songbird::get(ctx.ctx())
        .await
        .expect("Could not retrieve Songbird voice client.");

    if let Some(call) = manager.get(guild_id) {
        let seek_time = match ctx.args().single::<String>() {
            Ok(t) => t,
            Err(_) => {
                msg_send!(ctx, "Please provide a valid timestamp.");
                return;
            }
        };

        let mut timestamp = Args::new(&seek_time, &[Delimiter::Single(':')]);
        let (minutes, seconds) = (timestamp.single::<u64>(), timestamp.single::<u64>());

        if minutes.as_ref().and(seconds.as_ref()).is_err() {
            msg_send!(ctx, "The provided timestamp is incorrect.");
            return;
        }

        let timestamp = minutes.unwrap() * 60 + seconds.unwrap();

        let handler = call.lock().await;
        let track = handler
            .queue()
            .current()
            .expect("Failed to fetch handle for current track.");
        drop(handler);

        track
            .seek_time(Duration::from_secs(timestamp))
            .expect("Failed to seek on track.");

        msg_send!(
            ctx,
            &format!("Seeked current track to **{}**!", seek_time)
        );
        now_playing::now_playing(ctx.ctx(), guild_id, ctx.channel_id()).await;
    } else {
        msg_send!(ctx, "Not in a voice channel!")
    }
}
