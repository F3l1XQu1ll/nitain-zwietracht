use crate::{msg_send, music::store_playing, storage::manager::ContextStorageManager};
use serenity::{
    client::Context,
    framework::standard::Args,
    model::id::{ChannelId, GuildId},
};

pub async fn skip(ctx: &Context, guild_id: GuildId, channel_id: ChannelId, mut args: Args) {
    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    if let Some(handler_lock) = manager.get(guild_id) {
        let handler = handler_lock.lock().await;
        let queue = handler.queue();
        let mut guild_data = {
            ctx.storage()
                .await
                .read()
                .await
                .kv()
                .get(guild_id.as_u64())
                .cloned()
                .unwrap_or_default()
        };
        let shared_queue = &mut guild_data.queue;
        // If there are no args then continue and skip once.
        if let Ok(args) = args.single::<String>() {
            // If there are args, try to parse them as usize.
            if let Ok(limit) = args.parse::<usize>() {
                dbg!(limit);
                // The number 1 is the current playing track.
                if limit == 1 {
                    msg_send!(ctx, channel_id, "Already playing that song.")
                // Anything under 1 is impossible.
                } else if limit < 1 {
                    msg_send!(ctx, channel_id, "No valid queue index supplied.")
                // Anything higher than the queue length is impossible too.
                } else if limit > shared_queue.len() {
                    msg_send!(ctx, channel_id, "No valid queue index supplied.")
                // Now after filtering, every number is possible.
                } else {
                    let l = (limit - 1) as usize;
                    // remove entries from shared queue
                    shared_queue.drain(1..l);
                    // store new guild data
                    ctx.storage()
                        .await
                        .update(|mut write| {
                            write
                                .kv_mut()
                                .insert(*guild_id.as_u64(), guild_data.clone());
                        })
                        .await
                        .expect("Failed to write to shared queue");
                    // now skip the song in songbird to enqueue a new entry from action queue
                    if let Err(e) = queue.skip() {
                        println!("{e}")
                    }
                    msg_send!(ctx, channel_id, format!("Skipped to song #{limit}."))
                }
                // Parsing as usize failed.
            } else {
                msg_send!(ctx, channel_id, "No valid queue index supplied.")
            };
        } else {
            // Skip once.
            if queue.is_empty() {
                msg_send!(ctx, channel_id, "Nothing to skip.")
            } else {
                // now remove the entry from the shared queue
                // action queue could do that too, but we might be too fast
                // Instead, we remove the entry and skip afterwards
                {
                    ctx.storage()
                        .await
                        .update(|mut write| {
                            write
                                .kv_mut()
                                .entry(*guild_id.as_u64())
                                .or_default()
                                .queue
                                .next();
                        })
                        .await
                        .expect("Failed to write to shared queue");
                }

                // skip the current song in the songbird queue
                // now the action queue does not have to remove the entry anymore
                if let Err(e) = queue.skip() {
                    println!("{e}")
                }

                // read shared queue again (no async in update and we want to avoid holding the lock for too long)
                let shared_queue = ctx
                    .storage()
                    .await
                    .read()
                    .await
                    .kv()
                    .get(guild_id.as_u64())
                    .cloned()
                    .unwrap_or_default()
                    .queue;
                msg_send!(
                    ctx,
                    channel_id,
                    format!(
                        "Song skipped: {} in queue.",
                        if !shared_queue.is_empty() {
                            (shared_queue.len()).to_string()
                        } else {
                            "none".to_string()
                        }
                    ),
                )
            }
        }
        drop(handler);
        store_playing(ctx, guild_id, handler_lock).await;
    } else {
        msg_send!(ctx, channel_id, "Not in a voice channel to play in.");
    }
}
