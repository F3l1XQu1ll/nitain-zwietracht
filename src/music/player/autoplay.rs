use crate::{
    commands::CommandContext,
    msg_send,
    music::{autoplay, now_playing},
    storage::manager::ContextStorageManager,
    utils::is_playing,
};
use serenity::{
    client::Context,
    model::id::{ChannelId, GuildId},
};

pub async fn autoplay(ctx: &CommandContext) {
    let guild_id = ctx.guild_id().unwrap();
    if is_playing(ctx.ctx(), guild_id).await {
        {
            let mut storage_manager = ctx.storage().await;
            let mut guild_data = {
                let read = storage_manager.read().await;
                read.kv()
                    .get(guild_id.as_u64())
                    .cloned()
                    .unwrap_or_default()
            };

            let state = guild_data.autoplay_state;

            if state {
                msg_send!(ctx, "Autoplay was already enabled.")
            } else {
                guild_data.autoplay_state = true;
                // update autoplay state before calling output_autoplay
                storage_manager
                    .update(|mut write| {
                        write
                            .kv_mut()
                            .insert(*guild_id.as_u64(), guild_data.clone());
                    })
                    .await
                    .expect("Failed to update guild data");
                msg_send!(ctx, "Autoplay enabled.");
                autoplay::autoplay_task(ctx.ctx(), guild_id).await;
            }
        }
        now_playing::now_playing(ctx.ctx(), guild_id, ctx.channel_id()).await;
    } else {
        msg_send!(ctx, "Nothing is playing right now.")
    }
}

pub async fn autoplay_off(ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
    msg_send!(ctx, channel_id, "Autoplay disabled.");
    now_playing::now_playing(ctx, guild_id, channel_id).await;
    let mut storage_manager = ctx.storage().await;
    storage_manager
        .update(|mut write| {
            let mut guild_data = write
                .kv()
                .get(guild_id.as_u64())
                .cloned()
                .unwrap_or_default();
            guild_data.autoplay_state = false;
            guild_data.autoplay_list = Vec::new();
            write.kv_mut().insert(*guild_id.as_u64(), guild_data);
        })
        .await
        .expect("Failed to update guild data");
    //autoplay_enable = false;
}
