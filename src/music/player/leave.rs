use super::autoplay::autoplay_off;
use crate::{commands::CommandContext, msg_send, storage::manager::ContextStorageManager};

pub async fn leave(ctx: &CommandContext) {
    let guild = ctx.guild().unwrap();
    let guild_id = guild.id;

    let manager = songbird::get(ctx.ctx())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    let has_handler = manager.get(guild_id).is_some();

    if has_handler {
        let state = {
            let storage_manager = ctx.storage().await;
            let read = storage_manager.read().await;
            let guild_data = read
                .kv()
                .get(guild_id.as_u64())
                .cloned()
                .unwrap_or_default();
            guild_data.autoplay_state
        };
        if state {
            autoplay_off(ctx.ctx(), guild_id, ctx.channel_id()).await;
        }

        let mut storage = ctx.storage().await;
        storage
            .update(|mut write| {
                let mut kv = write
                    .kv()
                    .get(guild_id.as_u64())
                    .cloned()
                    .unwrap_or_default();
                kv.queue.clear();
                kv.playing_in = None;
                kv.paused_state = false;
                write.kv_mut().insert(*guild_id.as_u64(), kv);
            })
            .await
            .expect("[leave]Failed to update guild state");

        if let Err(e) = manager.remove(guild_id).await {
            msg_send!(ctx, format!("Failed: {:?}", e))
        }
        msg_send!(ctx, "Left voice channel.")
    } else {
        msg_send!(ctx, "Not in a voice channel.")
    }
}
