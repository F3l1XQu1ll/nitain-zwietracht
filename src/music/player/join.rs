use crate::{
    action_queue::ContextActionQueue, commands::CommandContext, message_box::CrabMessage, msg_send,
    music::store_playing,
};

pub async fn join(ctx: &CommandContext) {
    info!("handling join request");
    let guild = ctx.guild().unwrap();
    let guild_id = guild.id;

    let args = ctx.args();
    let arguments: Option<&str> = args.current();
    let mut channel_id = None;
    if let Some(channel_name) = arguments {
        for (curr_channel_id, channel) in guild.channels(&ctx).await.unwrap() {
            if channel.is_text_based() {
                continue;
            }

            if curr_channel_id.to_string() == channel_name
                || channel.name.to_lowercase() == channel_name.to_lowercase()
            {
                channel_id = Option::from(curr_channel_id);
                break;
            }
        }
    } else {
        channel_id = guild
            .voice_states
            .get(&ctx.user().id)
            .and_then(|voice_state| voice_state.channel_id);
    }

    let connect_to = match channel_id {
        Some(channel) => channel,
        None => {
            if arguments.is_some() {
                msg_send!(
                    ctx.ctx(),
                    ctx.channel_id(),
                    "There is no such voice channel."
                )
            } else {
                msg_send!(ctx, "Not in a voice channel.")
            }
            return;
        }
    };

    let manager = songbird::get(ctx.ctx())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    let (handler_lock, success) = manager.join(guild_id, connect_to).await;
    // Register action for this guild to play titles from the queue stored in CrabData
    // This is done here (and in join_to_play) to ensure that the task is running if a song is beeing queued by player::play
    let action_queue = ctx.ctx().action_queue().await;
    let mut write = action_queue.write().await;
    let ctx_clone = ctx.ctx().clone();
    write
        .register_action(
            100,
            false,
            crate::music::queue::QueueEnqueueNewAction {
                ctx: ctx_clone,
                guild_id,
            },
        )
        .await;

    if let Ok(_channel) = success {
        CrabMessage::from_ctx(ctx)
            .title("Joined your channel!")
            // .description(format!(
            //     "**Waiting in [{}](https://discord.com/channels/{guild_id}/{connect_to})**",
            //     connect_to.name(&ctx).await.unwrap_or_default()
            // ))
            .reference(ctx.ref_msg())
            .message()
            .await
            .expect("Failed to send message");
    } else {
        match connect_to.to_string().as_str() {
            "" => msg_send!(ctx, "Error joining the voice channel."),
            &_ => msg_send!(ctx, "Already in a voice channel."),
        }
    }
    // Store the queue in storage
    store_playing(ctx.ctx(), guild_id, handler_lock).await;
}
