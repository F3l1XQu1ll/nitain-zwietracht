use crate::{
    commands::CommandContext,
    msg_send,
    music::{helper_player, join_to_play, now_playing, playlist, spotify, store_playing},
    storage::manager::ContextStorageManager,
};

pub async fn play(ctx: &CommandContext) {
    let mut url = ctx
        .args()
        .rest()
        .replace('<', "")
        .replace('>', "")
        .split('&')
        .collect::<Vec<&str>>()[0]
        .trim()
        .to_string();
    // Handle empty args
    if url.is_empty() {
        msg_send!(
            ctx,
            "Please give me a link or a song name to search. `+p [link/song name]",
        )
    } else {
        let guild = ctx.guild().unwrap();
        let guild_id = guild.id;
        let manager = songbird::get(ctx.ctx())
            .await
            .expect("Songbird Voice client placed in at initialisation.")
            .clone();

        // Try to join a voice channel
        if manager.get(guild_id).is_none() {
            join_to_play::join_to_play(ctx).await;
        }

        // If there is no voice channel available, there is no action
        if let Some(handler_lock) = manager.get(guild_id) {
            if url.contains("album") && url.contains("spotify.") {
                let id = url.clone().split('?').collect::<Vec<&str>>()[0]
                    .replace("https://open.spotify.com/album/", "")
                    .trim()
                    .to_string();
                spotify::play_spotify_album(id, ctx).await;
            } else if url.contains("playlist") {
                if url.contains("spotify.") {
                    let id = url.clone().split('?').collect::<Vec<&str>>()[0]
                        .replace("https://open.spotify.com/playlist/", "")
                        .trim()
                        .to_string();
                    spotify::play_spotify_playlist(id, ctx).await;
                } else {
                    playlist::playlist(url.as_str(), ctx).await.unwrap();
                }
            } else {
                msg_send!(ctx, "Loading...");
                if url.contains("spotify.") && url.contains("track") {
                    let id = url.clone().split('?').collect::<Vec<&str>>()[0]
                        .replace("https://open.spotify.com/track/", "")
                        .trim()
                        .to_string();
                    if let Some(result) = spotify::get_track(id).await {
                        url = result.name + " " + result.artist.as_str();
                    } else {
                        // FIXME: Do we handle the broken url case then?
                        msg_send!(ctx, "Spotify did not return any results :sob:")
                    }
                }
                // Append youtube entries to shared queue. The tracks will be enqueued by ActionQueue
                if let Some(data) = helper_player::youtube_info(url).await {
                    ctx.storage()
                        .await
                        .update(|mut write| {
                            write
                                .kv_mut()
                                .entry(*guild_id.as_u64())
                                .or_default()
                                .queue
                                .push_back(data.clone())
                        })
                        .await
                        .expect("Could not write to shared queue!");
                    msg_send!(ctx, format!("Added `{}` to queue!", data.title))
                } else {
                    msg_send!(
                        ctx,
                        "Could not find this video on YouTube. Is your link correct?"
                    )
                }
            }
            // Remember the channel to use it in the action queue
            ctx.storage()
                .await
                .update(|mut write| {
                    write
                        .kv_mut()
                        .entry(*guild_id.as_u64())
                        .or_default()
                        .player_interaction = Some(ctx.channel_id())
                })
                .await
                .expect("Failed to store last player interaction (play)");
            now_playing::now_playing(ctx.ctx(), guild_id, ctx.channel_id()).await;

            // Store the queue in storage
            store_playing(ctx.ctx(), ctx.guild_id().unwrap(), handler_lock).await;
        } else {
            debug!("Failed to join a voice channel")
        }
    }
}
