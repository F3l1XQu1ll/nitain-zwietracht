use super::*;
use crate::{commands::CommandContext, msg_send, storage::crab_data::TrackEntry};
use futures::{stream, StreamExt};
use rspotify::{
    clients::BaseClient,
    model::{AlbumId, Id, IncludeExternal, PlaylistId, SearchResult, TrackId},
    ClientCredsSpotify, Credentials,
};
use serenity::{
    client::Context,
    model::prelude::{
        component::ButtonStyle,
        interaction::{message_component::MessageComponentInteraction, InteractionResponseType},
    },
};
use std::{sync::Arc, time::Duration};

pub struct SpotifySearchResult {
    pub name: String,
    pub artist: String,
    pub album: Option<String>,
    pub link: Option<String>,
}

async fn client() -> Option<ClientCredsSpotify> {
    let mut client = ClientCredsSpotify::new(Credentials::new(
        "3d938da8922f4fd988ccee3eb3701374",
        "82386c4458e7465bb3b457cdfce3b493",
    ));
    if client.request_token().await.is_ok() {
        Some(client)
    } else {
        None
    }
}

// -> (name, artist)
/// Get spotify track info by track id
pub async fn get_track(id: String) -> Option<SpotifySearchResult> {
    if let Some(client) = client().await {
        let track_id = TrackId::from_id(&id).unwrap();
        let search_result = client.track(&track_id).await;
        match search_result {
            Ok(track) => {
                let track_artist = track.artists[0].name.clone();
                let track_name = track.name;
                let search_result = SpotifySearchResult {
                    name: track_name,
                    artist: track_artist,
                    album: Some(track.album.name),
                    link: track.href,
                };
                Some(search_result)
            }
            _ => None,
        }
    } else {
        None
    }
}

pub async fn play_spotify_playlist(query: String, ctx: &CommandContext) {
    if let Some(client) = client().await {
        // Get playlist ID
        let id = PlaylistId::from_id(&query).unwrap();
        // Get playlist data
        let search_result = client.playlist(&id, None, None).await;
        match search_result {
            // Playlist data found
            Ok(playlist) => {
                // Get the total number of tracks
                let length = playlist.tracks.total;
                // Send message with the number of tracks
                msg_send!(ctx, format!("Adding {} songs to queue...", length));
                // Set the number of items returned per page
                let limit = 50;
                let mut offset = 0;
                // Get all tracks from each page
                loop {
                    // Get a page based of the offset
                    let page = client
                        .playlist_items_manual(&id, None, None, Some(limit), Some(offset))
                        .await
                        .unwrap();

                    let tracks = stream::iter(page.items.iter())
                        .filter_map(|item| {
                            async move {
                                // If there is data, insert the Track into the queue.
                                if let Some(track) = item.track.as_ref() {
                                    if let Some(track_id) = track.id() {
                                        if let Some(info) =
                                            get_track(track_id.id().to_string()).await
                                        {
                                            let track_info = TrackEntry {
                                                title: info.name,
                                                artist: info.artist,
                                                url: None,
                                                enqueued: false,
                                            };
                                            Some(track_info)
                                        } else {
                                            None
                                        }
                                    } else {
                                        None
                                    }
                                } else {
                                    None
                                }
                            }
                        })
                        .collect::<Vec<TrackEntry>>()
                        .await;

                    // Add tracks to shared queue
                    if !tracks.is_empty() {
                        let guild_id = ctx.guild().unwrap().id;
                        ctx.storage()
                            .await
                            .update(|mut write| {
                                write
                                    .kv_mut()
                                    .entry(*guild_id.as_u64())
                                    .or_default()
                                    .queue
                                    .extend(tracks.clone());
                            })
                            .await
                            .expect("Failed to write to shared queue");
                    }
                    // The iteration ends when the `next` field is `None`.
                    // Otherwise, the Spotify API will keep returning empty lists from then on.
                    if page.next.is_none() {
                        break;
                    }
                    offset += limit;
                }
            }
            // No data found on Spotify
            _ => msg_send!(ctx, "Spotify did not return any results :sob:"),
        }
    }
}

pub async fn play_spotify_album(query: String, ctx: &CommandContext) {
    if let Some(client) = client().await {
        // Get album ID
        let id = AlbumId::from_id(&query).unwrap();
        // Get album data
        let search_result = client.album(&id).await;
        match search_result {
            // Album data found
            Ok(album) => {
                // Get the total number of tracks
                let length = album.tracks.total;
                // Send message with the number of tracks
                msg_send!(ctx, format!("Adding {} songs to queue...", length));
                // Set the number of items returned per page
                let limit = 50;
                let mut offset = 0;

                // Get all tracks from each page
                loop {
                    // Get a page based of the offset
                    let page = client
                        .album_track_manual(&id, Some(limit), Some(offset))
                        .await
                        .unwrap();
                    let tracks = stream::iter(page.items)
                        .filter_map(|item| async move {
                            // If there is data, insert the Track into the queue.
                            if let Some(info) = get_track(item.id.unwrap().id().to_string()).await {
                                let track_info = TrackEntry {
                                    title: info.name,
                                    artist: info.artist,
                                    url: None,
                                    enqueued: false,
                                };
                                Some(track_info)
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<TrackEntry>>()
                        .await;

                    // Add tracks to shared queue

                    if !tracks.is_empty() {
                        let guild_id = ctx.guild().unwrap().id;
                        ctx.ctx()
                            .storage()
                            .await
                            .update(|mut write| {
                                write
                                    .kv_mut()
                                    .entry(*guild_id.as_u64())
                                    .or_default()
                                    .queue
                                    .extend(tracks.clone());
                            })
                            .await
                            .expect("Failed to write to shared queue");
                    }
                    // The iteration ends when the `next` field is `None`.
                    // Otherwise, the Spotify API will keep returning empty lists from then on.
                    if page.next.is_none() {
                        break;
                    }
                    offset += limit
                }
            }
            // No data found on Spotify
            _ => msg_send!(ctx, "Spotify did not return any results :sob:"),
        }
    }
}

pub async fn search(
    query: String,
    search_type: rspotify::model::SearchType,
) -> Option<SpotifySearchResult> {
    if let Some(client) = client().await {
        let search_result = client
            .search(
                &query,
                &search_type,
                None,
                Some(&IncludeExternal::Audio),
                Some(1),
                None,
            )
            .await;
        match search_result {
            Ok(SearchResult::Tracks(tracks)) => {
                if tracks.items.is_empty() {
                    return None;
                }
                let track = tracks.items[0].clone();
                let track_name = track.name.clone();
                let track_artist = track.artists[0].name.clone();
                let track_album = track.album.name.clone();
                let link = track.external_urls.get("spotify").cloned();
                let search_result = SpotifySearchResult {
                    name: track_name,
                    artist: track_artist,
                    album: Some(track_album),
                    link,
                };
                Some(search_result)
            }
            Ok(SearchResult::Playlists(playlists)) => {
                if playlists.items.is_empty() {
                    return None;
                }
                let playlist = &playlists.items[0];
                let playlist_name = playlist.name.clone();
                let playlist_artist = playlist.owner.clone();
                let playlist_album = None;
                let playlist_url = playlist.external_urls.get("spotify").cloned();
                let search_result = SpotifySearchResult {
                    name: playlist_name,
                    artist: playlist_artist
                        .display_name
                        .unwrap_or_else(|| "Unknown".to_string()),
                    album: playlist_album,
                    link: playlist_url,
                };
                Some(search_result)
            }
            _ => None,
        }
    } else {
        None
    }
}

pub(crate) async fn load_msg(ctx: CommandContext, search_type: rspotify::model::SearchType) {
    // If args are empty, there is nothing to search.
    if !ctx.args().is_empty() {
        // Check for Spotify response
        if let Some(result) = search(ctx.args.rest().to_string(), search_type).await {
            // Get link
            if let Some(link) = result.link {
                // If the search started from a guild, add the play button.
                if let Some(guild) = ctx.guild() {
                    // Spawn a new thread to wait for the button interaction
                    tokio::spawn(async move {
                        // Send the message with the play button
                        let mut m = ctx
                            .channel_id
                            .send_message(&ctx.ctx.http, |m| {
                                m.content(link.clone());
                                m.components(|c| {
                                    c.create_action_row(|a| {
                                        a.create_button(|b| {
                                            b.style(ButtonStyle::Primary)
                                                .label("Play now!")
                                                .custom_id("play")
                                        })
                                    })
                                });
                                m
                            })
                            .await
                            .unwrap();
                        // Wait 10 min for interaction
                        if let Some(mci) = m
                            .await_component_interaction(&ctx.clone())
                            .timeout(Duration::from_secs(60 * 10))
                            .await
                        {
                            // Interaction happened, handle the interaction
                            let guild_id = guild.id;
                            let manager = songbird::get(&ctx.ctx)
                                .await
                                .expect("Songbird Voice client placed in at initialisation.")
                                .clone();
                            // Disable the play button
                            disable_button(ctx.ctx.clone(), mci, link.clone()).await;
                            // If there is no voice chat connection, try to join.
                            if manager.get(guild_id).is_none() {
                                join_to_play::join_to_play(&ctx).await;
                            }
                            // If there is a voice chat connection, play the searched link.
                            if let Some(handler_lock) = manager.get(guild_id) {
                                // A track has been searched
                                if search_type == rspotify::model::SearchType::Track {
                                    let id = link.clone().split('?').collect::<Vec<&str>>()[0]
                                        .replace("https://open.spotify.com/track/", "")
                                        .trim()
                                        .to_string();
                                    // Get track data from spotify
                                    let result = spotify::get_track(id).await.unwrap();
                                    let query = result.artist + "-" + result.name.as_str();
                                    // Send loading message
                                    msg_send!(&ctx, "Loading...");
                                    // Play the track
                                    if let Some(track) =
                                        helper_player::youtube_info(query.replace('-', " ")).await
                                    {
                                        ctx.storage()
                                            .await
                                            .update(|mut write| {
                                                write
                                                    .kv_mut()
                                                    .entry(*guild_id.as_u64())
                                                    .or_default()
                                                    .queue
                                                    .push_back(track.clone());
                                            })
                                            .await
                                            .expect("Failed to write to shared queue");
                                    }
                                    // Store the queue in storage
                                    store_playing(&ctx.ctx, ctx.guild_id().unwrap(), handler_lock)
                                        .await;
                                    // Send message with buttons
                                    msg_send!(
                                        ctx,
                                        "Added `".to_string() + query.as_str() + "` to queue.",
                                    );
                                    now_playing::now_playing(&ctx.ctx, guild_id, ctx.channel_id)
                                        .await;
                                // A playlist has been searched
                                } else if search_type == rspotify::model::SearchType::Playlist {
                                    let id = link.split('?').collect::<Vec<&str>>()[0]
                                        .replace("https://open.spotify.com/playlist/", "")
                                        .trim()
                                        .to_string();
                                    play_spotify_playlist(id, &ctx).await;
                                    // Store the queue in storage
                                    store_playing(&ctx.ctx, ctx.guild_id().unwrap(), handler_lock)
                                        .await;
                                    // Send message with buttons
                                    now_playing::now_playing(&ctx.ctx, guild_id, ctx.channel_id)
                                        .await;
                                }
                            }
                        // There was no interaction after 30 min
                        } else {
                            // Edit the message to disable the play button
                            if let Err(e) = m
                                .edit(ctx.ctx, |m| {
                                    m.content(link).components(|c| {
                                        c.create_action_row(|a| {
                                            a.create_button(|b| {
                                                b.style(ButtonStyle::Primary)
                                                    .label("Play now!")
                                                    .custom_id("play")
                                                    .disabled(true)
                                            })
                                        })
                                    })
                                })
                                .await
                            {
                                println!("{e}")
                            }
                        };
                    });
                // If it was a direct message, just send the link.
                } else {
                    msg_send!(ctx, link)
                }
            // No link was found on spotify
            } else {
                msg_send!(ctx, "Spotify found something but no link :thinking:")
            }
        // Spotify did not response
        } else {
            msg_send!(ctx, "Spotify did not return any results :sob:")
        }
    // No args given, nothing to search
    } else {
        msg_send!(
            ctx,
            "Please give me terms to search on Spotify, for example: `+spotify_track [song name]`."
                .to_string()
        )
    }
}

pub async fn disable_button(ctx: Context, mci: Arc<MessageComponentInteraction>, link: String) {
    mci.create_interaction_response(&ctx, |r| {
        r.kind(InteractionResponseType::UpdateMessage);
        r.interaction_response_data(|d| {
            d.content(link);
            d.components(|c| {
                c.create_action_row(|a| {
                    a.create_button(|b| {
                        b.style(ButtonStyle::Primary)
                            .label("Play now!")
                            .custom_id("play")
                            .disabled(true)
                    })
                })
            })
        })
    })
    .await
    .unwrap();
}
