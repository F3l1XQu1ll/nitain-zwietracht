use super::*;
use crate::{commands::CommandContext, msg_send, storage::crab_data::TrackEntry};
use serenity::client::Context;
use std::env;
use yt_api::{
    playlistitems::{Error, PlaylistItems, Response},
    ApiKey,
};

/// Prints the first answer of a search query
pub async fn playlist(url: &str, ctx: &CommandContext) -> Result<(), Error> {
    // Take YouTube API key from env
    let key = ApiKey::new(&env::var("YT_API_KEY").expect("YT_API_KEY env-var not found"));
    // Create the PlaylistItems struct for some playlist ID
    let list_id = url.split("?list=").collect::<Vec<&str>>()[1];
    let mut result = PlaylistItems::new(key.clone())
        .playlist_id(list_id)
        .max_results(50)
        .await?;
    // Send message for user feedback
    msg_send!(
        ctx,
        format!(
            "Adding {} songs to queue...",
            result.page_info.total_results
        ),
    );
    let guild_id = ctx.guild().unwrap().id;
    store_playlist(guild_id, ctx.ctx(), result.clone()).await;
    // If there are more than 50 songs in a playlist,
    // we have to get other result pages from YouTube.
    let mut next_page_token;
    while result.items.len() == 50 {
        // Get new result page token
        next_page_token = result
            .next_page_token
            .clone()
            .unwrap_or_else(|| "".to_string());
        // no more pages
        if next_page_token.is_empty() {
            break;
        } else {
            // Got token, getting new result page
            result = PlaylistItems::new(key.clone())
                .playlist_id(list_id)
                .page_token(next_page_token)
                .max_results(50)
                .await?;
            // Add songs to the queue
            store_playlist(guild_id, ctx.ctx(), result.clone()).await;
        }
    }
    Ok(())
}

async fn store_playlist(guild_id: GuildId, ctx: &Context, result: Response) {
    let urls = result
        .items
        .iter()
        .map(|item| TrackEntry {
            title: item.snippet.title.as_ref().unwrap().to_string(),
            artist: item.snippet.channel_title.as_ref().unwrap().to_string(),
            url: Some(format!(
                "https://youtube.com/watch?v={}",
                item.snippet.resource_id.video_id
            )),
            enqueued: false,
        })
        .collect::<Vec<TrackEntry>>();

    ctx.storage()
        .await
        .update(|mut write| {
            write
                .kv_mut()
                .entry(*guild_id.as_u64())
                .or_default()
                .queue
                .extend(urls.clone())
        })
        .await
        .expect("Could not write to shared queue");
}
