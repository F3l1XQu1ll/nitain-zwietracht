use crate::{msg_send, storage::manager::ContextStorageManager};
use serenity::{
    client::Context,
    model::id::{ChannelId, GuildId},
};

pub async fn toggle_loop(
    ctx: &Context,
    guild_id: GuildId,
    channel_id: ChannelId,
    option: Option<bool>,
) {
    // Get server data
    let mut storage_manager = ctx.storage().await;

    let mut server_data = {
        let read = storage_manager.read().await;
        read.kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
    };

    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    // Check if there is a voice chat connection.
    if let Some(handler_lock) = manager.get(guild_id) {
        let handler = handler_lock.lock().await;
        // Check if there is something playing.
        if handler.queue().current().is_none() {
            msg_send!(ctx, channel_id, "Nothing is playing right now.")
        } else {
            let queue = handler.queue();
            if !server_data.loop_state {
                // Check if there is an option given for the function
                match option {
                    // If the option is false, we want to disable the loop. But it already was disabled.
                    Some(false) => msg_send!(ctx, channel_id, "Loop is already disabled."),
                    // If there was no option given, then just switch the loop state.
                    _ => {
                        // Set the loop_state to true
                        storage_manager
                            .update(move |mut write| {
                                server_data.loop_state = true;
                                write
                                    .kv_mut()
                                    .insert(*guild_id.as_u64(), server_data.clone());
                            })
                            .await
                            .expect("Failed to commit guild loop_state");
                        // Enable the loop
                        if let Err(e) = queue.current().unwrap().enable_loop() {
                            println!("{e}")
                        }
                        // Send the message
                        msg_send!(ctx, channel_id, "Loop enabled.")
                    }
                }
                // If there was no option given, then just switch the loop state.
            } else if server_data.loop_state {
                // Check if there is an option given for the function
                match option {
                    // If the option is true, we want to enable the loop. But it already was enabled.
                    Some(true) => msg_send!(ctx, channel_id, "Loop is already enabled."),
                    // If there was no option given, then just switch the loop state.
                    _ => {
                        // Set the loop_state to false
                        storage_manager
                            .update(move |mut write| {
                                server_data.loop_state = false;
                                write
                                    .kv_mut()
                                    .insert(*guild_id.as_u64(), server_data.clone());
                            })
                            .await
                            .expect("Failed to commit guild loop_state");
                        // Disable the loop
                        if let Err(e) = queue.current().unwrap().disable_loop() {
                            println!("{e}")
                        }
                        // Send the message
                        msg_send!(ctx, channel_id, "Loop disabled.")
                    }
                }
            }
        }
    } else {
        msg_send!(ctx, channel_id, "Nothing is playing right now.")
    }
}
