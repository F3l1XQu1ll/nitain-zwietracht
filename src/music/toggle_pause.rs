use crate::{msg_send, storage::manager::ContextStorageManager};
use serenity::{
    client::Context,
    model::id::{ChannelId, GuildId},
};

/// Pause or unpause playback
///
/// If pause is None, the `paused_state` value in [`ServerStorage`](crate::storage::crab_data::ServerStorage) will be used
pub async fn toggle_pause(
    ctx: &Context,
    guild_id: GuildId,
    channel_id: ChannelId,
    pause: Option<bool>,
) {
    let mut server_data = {
        let storage_manager = ctx.storage().await;
        let read = storage_manager.read().await;
        read.kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
    };

    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    // Check if there is a voice chat connection.
    if let Some(handler_lock) = manager.get(guild_id) {
        let handler = handler_lock.lock().await;
        // Check if there is something playing.
        if handler.queue().current().is_none() {
            msg_send!(ctx, channel_id, "Nothing is playing right now.")
        } else {
            let queue = handler.queue();
            if pause.unwrap_or(false) || !server_data.paused_state {
                let mut storage_manager = ctx.storage().await;
                storage_manager
                    .update(|mut write| {
                        server_data.paused_state = true;
                        write
                            .kv_mut()
                            .insert(*guild_id.as_u64(), server_data.clone());
                    })
                    .await
                    .expect("Failed to update server data");

                if let Err(e) = queue.pause() {
                    error!("{e}")
                }
                msg_send!(ctx, channel_id, "Track paused.")
            } else {
                let mut storage_manager = ctx.storage().await;
                storage_manager
                    .update(|mut write| {
                        server_data.paused_state = false;
                        write
                            .kv_mut()
                            .insert(*guild_id.as_u64(), server_data.clone());
                    })
                    .await
                    .expect("Failed to commit guild paused_state");
                if let Err(e) = queue.resume() {
                    error!("{e}")
                }
                msg_send!(ctx, channel_id, "Track resumed.")
            }
        }
    } else {
        msg_send!(ctx, channel_id, "Nothing is playing right now.")
    }
}
