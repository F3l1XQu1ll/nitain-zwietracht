use super::*;
use crate::music::player::autoplay::autoplay_off;
use crate::{msg_send, storage::manager::ContextStorageManager, utils::do_fetch};
use serenity::{client::Context, model::id::GuildId};

async fn get_new_videos(link: String) -> Option<Vec<String>> {
    // Get the String from the YouTube page of the currently playing song
    if let Ok(response) = do_fetch(link.clone()).await {
        // Get the current video-ID
        let current_id = id_from_link(&link).await;
        // println!("{current_id}");
        let mut found_strings = Vec::new();
        // Search for video-IDs on the response
        let regex = regex::Regex::new("\"videoId\":\"([^\"]+)\",").unwrap();
        let captures = regex.captures_iter(response.as_str());
        // Collect each video-ID without duplicates
        for ids in captures {
            let id = ids.get(1).unwrap().as_str().to_string();
            if !found_strings.contains(&id) && id != current_id {
                found_strings.push(id)
            }
        }
        // println!("{:?}", found_strings);
        // Get the metadata of the current song
        let current_json = json::parse(
            do_fetch(format!("https://youtube.com/oembed?url={link}&format=json"))
                .await
                .unwrap()
                .as_str(),
        )
        .unwrap_or_else(|_| json::object! {});
        // Get the title of the song
        let current_title = current_json["title"].as_str().unwrap_or("");
        // println!("{current_title}");
        // Fix unavailable songs
        let mut fixed_found = Vec::new();
        for found_string in found_strings {
            // Get the metadata of the song
            let metadata_json = json::parse(
                do_fetch(format!("https://youtube.com/oembed?url=https://www.youtube.com/watch?v={found_string}&format=json"))
                    .await
                    .unwrap()
                    .as_str(),
            )
            .unwrap_or_else(|_| json::object! {});
            // Get the title of the song
            if let Some(title) = metadata_json["title"].as_str() {
                // Fix playing hour versions of one song
                if !current_title.to_lowercase().contains("hour")
                    && title.to_lowercase().contains("hour")
                {
                } else {
                    fixed_found.push(found_string);
                }
            }
        }
        // println!("{:?}", fixed_found);
        Some(fixed_found)
        // If no String from the YouTube page was found, return.
    } else {
        None
    }
}

async fn filter_found_ids(known_ids: Vec<String>, found_ids: Vec<String>) -> Option<String> {
    let mut found_id = String::new();
    for id in found_ids {
        // The new video-ID is found if the ID hasn't been played yet.
        if !known_ids.contains(&id) {
            found_id = id;
            break;
        }
    }
    // If no new video was found, return.
    if found_id.is_empty() {
        None
    // Insert the new video-ID into the autoplay_list and return.
    } else {
        Some(found_id)
    }
}

async fn id_from_link(link: &str) -> String {
    let id = link.split("?v=").last().unwrap().to_string();
    id
}

#[test]
fn test_get_new_videos() {
    let runtime = tokio::runtime::Runtime::new().unwrap();
    runtime.block_on(async {
        println!("Running in runtime ;)");
        let new_video_ids =
            get_new_videos("https://www.youtube.com/watch?v=sBugnmrF3G8".to_string()).await;
        assert!(new_video_ids.is_some());
        assert!(!new_video_ids.unwrap().is_empty());
    });
}

#[test]
fn test_filter_founds() {
    let runtime = tokio::runtime::Runtime::new().unwrap();
    runtime.block_on(async {
        let known = vec![String::from("foo"), String::from("bar")];
        let found = vec![String::from("bar"), String::from("Foo")];
        let filtered = filter_found_ids(known.clone(), found.clone()).await;
        assert!(filtered.is_some());
        assert!(filtered.unwrap() == *"Foo");
        assert!(filter_found_ids(known.clone(), known).await.is_none());
    })
}

/*
pub async fn output_autoplay(ctx: Context, msg: Message) {
    //let old_video_link = Arc::new(Mutex::new(String::new()));
    let guild_id = msg.guild(&ctx.cache).await.unwrap().id;
    //let action_queue = ctx.action_queue().await;
    //let mut write = action_queue.write().await;
    /*write
    .regiser_action(
        "output_autoplay".to_string(),
        100,
        false,
        Box::new(move || {
            Box::pin(autoplay_task(
                ctx.clone(),
                msg.clone(),
                guild_id,
                old_video_link.clone(),
            ))
        }),
    )
    .await;*/
    //});
    autoplay_task(ctx, guild_id).await;
}
*/

pub async fn autoplay_task(ctx: &Context, guild_id: GuildId) {
    // Get AutoplayState
    let autoplay_enabled = {
        let manager = ctx.storage().await;
        let read = manager.read().await;
        let guild_data = read
            .kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default();
        guild_data.autoplay_state
    };
    // If autoplay is enabled, search for new songs
    debug!("autoplay_enabled: {autoplay_enabled:#?}");
    if autoplay_enabled {
        let guild_data = {
            ctx.storage()
                .await
                .read()
                .await
                .kv()
                .get(guild_id.as_u64())
                .cloned()
                .unwrap_or_default()
        };
        let channel_id = guild_data.player_interaction.unwrap();
        /*let manager = songbird::get(&ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();*/

        // Get the queue
        // use shared queue to trigger autoplay as the songbird queue never contains more than one track
        let shared_queue = guild_data.queue;

        // Check if the shared queue is not empty
        if let Some(entry) = shared_queue.back() {
            debug!("shared queue not empty");
            let link = entry.url.as_ref().unwrap();
            // If there is more than one song queued, don't add a new song.
            debug!("shared queue length: {}", shared_queue.len());
            if shared_queue.len() == 1 {
                debug!("shared queue has only one song");
                let mut storage_manager = ctx.storage().await;

                // Get guild data from storage
                let mut guild_data = {
                    let read = storage_manager.read().await;
                    read.kv()
                        .get(guild_id.as_u64())
                        .cloned()
                        .unwrap_or_default()
                };

                if let Some(new_ids) = get_new_videos(link.clone()).await {
                    debug!("new_ids: {new_ids:#?}");
                    if let Some(new_video_id) =
                        filter_found_ids(guild_data.autoplay_list.clone(), new_ids).await
                    {
                        debug!("filter passed: {new_video_id:#?}");
                        if !guild_data.autoplay_list.contains(&link.clone()) {
                            guild_data.autoplay_list.push(id_from_link(link).await);
                        }
                        guild_data.autoplay_list.push(new_video_id.clone());

                        storage_manager
                            .update(move |mut write| {
                                write
                                    .kv_mut()
                                    .insert(*guild_id.as_u64(), guild_data.clone());
                            })
                            .await
                            .expect("Failed to update guild autoplay_list");

                        let url =
                            "https://www.youtube.com/watch?v=".to_string() + new_video_id.as_str();
                        // Insert the song into the queue
                        if let Some(track) = helper_player::youtube_info(url).await {
                            debug!("Got youtube info: {track:#?}");
                            storage_manager
                                .update(|mut write| {
                                    write
                                        .kv_mut()
                                        .entry(*guild_id.as_u64())
                                        .or_default()
                                        .queue
                                        .push_back(track.clone());
                                })
                                .await
                                .expect("Could not write to shared queue");
                        } else {
                            error!("Could not get youtube info new autoplay video url");
                        }
                    // If there was no new video-ID found, stop autoplay.
                    } else {
                        msg_send!(ctx, channel_id, "Could not find new videos.");
                        autoplay_off(ctx, guild_id, channel_id).await;
                    }
                }
            } else {
                debug!("autoplay_task won't do anything because queue_length is not 1")
            }
        } else {
            // If the queue is empty, stop autoplay.
            msg_send!(ctx, channel_id, "Nothing is playing right now.");
            autoplay_off(ctx, guild_id, channel_id).await;
        }
    }
}
