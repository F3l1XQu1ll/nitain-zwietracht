use super::*;
use crate::{
    action_queue::{ActionContinue, CrabAction},
    commands::CommandContext,
    message_box::{CrabMessage, FromHex},
    msg_send,
    storage::{crab_data::ServerStorage, manager::ContextStorageManager},
};
use rand::thread_rng;
use serenity::{
    builder::{CreateActionRow, CreateButton},
    client::Context,
    model::{
        channel::ReactionType,
        id::{ChannelId, GuildId},
        prelude::{
            component::ButtonStyle,
            interaction::{
                message_component::MessageComponentInteraction, InteractionResponseType,
            },
        },
    },
    utils::Colour,
};
use songbird::{Event, EventContext};
use std::{
    collections::VecDeque,
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    str::FromStr,
    sync::Arc,
    time::Duration,
};
use tokio::time::sleep;

pub struct QueueData {
    strings: String,
    page: usize,
    len: usize,
    max: usize,
}

pub async fn output_queue(ctx1: &CommandContext) {
    // Get new queue data
    if let Some(queue_data) = new_queue(ctx1).await {
        let ctx = ctx1.clone();
        tokio::spawn(async move {
            // Delete old queue messages in this channel
            {
                let manager = ctx.storage().await;
                let read = manager.read().await;
                if let Some(last_known_message_id) =
                    read.config().queue_message().get(ctx.channel_id().as_u64())
                {
                    // Try to delete the old buttons
                    // If it fails, do not panic.
                    if let Ok(()) = ctx
                        .ctx()
                        .http
                        .delete_message(*ctx.channel_id().as_u64(), *last_known_message_id)
                        .await
                    {}
                }
            }
            // Send new queue message
            let m = CrabMessage::from_ctx(&ctx)
                .title("Current queue:")
                .description(queue_data.strings.clone())
                .footer_text(format!(
                    "Page {}/{} | {} songs in queue.",
                    queue_data.page, queue_data.max, queue_data.len
                ))
                .reference(ctx.ref_msg())
                .action_row(QueueButtons::action_row())
                .message()
                .await
                .unwrap()
                .message;
            // Register message id
            {
                let mut manager = ctx.storage().await;
                manager
                    .update(|mut write| {
                        write
                            .config_mut()
                            .queue_message_mut()
                            .insert(*ctx.channel_id().as_u64(), *m.id.as_u64());
                    })
                    .await
                    .expect("Failed to reger message id");
            }

            // Message has been created and registered.
            // Loop will now take over handling.
            // The message will only be sent once.
            // If the loop repeats, another interaction will be awaited.

            loop {
                // Wait for interaction
                let cib = m
                    .await_component_interaction(&ctx)
                    // Stop waiting after 3 min
                    .timeout(Duration::from_secs(3 * 60))
                    .await;
                // Interaction happened
                if let Some(mci) = cib {
                    let option = QueueButtons::from_str(&mci.data.custom_id).unwrap();
                    // Get page number
                    let page = {
                        let storage_manager = ctx.storage().await;
                        let read = storage_manager.read().await;
                        *read
                            .config()
                            .queue_page()
                            .get(ctx.channel_id().as_u64())
                            .unwrap()
                    };
                    // Handle the interaction
                    match option {
                        QueueButtons::First => {
                            update_page(&ctx, 1).await;
                        }
                        QueueButtons::Prev => {
                            update_page(&ctx, page.saturating_sub(1)).await;
                        }
                        QueueButtons::Next => {
                            update_page(&ctx, page.saturating_add(1)).await;
                        }
                        QueueButtons::Last => {
                            update_page(&ctx, usize::MAX).await;
                        }
                        QueueButtons::Stop => {
                            m.delete(&ctx.ctx()).await.unwrap();
                            player::stop(&ctx).await;
                            break;
                        }
                    }
                    // Get new queue data
                    if let Some(queue_data) = new_queue(&ctx).await {
                        update_message(queue_data, ctx.ctx(), mci).await;
                    } else {
                        m.delete(&ctx.ctx()).await.unwrap();
                        msg_send!(ctx, "Nothing is playing right now.");
                        break;
                    }
                }
                // Interaction has been handled, or there was no interaction in 3 minutes.
                let last_queue_msg_id = {
                    let storage_manager = ctx.storage().await;
                    let read = storage_manager.read().await;
                    *read
                        .config()
                        .queue_message()
                        .get(ctx.channel_id().as_u64())
                        .unwrap()
                };
                // Check if last page for this channel does not equal the last page we have sent from
                // this thread. In this case an other thread has been spawned.
                if &last_queue_msg_id != m.id.as_u64() {
                    // Break loop and let the task die
                    break;
                }
                // If we did not break, wait again at next iteration
            }
        });
    } else {
        msg_send!(ctx1, "Nothing is playing right now.")
    }
}

pub async fn new_queue(ctx: &CommandContext) -> Option<QueueData> {
    // Check if there is something playing
    let guild_id = ctx.guild().unwrap().id;
    let manager = songbird::get(ctx.ctx())
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();
    if manager.get(guild_id).is_some() {
        //let handler = handler_lock.lock().await;
        //let queue = handler.queue();
        //let queue_list = queue.current_queue();
        let queue_list = ctx
            .storage()
            .await
            .read()
            .await
            .kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
            .queue;
        //dbg!(queue_list.clone());
        if queue_list.is_empty() {
            None
        } else {
            // Set maximum tracks on one page
            let page_size = 10;
            // Get page number
            let page = {
                let storage_manager = ctx.storage().await;
                let read = storage_manager.read().await;
                *read
                    .config()
                    .queue_page()
                    .get(ctx.channel_id().as_u64())
                    .unwrap()
            };
            let len = queue_list.len();
            // Set maximum page number
            let mut max = len / page_size;
            if len % page_size != 0 {
                max += 1
            }
            let mut strings = String::new();
            // Split the queue into chunks for each page
            let queue_vec = queue_list.tracks_vec();
            let mut chunks = queue_vec.chunks(page_size);
            if let Some(chunk) = chunks.nth(page.saturating_sub(1)) {
                // Set offset for each page
                let mut num = page.saturating_sub(1) * page_size;
                // Get each track and push an embed field to the vec
                for item in chunk {
                    num += 1;
                    strings.push_str(
                        ("**".to_string()
                            + num.to_string().as_str()
                            + ":** "
                            + "`"
                            + &item.title.replace('`', "'")
                            + "`\n\n")
                            .as_str(),
                    );
                    // After page_size iterations, stop pushing to the vec
                    if num == num + page_size {
                        break;
                    }
                }
            }
            Some(QueueData {
                strings,
                page,
                len,
                max,
            })
        }
    } else {
        None
    }
}

pub async fn update_page(ctx: &CommandContext, mut page: usize) {
    // Check if there is something in the queue
    let guild_id = ctx.guild().unwrap().id;
    /*
    let handler = handler_lock.lock().await;
    let queue = handler.queue();
    let queue_list = queue.current_queue();
    */
    let queue_list = ctx
        .storage()
        .await
        .read()
        .await
        .kv()
        .get(guild_id.as_u64())
        .cloned()
        .unwrap_or_default()
        .queue;
    if !queue_list.is_empty() {
        // Set maximum tracks on one page
        let page_size = 10;
        let len = queue_list.len();
        // Set maximum page number
        let mut max = len / page_size;
        if len % page_size != 0 {
            max += 1
        }
        // A page number under 1 and above the maximum is impossible.
        if page < 1 {
            page = 1
        }
        if page > max {
            page = max
        }
        // Update the page number after corrections above
        let mut storage_manager = ctx.storage().await;
        storage_manager
            .update(|mut write| {
                write
                    .config_mut()
                    .queue_page_mut()
                    .insert(*ctx.channel_id().as_u64(), page);
            })
            .await
            .expect("Faild to update guild queue");
    }
}

pub async fn update_message(
    queue_data: QueueData,
    ctx: &Context,
    mci: Arc<MessageComponentInteraction>,
) {
    // Edit the message
    mci.create_interaction_response(&ctx, |r| {
        r.kind(InteractionResponseType::UpdateMessage);
        r.interaction_response_data(|d| {
            d.embed(|e| {
                e.title("Current queue:");
                e.description(queue_data.strings.clone());
                e.color(Colour::from_hex("40FF40"));
                e.footer(|f| {
                    f.text(format!(
                        "Page {}/{} | {} songs in queue.",
                        queue_data.page, queue_data.max, queue_data.len
                    ));
                    f
                });
                e
            });
            d.components(|c| c.add_action_row(QueueButtons::action_row()))
        })
    })
    .await
    .unwrap();
}

pub trait LenAndSwap {
    fn len(&self) -> usize;
    fn swap(&mut self, i: usize, j: usize);
}

// An exact copy of rand::Rng::shuffle, with the signature modified to
// accept any type that implements LenAndSwap
pub fn shuffle<T, R>(values: &mut T, mut rng: R)
where
    T: LenAndSwap,
    R: rand::Rng,
{
    let mut i = values.len();
    while i >= 2 {
        // invariant: elements with index >= i have been locked in place.
        i -= 1;
        // lock element i in place.
        values.swap(i, rng.gen_range(1..i + 1));
    }
}

// VecDeque trivially fulfills the LenAndSwap requirement, but
// we have to spell it out.
impl<T> LenAndSwap for VecDeque<T> {
    fn len(&self) -> usize {
        self.len()
    }
    fn swap(&mut self, i: usize, j: usize) {
        self.swap(i, j)
    }
}

pub async fn shuffle_queue(ctx: &Context, guild_id: GuildId, channel_id: ChannelId) {
    let manager = songbird::get(ctx)
        .await
        .expect("Songbird Voice client placed in at initialisation.")
        .clone();

    if let Some(call) = manager.get(guild_id) {
        let mut storage_manager = ctx.storage().await;
        let mut queue = storage_manager
            .read()
            .await
            .kv()
            .get(guild_id.as_u64())
            .cloned()
            .unwrap_or_default()
            .queue;

        shuffle(&mut queue, thread_rng());

        storage_manager
            .update(|mut write| {
                write.kv_mut().entry(*guild_id.as_u64()).or_default().queue = queue.clone();
            })
            .await
            .expect("Could not shuffle queue");

        store_playing(ctx, guild_id, call).await;

        msg_send!(ctx, channel_id, "Queue shuffled!")
    } else {
        msg_send!(ctx, channel_id, "Nothing to shuffle.")
    }
}

#[derive(Debug)]
struct ParseComponentError(String);

impl Display for ParseComponentError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "Failed to parse {} as component", self.0)
    }
}

impl StdError for ParseComponentError {}

#[derive(Debug)]
enum QueueButtons {
    First,
    Prev,
    Stop,
    Next,
    Last,
}

impl Display for QueueButtons {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            QueueButtons::First => write!(f, "first"),
            QueueButtons::Prev => write!(f, "prev"),
            QueueButtons::Stop => write!(f, "stop"),
            QueueButtons::Next => write!(f, "next"),
            QueueButtons::Last => write!(f, "last"),
        }
    }
}

impl QueueButtons {
    fn emoji(&self) -> &str {
        match self {
            QueueButtons::First => "⏪",
            QueueButtons::Prev => "◀️",
            QueueButtons::Stop => "⏹️",
            QueueButtons::Next => "▶️",
            QueueButtons::Last => "⏩",
        }
    }

    fn button(&self) -> CreateButton {
        let mut b = CreateButton::default();
        b.custom_id(self.to_string());
        b.emoji(ReactionType::Unicode(self.emoji().to_string()));
        b.style(ButtonStyle::Secondary);
        b
    }

    fn action_row() -> CreateActionRow {
        let mut ar = CreateActionRow::default();
        // We can add up to 5 buttons per action row
        ar.add_button(QueueButtons::First.button());
        ar.add_button(QueueButtons::Prev.button());
        ar.add_button(QueueButtons::Stop.button());
        ar.add_button(QueueButtons::Next.button());
        ar.add_button(QueueButtons::Last.button());
        ar
    }
}

impl FromStr for QueueButtons {
    type Err = ParseComponentError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "first" => Ok(QueueButtons::First),
            "prev" => Ok(QueueButtons::Prev),
            "stop" => Ok(QueueButtons::Stop),
            "next" => Ok(QueueButtons::Next),
            "last" => Ok(QueueButtons::Last),
            _ => Err(ParseComponentError(s.to_string())),
        }
    }
}

pub struct QueueEnqueueNewAction {
    pub ctx: Context,
    pub guild_id: GuildId,
}

#[async_trait]
impl CrabAction for QueueEnqueueNewAction {
    async fn action(&mut self) -> ActionContinue {
        let ctx = &self.ctx;
        let guild_id = self.guild_id;
        if let Some(call) = songbird::get(ctx).await.unwrap().get(guild_id) {
            let songbird_queue = {
                let call = call.lock().await;
                call.queue().clone()
            };

            // Only modify queues if songbird wants a new song to play
            if songbird_queue.is_empty() {
                let mut storage = ctx.storage().await;
                let guild_data = {
                    storage
                        .read()
                        .await
                        .kv()
                        .get(guild_id.as_u64())
                        .cloned()
                        .unwrap_or_default()
                };
                // make temp copy of shared queue
                let queue = guild_data.queue.clone();
                let mut shared_queue = guild_data.queue.clone();
                // iterate queue entries (we only need either the first or second)
                while let Some(track) = shared_queue.front_mut() {
                    debug!("{track:#?}");
                    // remove entry if it was playing
                    if track.enqueued && !guild_data.first_launch {
                        shared_queue.next();
                        debug!("Removed previous enqueued track from shared queue");
                        // store queue
                        storage
                            .update(|mut write| {
                                let guild_data =
                                    write.kv_mut().entry(*guild_id.as_u64()).or_default();
                                guild_data.queue = shared_queue.clone();
                            })
                            .await
                            .expect("Failed to write to shared queue");
                        continue;
                    } else {
                        debug!("{track:#?}");
                        // enqueue the track entry and retrieve the result
                        let player_result = if let Some(url) = &track.url {
                            helper_player::play_title(url.clone(), call.clone()).await
                        } else {
                            helper_player::play_title(
                                track.title.clone() + track.artist.as_str(),
                                call.clone(),
                            )
                            .await
                        };

                        // check if enqueuing was successfull - it *should* be
                        if let Err(e) = player_result {
                            error!("{e}");
                        } else {
                            // Set the current entry as playing
                            track.enqueued = true;
                            // store queue
                            storage
                                .update(|mut write| {
                                    let guild_data =
                                        write.kv_mut().entry(*guild_id.as_u64()).or_default();
                                    guild_data.queue = shared_queue.clone();
                                    guild_data.first_launch = false;
                                })
                                .await
                                .expect("Failed to write to shared queue");
                            debug!("New track enqueued in songbird_queue");

                            if guild_data.first_launch {
                                Self::post_enqueue_operations(guild_data.clone(), call.clone())
                                    .await;
                            }
                        }
                        break;
                    }
                }
                if guild_data.paused_state {
                    let _ = call.lock().await.queue().pause();
                }
                if guild_data.loop_state {
                    if let Some(track) = call.lock().await.queue().current() {
                        let _ = track.enable_loop();
                    }
                }
                if !queue.is_empty() {
                    debug!("Shared queue was not empty, running autoplay_task");
                    let ctx1 = ctx.clone();
                    tokio::spawn(async move {
                        // perform autoplay task
                        autoplay::autoplay_task(&ctx1, guild_id).await;
                    });
                }
            }
            ActionContinue(true)
        } else {
            debug!("Gould not get handler_lock for this guild: {guild_id:#?}");
            ActionContinue(false)
        }
    }
    fn name(&self) -> String {
        "QueueEnqueueNewAction".to_string() + &self.guild_id.as_u64().to_string()
    }
}

impl QueueEnqueueNewAction {
    /// Apply old track position if existing, apply [`paused_state`](crate::storage::crab_data::ServerStorage) and [`loop_state`](crate::storage::crab_data::ServerStorage)
    async fn post_enqueue_operations(guild_data: ServerStorage, call: Arc<Mutex<Call>>) {
        // Wait for songbird to catch up
        while call.lock().await.queue().current().is_none() {
            sleep(Duration::from_millis(100)).await;
        }
        // Seek track if we know the previous position before the bot went down
        if let Some(position) = guild_data.track_position {
            debug!("found position: {position:#?}");
            /*if let Err(e) = handler_lock
                .lock()
                .await
                .queue()
                .current()
                .unwrap()
                .seek_time(position)
            {
                error!("Failed to seek to position {position:#?} after bot rebooted: {e:#?}");
            } else {
                debug!("Seeked to position {position:#?}");
            }*/
            // FIXME: Seeking takes ages and does not work all the time, so we disable it for now.
            // FIXME: Dirty hack to make seeking work a bit more properly, due to a bug in songbird: https://github.com/serenity-rs/songbird/issues/97
            /*
            call.lock()
            .await
            .queue()
            .current()
            .unwrap()
            .add_event(
                Event::Delayed(Duration::from_secs(2)),
                Seeker { seek_to: position },
            )
            .expect("Failed to register startup seek event");
            */
        }
        // Pause track if it was paused before the bot went down
        if guild_data.paused_state {
            if let Err(e) = call.lock().await.queue().pause() {
                error!("Failed to pause after bot rebooted: {e:#?}");
            }
        }
        // enable loop for track loop was enabled before the bot went down
        if guild_data.loop_state {
            if let Err(e) = call.lock().await.queue().current().unwrap().enable_loop() {
                error!("Failed to enable loop after bot rebooted: {e:#?}");
            }
        }
    }
}

/// Assistance struct for seeking
struct Seeker {
    seek_to: Duration,
}

#[async_trait]
impl songbird::EventHandler for Seeker {
    async fn act(&self, ctx: &EventContext<'_>) -> Option<Event> {
        if let EventContext::Track(&[(_, track)]) = ctx {
            track.seek_time(self.seek_to).unwrap();
            debug!("Seek to {:?}", self.seek_to);
        }
        None
    }
}
