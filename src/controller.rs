use std::{sync::Arc, time::Duration};

use crab_model::player::{AutoplayState, LoopState};
use serenity::model::prelude::GuildId;
use songbird::Songbird;
use tokio::sync::RwLock;

use crate::{error::LogError, storage::crab_data::CrabData};

pub struct CrabController {
    data: Arc<RwLock<CrabData>>,
    songbird: Arc<Songbird>,
}

impl CrabController {
    pub fn new(data: Arc<RwLock<CrabData>>, songbird: Arc<Songbird>) -> Self {
        Self { data, songbird }
    }

    pub async fn player_info(&self, guild_id: GuildId) -> crab_model::player::State {
        let connected: bool;
        if let Some(call) = self.songbird.get(guild_id) {
            connected = true;
            // Songbird is a bit slow at enqueuing tracks
            let current_track = loop {
                let queue_empty = self
                    .data
                    .read()
                    .await
                    .kv()
                    .get(guild_id.as_u64())
                    .map(|guild| guild.queue.is_empty())
                    .unwrap_or(true);
                let lock = call.lock().await;
                let current_track = lock.queue().current();
                if current_track.is_some() || queue_empty {
                    break current_track;
                } else {
                    tokio::time::sleep(Duration::from_millis(10)).await;
                }
            };
            let lock = call.lock().await;
            let current_channel = lock.current_channel();
            let current_track_metadata =
                current_track.clone().map(|track| track.metadata().clone());
            let current_track_info = match current_track {
                Some(track) => track.get_info().await.ok(),
                None => None,
            };
            let position = current_track_info.map(|info| info.position);
            let server_data = self
                .data
                .read()
                .await
                .kv()
                .get(guild_id.as_u64())
                .cloned()
                .unwrap_or_default();
            let loop_state = server_data.loop_state;
            let paused = server_data.paused_state;
            let autoplay = server_data.autoplay_state;
            let title = current_track_metadata.clone().and_then(|meta| meta.title);
            let artist = current_track_metadata.clone().and_then(|meta| meta.artist);
            let duration = current_track_metadata.and_then(|meta| meta.duration);
            crab_model::player::State {
                connected,
                channel: current_channel.map(|c| c.0),
                position,
                loop_state: if loop_state {
                    LoopState::Enabled
                } else {
                    LoopState::Disabled
                },
                paused,
                autoplay_state: if autoplay {
                    AutoplayState::Enabled
                } else {
                    AutoplayState::Disabled
                },
                title,
                artist,
                duration,
            }
        } else {
            connected = false;
            crab_model::player::State {
                connected,
                ..Default::default()
            }
        }
    }

    pub async fn player_interaction(
        &self,
        guild_id: GuildId,
        interaction: crab_model::player::Interaction,
    ) {
        if let Some(call) = self.songbird.get(guild_id) {
            let lock = call.lock().await;

            match interaction {
                crab_model::player::Interaction::Resume => {
                    if lock.queue().resume().is_ok() {
                        if let Some(guild) =
                            self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                        {
                            guild.paused_state = false
                        }
                    }
                }
                crab_model::player::Interaction::Pause => {
                    if lock.queue().pause().is_ok() {
                        if let Some(guild) =
                            self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                        {
                            guild.paused_state = true
                        }
                    }
                }
                crab_model::player::Interaction::Back => {
                    if let Some(track) = lock.queue().current() {
                        let is_long_playing = track
                            .get_info()
                            .await
                            .map(|info| info.position > Duration::from_secs(5))
                            .unwrap_or(false);
                        if is_long_playing {
                            track.seek_time(Duration::from_secs(0)).handle();
                        } else if self
                            .data
                            .write()
                            .await
                            .kv_mut()
                            .get_mut(guild_id.as_u64())
                            .map(|guild| guild.queue.prev())
                            .is_some()
                        {
                            lock.queue().skip().handle();
                        }
                    }
                }
                crab_model::player::Interaction::Next => {
                    if self
                        .data
                        .write()
                        .await
                        .kv_mut()
                        .get_mut(guild_id.as_u64())
                        .map(|guild| guild.queue.next())
                        .is_some()
                    {
                        lock.queue().skip().handle();
                    }
                }
                crab_model::player::Interaction::Autoplay => {
                    if let Some(guild) = self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                    {
                        guild.autoplay_state = true
                    }
                }
                crab_model::player::Interaction::AutoOff => {
                    if let Some(guild) = self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                    {
                        guild.autoplay_state = false
                    }
                }
                crab_model::player::Interaction::Loop => {
                    if let Some(track) = lock.queue().current() {
                        if track.enable_loop().is_ok() {
                            if let Some(guild) =
                                self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                            {
                                guild.loop_state = true
                            }
                        }
                    }
                }
                crab_model::player::Interaction::Unloop => {
                    if let Some(track) = lock.queue().current() {
                        if track.disable_loop().is_ok() {
                            if let Some(guild) =
                                self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                            {
                                guild.loop_state = false
                            }
                        }
                    }
                }
                crab_model::player::Interaction::Remove(id) => {
                    if let Some(guild) = self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                    {
                        guild.queue.retain(|track| track.id() != id)
                    }
                }
                crab_model::player::Interaction::Swap(id, other) => {
                    if let Some(guild) = self.data.write().await.kv_mut().get_mut(guild_id.as_u64())
                    {
                        guild.queue.swap_by_ids(id, other).handle()
                    }
                }
            }
        }
    }

    pub async fn queue_info(&self, guild_id: GuildId) -> crab_model::player::QueueState {
        let queue = {
            self.data
                .read()
                .await
                .kv()
                .get(guild_id.as_u64())
                .map(|guild| guild.queue.clone())
                .unwrap_or_default()
        };
        let tracks = queue.tracks_vec_unique();
        let played_tracks = queue.played_tracks();
        let enqueued = tracks
            .iter()
            .map(|track| crab_model::player::QueueEntry {
                title: track.title.clone(),
                id: track.id(),
            })
            .collect::<Vec<_>>();
        let played = played_tracks
            .iter()
            .map(|track| crab_model::player::QueueEntry {
                title: track.title.clone(),
                id: track.id(),
            })
            .collect::<Vec<_>>();
        crab_model::player::QueueState { played, enqueued }
    }
}
