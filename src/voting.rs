use crate::{
    commands::CommandContext,
    error::LogError,
    message_box::{CrabMessage, FromHex},
    msg_send,
};
use anyhow::{Context, Result};
use serenity::{
    builder::{CreateActionRow, CreateButton},
    futures::StreamExt,
    model::{
        channel::ReactionType,
        prelude::{component::ButtonStyle, interaction::InteractionResponseType, User},
    },
    utils::Colour,
};
use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    str::FromStr,
    time::Duration,
};

#[derive(Debug)]
struct ParseComponentError(String);

impl Display for ParseComponentError {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        write!(f, "Failed to parse {} as component", self.0)
    }
}

impl StdError for ParseComponentError {}

#[derive(Debug)]
enum VoteButtons {
    Yes,
    Maybe,
    No,
    Finished,
}

impl Display for VoteButtons {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Self::Yes => write!(f, "yes"),
            Self::Maybe => write!(f, "maybe"),
            Self::No => write!(f, "no"),
            Self::Finished => write!(f, "finished"),
        }
    }
}

impl VoteButtons {
    fn emoji(&self) -> &str {
        match self {
            Self::Yes => "✅",
            Self::Maybe => "💭",
            Self::No => "❌",
            Self::Finished => "🏁",
        }
    }

    fn button(&self) -> CreateButton {
        let mut b = CreateButton::default();
        b.custom_id(self.to_string());
        b.emoji(ReactionType::Unicode(self.emoji().to_string()));
        b.style(ButtonStyle::Secondary);
        b
    }

    fn action_row() -> CreateActionRow {
        let mut ar = CreateActionRow::default();
        // We can add up to 5 buttons per action row
        ar.add_button(VoteButtons::Yes.button());
        ar.add_button(VoteButtons::Maybe.button());
        ar.add_button(VoteButtons::No.button());
        ar.add_button(VoteButtons::Finished.button());
        ar
    }
}

impl FromStr for VoteButtons {
    type Err = ParseComponentError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "yes" => Ok(VoteButtons::Yes),
            "maybe" => Ok(VoteButtons::Maybe),
            "no" => Ok(VoteButtons::No),
            "finished" => Ok(VoteButtons::Finished),
            _ => Err(ParseComponentError(s.to_string())),
        }
    }
}

enum VoteType {
    Yes,
    Maybe,
    No,
}

struct VoteData {
    yes_votes: Vec<User>,
    maybe_votes: Vec<User>,
    no_votes: Vec<User>,
}

impl VoteData {
    fn new() -> Self {
        Self {
            yes_votes: Vec::new(),
            maybe_votes: Vec::new(),
            no_votes: Vec::new(),
        }
    }

    /// Move user from one list to the chosen one
    fn update(&mut self, user: User, option: VoteButtons) {
        // Handle the chosen option
        match option {
            // Add user to yes list, remove from no and maybe lists
            VoteButtons::Yes => {
                self.remove_vote(VoteType::Maybe, &user);
                self.remove_vote(VoteType::No, &user);
                if !self.yes_votes.contains(&user) {
                    self.yes_votes.push(user);
                }
            }
            // Add to maybe, remove from yes and no
            VoteButtons::Maybe => {
                self.remove_vote(VoteType::Yes, &user);
                self.remove_vote(VoteType::No, &user);
                if !self.maybe_votes.contains(&user) {
                    self.maybe_votes.push(user);
                }
            }
            // Add to no, remove from yes and maybe
            VoteButtons::No => {
                self.remove_vote(VoteType::Yes, &user);
                self.remove_vote(VoteType::Maybe, &user);
                if !self.no_votes.contains(&user) {
                    self.no_votes.push(user)
                }
            }
            _ => {}
        }
    }

    /// Remove a vote from a list
    fn remove_vote(&mut self, vote_type: VoteType, user: &User) {
        let votes = match vote_type {
            VoteType::Yes => &mut self.yes_votes,
            VoteType::Maybe => &mut self.maybe_votes,
            VoteType::No => &mut self.no_votes,
        };
        if let Some(pos) = votes.iter().position(|x| *x == *user) {
            votes.remove(pos);
        }
    }

    /// Build a string containing usernames separated by '\n'
    fn gen_user_name_list(&self, vote_type: VoteType) -> String {
        let votes = match vote_type {
            VoteType::Yes => &self.yes_votes,
            VoteType::Maybe => &self.maybe_votes,
            VoteType::No => &self.no_votes,
        };
        let mut name_list = String::new();
        if votes.is_empty() {
            name_list = "\u{200B}".to_string();
        } else {
            for name in votes {
                if votes.len() > 1 {
                    name_list.push('\n');
                }
                name_list.push_str(name.name.as_str());
            }
        }
        name_list
    }
}

pub async fn vote(ctx: &CommandContext) {
    let title = ctx.args().single::<String>().unwrap_or_default();
    if title.is_empty() {
        msg_send!(
            ctx,
            "Please try again and tell me what the vote is about `+vote [topic]`.",
        )
    } else {
        let ctx = ctx.clone();
        tokio::spawn(async move {
            // Send message for buttons if this task has just been launched
            let m = CrabMessage::from_ctx(&ctx).embed(|e| {
                        e.title(format!("__{title}__"))
                        .description("React on the buttons below to choose your option!\nClick on the checkered flag :checkered_flag: to end the voting, otherwise it will stay for 6 hours.")
                        .field("__Yes__", "\u{200B}", true)
                        .field("__Maybe__", "\u{200B}", true)
                        .field("__No__", "\u{200B}", true)
                        .color(Colour::from_hex("40FF40"))
                        .footer(|f| f.text("created by ".to_string() + ctx.user().name.as_str()))
                    }).action_row(VoteButtons::action_row()).reference(ctx.ref_msg()).message().await.expect("Failed to send message!").message;
            // Wait for interaction
            let mut cib = m
                .await_component_interactions(&ctx)
                // Stop waiting after 6h
                .timeout(Duration::from_secs(6 * 60 * 60))
                .build();
            // Handle interaction
            let mut vote_data = VoteData::new();
            while let Some(mci) = cib.next().await {
                let user = mci.user.clone();
                let option = VoteButtons::from_str(&mci.data.custom_id).unwrap();
                match option {
                    VoteButtons::Yes | VoteButtons::Maybe | VoteButtons::No => {
                        vote_data.update(user, option);
                    }
                    VoteButtons::Finished => break,
                }
                mci.create_interaction_response(&ctx, |r| {
                    r.kind(InteractionResponseType::UpdateMessage);
                    r.interaction_response_data(|d|  {
                    d.embed(|e| {
                        e.title(format!("__{title}__"))
                        .description("React on the buttons below to choose your option!\nClick on the checkered flag :checkered_flag: to end the voting, otherwise it will stay for 6 hours.")
                        .field("__Yes__",vote_data.gen_user_name_list(VoteType::Yes), true)
                        .field("__Maybe__", vote_data.gen_user_name_list(VoteType::Maybe), true)
                        .field("__No__", vote_data.gen_user_name_list(VoteType::No), true)
                        .color(Colour::from_hex("40FF40"))
                        .footer(|f| f.text("created by ".to_string() + ctx.user().name.as_str()))
                    });
                    d.components(|c| c.add_action_row(VoteButtons::action_row()))
                })
                })
                .await
                .unwrap();
            }
            m.delete(&ctx.ctx()).await.unwrap();
            CrabMessage::from_ctx(&ctx)
                .embed(|e| {
                    e.title(format!("__{title}__"))
                        .description("Vote ended! Here are the results:")
                        .field("__Yes__", vote_data.gen_user_name_list(VoteType::Yes), true)
                        .field(
                            "__Maybe__",
                            vote_data.gen_user_name_list(VoteType::Maybe),
                            true,
                        )
                        .field("__No__", vote_data.gen_user_name_list(VoteType::No), true)
                        .color(Colour::from_hex("40FF40"))
                        .footer(|f| f.text("created by ".to_string() + ctx.user().name.as_str()))
                })
                .message()
                .await
                .context("Failed to send vote results message!")
                .handle();
        });
    }
}
