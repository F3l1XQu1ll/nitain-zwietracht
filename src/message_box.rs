use futures::StreamExt;
use serenity::{
    builder::{CreateActionRow, CreateAllowedMentions, CreateEmbed, CreateMessage, EditMessage},
    client::bridge::gateway::ShardMessenger,
    http::Http,
    model::{
        channel::{Message, MessageReference},
        id::{ChannelId, MessageId},
        prelude::interaction::InteractionResponseType,
    },
    prelude::TypeMapKey,
    utils::Colour,
};
use std::{
    collections::{HashMap, VecDeque},
    convert::TryInto,
    iter::FromIterator,
    sync::Arc,
    time::Duration,
};

use crate::{
    commands::CommandContext,
    error::{CommandHandleError, LogError},
    utils::Ellipsize,
};

static PAGE_LEN_LIMIT: usize = 2048;

pub trait FromHex {
    fn from_hex(color: &str) -> Colour;
}

impl FromHex for Colour {
    fn from_hex(color: &str) -> Colour {
        let value_only = color.replace('#', "");
        let r_str = &value_only[0..2];
        let g_str = &value_only[2..4];
        let b_str = &value_only[4..6];

        let r_val = i32::from_str_radix(r_str, 16).unwrap_or_default();
        let g_val = i32::from_str_radix(g_str, 16).unwrap_or_default();
        let b_val = i32::from_str_radix(b_str, 16).unwrap_or_default();

        Colour::from_rgb(
            r_val.try_into().unwrap(),
            g_val.try_into().unwrap(),
            b_val.try_into().unwrap(),
        )
    }
}

#[test]
fn test_color_conversion() {
    let ffffff = Colour::from_hex("#FFFFFF");
    let black = Colour::from_hex("#000000");
    assert_eq!(ffffff.hex(), "FFFFFF");
    assert_eq!(black.hex(), "000000");
}

struct CommandHanderKey();
impl TypeMapKey for CommandHanderKey {
    type Value = Arc<CommandHandler>;
}

/// TODO: Prototype command handler
pub struct CommandHandler {
    channel: ChannelId,
    http: Arc<Http>,
    shard_messenger: ShardMessenger,
    reference: Option<MessageReference>,
}

impl CommandHandler {
    pub fn new(
        http: Arc<Http>,
        shard_messenger: ShardMessenger,
        channel: ChannelId,
        ref_msg: Option<impl AsReferenceMessage>,
    ) -> Self {
        Self {
            channel,
            http,
            shard_messenger,
            reference: ref_msg.map(|r| r.as_ref_msg()),
        }
    }

    pub async fn send_typing(&self) {
        self.channel.broadcast_typing(&self.http).await.unwrap();
    }

    pub async fn handle(&self, result: Result<(), CommandHandleError>) {
        // TODO: Handle errors
        if let Err(e) = &result {
            self.send_typing().await;
            match e {
                CommandHandleError::Internal(error) => {
                    CrabMessage::build(
                        self.shard_messenger.clone(),
                        self.http.clone(),
                        self.channel,
                    )
                    .reference(self.reference.clone())
                    .color_hex("FF4040")
                    .embed(|e| {
                        e.title("Whooops!");
                        e.description("An error occured while executing the command.");
                        e.field("Error", error.to_string(), true);
                        e.color(Colour::from_hex("FF4040"));
                        e
                    })
                    .message()
                    .await
                    .expect("Failed to send debug message!");
                }
                CommandHandleError::Custom { message } => {
                    CrabMessage::build(
                        self.shard_messenger.clone(),
                        self.http.clone(),
                        self.channel,
                    )
                    .reference(self.reference.clone())
                    .title(message)
                    .color_hex("FF4040")
                    .message()
                    .await
                    .expect("Failed to send hint");
                }
            }
        }
    }
}

pub async fn send_msg(
    ctx: CommandContext,
    title: &str,
    strings: Vec<(String, String, bool)>,
    image_url: &str,
    url: Option<String>,
) {
    let msg = CrabMessage::from_ctx(&ctx)
        .embed(|e| {
            e.title(title);
            e.thumbnail(image_url);
            e.fields(strings);
            if let Some(link) = url {
                e.url(link);
            }
            e.color(Colour::from_hex("40FF40"));
            e
        })
        .reference(ctx.ref_msg())
        .message()
        .await;
    if let Err(why) = msg {
        println!("Error sending message: {:?}", why);
    }
}

pub trait CrabCreateMessage {
    fn embed<F>(&mut self, f: F) -> &mut Self
    where
        F: FnOnce(&mut CreateEmbed) -> &mut CreateEmbed;
}

impl CrabCreateMessage for CreateMessage<'_> {
    fn embed<F>(&mut self, f: F) -> &mut Self
    where
        F: FnOnce(&mut CreateEmbed) -> &mut CreateEmbed,
    {
        CreateMessage::embed(self, f)
    }
}

impl CrabCreateMessage for EditMessage<'_> {
    fn embed<F>(&mut self, f: F) -> &mut Self
    where
        F: FnOnce(&mut CreateEmbed) -> &mut CreateEmbed,
    {
        EditMessage::embed(self, f)
    }
}

#[derive(Clone)]
pub struct CrabMessage {
    pub message: Message,
    pub channel: ChannelId,
    pager: Option<CrabMessageDescriptionPager>,
    embed_pager: Option<CrabMessagePager>,
}

#[derive(Clone)]
pub struct CrabMessagePage {
    /// The index to keep the pages in order
    index: usize,

    embed: Option<CreateEmbed>,

    title: Option<String>,
    description: Option<Vec<String>>,
    footer_text: Option<String>,
    fields: Option<CrabMessageFields>,
    image_link: Option<String>,
    link: Option<String>,
}

pub struct CrabMessageBuilder {
    shard_messenger: ShardMessenger,
    http: Arc<Http>,
    channel: ChannelId,
    reference: Option<MessageReference>,
    allowed_mentions: CreateAllowedMentions,
    color: Colour,
    action_row: Option<CreateActionRow>,

    pages: HashMap<String, CrabMessagePage>,
    page: String,
}
#[derive(Clone)]
pub struct CrabMessageDescriptionPager {
    pages: Vec<String>,
}

#[derive(Clone)]
pub struct CrabMessagePager {
    pages: HashMap<String, CrabMessagePage>,
}

/// Wrapper for embed fields
#[derive(Clone)]
pub struct CrabMessageField {
    title: String,
    description: String,
    inline: bool,
}

#[derive(Clone)]
pub struct CrabMessageFields(Vec<CrabMessageField>);

impl<T, U> From<(T, U, bool)> for CrabMessageField
where
    T: Into<String>,
    U: Into<String>,
{
    fn from((title, desc, inline): (T, U, bool)) -> Self {
        let title = title.into();
        let description = desc.into();
        Self {
            title,
            description,
            inline,
        }
    }
}

impl From<CrabMessageField> for (String, String, bool) {
    fn from(f: CrabMessageField) -> Self {
        let mut f = f;
        f.fix_empty();
        (f.title, f.description, f.inline)
    }
}

impl IntoIterator for CrabMessageFields {
    type Item = (String, String, bool);

    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0
            .into_iter()
            .map(|f| f.into())
            .collect::<Vec<_>>()
            .into_iter()
    }
}

impl<A> FromIterator<A> for CrabMessageFields
where
    A: Into<CrabMessageField>,
{
    fn from_iter<T: IntoIterator<Item = A>>(iter: T) -> Self {
        let mut vec: Vec<CrabMessageField> = Vec::new();
        for field in iter {
            vec.push(field.into());
        }
        Self(vec)
    }
}

impl<T, U> From<Vec<(T, U, bool)>> for CrabMessageFields
where
    T: Into<String>,
    U: Into<String>,
{
    fn from(v: Vec<(T, U, bool)>) -> Self {
        Self(v.into_iter().map(|f| f.into()).collect())
    }
}

impl CrabMessageField {
    /// Fix empty title or description fields
    pub fn fix_empty(&mut self) {
        Self::space_if_empty(&mut self.title);
        Self::space_if_empty(&mut self.description);
    }

    fn space_if_empty(input: &mut String) {
        if input.is_empty() {
            *input = "\u{200B}".to_string()
        }
    }
}

impl CrabMessage {
    pub fn build(
        shard_messenger: ShardMessenger,
        http: Arc<Http>,
        channel: ChannelId,
    ) -> CrabMessageBuilder {
        CrabMessageBuilder {
            shard_messenger,
            http,
            channel,
            reference: None,
            allowed_mentions: { CreateAllowedMentions::default().empty_parse().clone() },
            color: Colour::from_hex("40FF40"),
            action_row: None,
            pages: HashMap::from([(
                String::from("Default"),
                CrabMessagePage {
                    index: 0,
                    embed: None,
                    title: None,
                    description: None,
                    footer_text: None,
                    fields: None,
                    image_link: None,
                    link: None,
                },
            )]),
            page: String::from("Default"),
        }
    }

    pub fn from_ctx(ctx: &CommandContext) -> CrabMessageBuilder {
        Self::build(
            ctx.ctx().shard.clone(),
            ctx.ctx().http.clone(),
            ctx.channel_id(),
        )
    }

    pub async fn handle_page_requests(mut self, shard_messenger: ShardMessenger, http: Arc<Http>) {
        tokio::spawn(async move {
            let mut interactions = self
                .message
                .await_component_interactions(&shard_messenger)
                .timeout(Duration::from_secs(60 * 5))
                .build();
            while let Some(interaction) = interactions.next().await {
                if interaction.data.custom_id == "page-selector" {
                    let selected = interaction.data.values[0].clone();

                    let new_embed =
                        |create_embed: Option<CreateEmbed>,
                         pager: Option<CrabMessageDescriptionPager>| {
                            let mut old_embed = create_embed?;
                            let selected = selected.parse::<usize>().ok()?;
                            let pager = pager?;
                            let page = pager.pages.get(selected)?;
                            let new_embed = old_embed.description(page).clone();
                            Some(new_embed)
                        };

                    if let Some(new_embed) = new_embed(
                        self.message
                            .embeds
                            .get(0)
                            .map(|e| CreateEmbed::from(e.clone())),
                        self.pager.clone(),
                    ) {
                        if let Err(e) = self
                            .message
                            .edit(http.clone(), |m| {
                                m.embed(|e| {
                                    *e = new_embed.clone();
                                    e
                                });
                                m
                            })
                            .await
                        {
                            error!("{e:#?}");
                        }
                    }
                    if let Err(e) = interaction
                        .create_interaction_response(http.clone(), |r| {
                            r.kind(InteractionResponseType::UpdateMessage)
                        })
                        .await
                    {
                        error!("{e:#?}");
                    }
                } else if interaction.data.custom_id == "page-content-selector" {
                    let selected = interaction.data.values[0].clone();
                    let mut pager = self.embed_pager.clone().expect("We lost the embed pager");
                    let page = pager
                        .pages
                        .get_mut(&selected)
                        .map(|page| (&selected, page))
                        .expect("Selected non existing page");
                    let mut old_message = interaction.message.clone();
                    old_message
                        .edit(http.clone(), |m| {
                            CrabMessageBuilder::make_embed(m, page, Colour::from_hex("44FF44"));
                            m
                        })
                        .await
                        .handle();
                    if let Err(e) = interaction
                        .create_interaction_response(http.clone(), |r| {
                            r.kind(InteractionResponseType::UpdateMessage)
                        })
                        .await
                    {
                        error!("{e:#?}");
                    }
                } else {
                    continue;
                }
            }
        });
    }
}

pub trait AsReferenceMessage {
    fn as_ref_msg(&self) -> MessageReference;
}

// Cant do that due to possibe future upstream impls
// impl<T> AsReferenceMessage for T
// where
//     T: Into<MessageReference>,
// {
//     fn as_ref_msg(self) -> MessageReference {
//         self.into()
//     }
// }

// Instead we implement our trait by hand for everything
impl AsReferenceMessage for Message {
    fn as_ref_msg(&self) -> MessageReference {
        self.into()
    }
}

impl AsReferenceMessage for &Message {
    fn as_ref_msg(&self) -> MessageReference {
        (*self).into()
    }
}

impl AsReferenceMessage for (ChannelId, MessageId) {
    fn as_ref_msg(&self) -> MessageReference {
        (*self).into()
    }
}

impl AsReferenceMessage for MessageReference {
    fn as_ref_msg(&self) -> MessageReference {
        self.clone()
    }
}

impl CrabMessageBuilder {
    pub fn reference(&mut self, reference: Option<impl AsReferenceMessage>) -> &mut Self {
        self.reference = reference.map(|r| r.as_ref_msg());
        self
    }

    #[allow(unused)]
    pub fn mention<F: FnOnce(&mut CreateAllowedMentions) -> &mut CreateAllowedMentions>(
        &mut self,
        mention: F,
    ) -> &mut Self {
        let allowed_mentions = &mut self.allowed_mentions;
        *allowed_mentions = mention(allowed_mentions).clone();
        self
    }

    pub fn embed<F: FnOnce(&mut CreateEmbed) -> &mut CreateEmbed>(
        &mut self,
        embed: F,
    ) -> &mut Self {
        let page = self.page_mut();
        if let Some(create_embed) = page.embed.as_mut() {
            *create_embed = embed(create_embed).clone();
        } else {
            page.embed = Some(embed(&mut CreateEmbed::default()).clone())
        }
        self
    }

    pub fn title<T: Into<String>>(&mut self, title: T) -> &mut Self {
        self.page_mut().title = Some(title.into());
        self
    }

    pub fn description<D: Into<String>>(&mut self, description: D) -> &mut Self {
        self.page_mut().description = Some(Self::description_pages(description, PAGE_LEN_LIMIT));
        self
    }

    pub fn color(&mut self, color: Colour) -> &mut Self {
        self.color = color;
        self
    }

    pub fn color_hex(&mut self, color: &str) -> &mut Self {
        self.color(Colour::from_hex(color))
    }

    pub fn action_row(&mut self, action_row: CreateActionRow) -> &mut Self {
        self.action_row = Some(action_row);
        self
    }

    pub fn footer_text<T: Into<String>>(&mut self, text: T) -> &mut Self {
        self.page_mut().footer_text = Some(text.into());
        self
    }

    /// Begin a new page or edit an existing page
    pub fn page<T: Into<String>>(&mut self, page: T) -> &mut Self {
        let index = self.page_ref().index;
        let page = page.into();
        if !self.pages.contains_key(&page) {
            self.pages.insert(
                page.clone(),
                CrabMessagePage {
                    index: index + 1,
                    embed: None,
                    title: None,
                    description: None,
                    footer_text: None,
                    fields: None,
                    image_link: None,
                    link: None,
                },
            );
        }
        self.page = page;
        self
    }

    /// Sets a new name for the current page,
    /// if a page with the new name already exists it will be replaced by the current page
    pub fn rename<T: Into<String>>(&mut self, name: T) -> &mut Self {
        let name = name.into();
        let page = self
            .pages
            .remove(&self.page)
            .expect("Where is the current page?");
        self.pages.insert(name.clone(), page);
        self.page = name;
        self
    }

    /// Add fields to page
    pub fn fields<F: Into<CrabMessageFields> + Clone>(&mut self, fields: F) -> &mut Self {
        let page = self.page_mut();
        page.fields = Some(
            fields.into(), // .iter()
                           // .map(|f| f.clone().into())
                           // .collect::<CrabMessageFields>(),
        );
        self
    }

    /// Set image link
    pub fn image<T: Into<String>>(&mut self, url: T) -> &mut Self {
        self.page_mut().image_link = Some(url.into());
        self
    }

    /// Set link in title
    pub fn link<T: Into<String>>(&mut self, link: T) -> &mut Self {
        self.page_mut().link = Some(link.into());
        self
    }

    /// Build and send message
    pub async fn message(&mut self) -> anyhow::Result<CrabMessage> {
        let mut pager = None;
        let mut embed_pager = None;
        let mut pages = self.pages.clone();
        let page = pages
            .iter_mut()
            .find(|(_, page)| page.index == 0)
            .expect("We lost the default page");
        let message = self
            .channel
            .send_message(self.http.clone(), |m| {
                self.create_message(m, page, &mut pager, &mut embed_pager);
                m
            })
            .await?;
        let crab_message = CrabMessage {
            message,
            channel: self.channel,
            pager,
            embed_pager,
        };
        crab_message
            .clone()
            .handle_page_requests(self.shard_messenger.clone(), self.http.clone())
            .await;
        Ok(crab_message)
    }

    /// Generate message contents by page data
    pub fn create_message(
        &mut self,
        m: &mut CreateMessage,
        (page_name, page): (&String, &mut CrabMessagePage),
        pager: &mut Option<CrabMessageDescriptionPager>,
        embed_pager: &mut Option<CrabMessagePager>,
    ) {
        Self::make_embed(m, (page_name, page), self.color);
        let allowed_mentions = self.allowed_mentions.clone();
        m.allowed_mentions(|allow| {
            *allow = allowed_mentions;
            allow
        });

        if let Some(referce) = self.reference.clone() {
            m.reference_message(referce);
        }

        // Add a new interaction row to the message, if the the description has multible pages
        if let Some(description) = &page.description {
            if description.len() > 1 {
                let message_pager = CrabMessageDescriptionPager::new(description.clone());
                m.components(|c| c.add_action_row(message_pager.action_row()));
                *pager = Some(message_pager)
            }
        }

        if self.pages.len() > 1 {
            m.components(|c| {
                c.create_action_row(|r| {
                    r.create_select_menu(|s| {
                        s.custom_id("page-content-selector");
                        s.options(|o| {
                            let mut pages = self.pages.iter().collect::<Vec<_>>();
                            pages.sort_unstable_by_key(|(_, page)| page.index);
                            for (page_name, _) in &pages {
                                o.create_option(|c| c.label(page_name).value(page_name));
                            }
                            o
                        })
                    })
                })
            });
            *embed_pager = Some(CrabMessagePager {
                pages: self.pages.clone(),
            })
        }

        if let Some(action_row) = self.action_row.clone() {
            m.components(|c| c.add_action_row(action_row));
        }
    }

    /// Create an embed for the given message and page
    ///
    /// The color attribute is ignored if the page has a cutom embed
    pub fn make_embed(
        m: &mut impl CrabCreateMessage,
        (_page_name, page): (&String, &mut CrabMessagePage),
        color: Colour,
    ) {
        let mut create_embed = page.embed.clone();

        if let Some(create_embed) = page.embed.clone() {
            m.embed(|e| {
                *e = create_embed.clone();
                if let Some(embed_description) = create_embed
                    .0
                    .get("description")
                    .and_then(|desc| desc.as_str())
                {
                    page.description =
                        Some(Self::description_pages(embed_description, PAGE_LEN_LIMIT));
                    e.description(
                        page.description
                            .clone()
                            .unwrap()
                            .get(0)
                            .cloned()
                            .unwrap_or_default(),
                    );
                }
                e
            });
        } else {
            m.embed(|e| {
                if let Some(title) = &page.title {
                    e.title(title);
                }
                if let Some(description) = &page.description {
                    e.description(description.get(0).cloned().unwrap_or_default());
                }
                if let Some(footer_text) = page.footer_text.clone() {
                    e.footer(|f| f.text(footer_text));
                }
                if let Some(fields) = &page.fields {
                    e.fields(fields.clone());
                }
                if let Some(image_link) = &page.image_link {
                    e.image(image_link);
                }
                if let Some(link) = &page.link {
                    e.url(link);
                }
                e.color(color);

                create_embed = Some(e.clone());

                e
            });
        }
    }

    fn page_mut(&mut self) -> &mut CrabMessagePage {
        self.pages
            .get_mut(&self.page)
            .expect("How did we end up here?!")
    }

    fn page_ref(&self) -> &CrabMessagePage {
        self.pages
            .get(&self.page)
            .expect("How did we end up here?!")
    }

    fn description_pages<T: Into<String>>(description: T, limit: usize) -> Vec<String> {
        // get description as String
        let description_string: String = description.into();

        let mut pages = Vec::new();

        let paragraphs = description_string
            .split('\n')
            .map(|s| s.to_string())
            .collect::<VecDeque<_>>();
        // buffer to iterate paragraphs until it is empty
        let mut paragraphs_buffer = paragraphs;
        // buffer for the current pages text
        let mut page_text_buffer = String::new();
        let mut paragraph_counter = 0;
        // iterate paragraphs
        while let Some(paragraph) = paragraphs_buffer.pop_front() {
            // check if adding the paragraph to the page would exceed the limit
            if page_text_buffer.len() + paragraph.len() > limit {
                // adding paragraph to page would exceed limit
                // try to append individual words
                let mut words = paragraph.split_whitespace().collect::<VecDeque<_>>();
                // iterate words
                while let Some(word) = words.pop_front() {
                    // check if adding the word to the page would exceed the limit
                    if page_text_buffer.len() + word.len() > limit {
                        // adding this word would exceed the limit
                        // append current page text buffer as new page
                        // and clear the buffer
                        pages.push(page_text_buffer.clone());
                        page_text_buffer.clear();
                    }
                    let mut word_string = word.to_string();
                    // In case the word is longer than the limit, ellipsize it.
                    word_string.ellipsize(limit);
                    // If appending  the word does not exceed the limit, we would want to store it,
                    // if appending the word exceeds the limit, we flush the buffer and store
                    // the word for the next iteration.
                    page_text_buffer += &(word_string + " ");
                }
            } else {
                page_text_buffer += &(paragraph + "\n");
            }

            paragraph_counter += 1;
            if paragraph_counter % 35 == 0 {
                pages.push(page_text_buffer.clone());
                page_text_buffer.clear();
            }
        }
        if !page_text_buffer.is_empty() {
            pages.push(page_text_buffer);
        }
        pages
            .iter_mut()
            .for_each(|page| *page = page.trim().to_owned());

        pages
    }
}

impl CrabMessageDescriptionPager {
    pub fn new(pages: Vec<String>) -> Self {
        Self { pages }
    }

    pub fn action_row(&self) -> CreateActionRow {
        let mut create_row = CreateActionRow::default();
        create_row.create_select_menu(|s| {
            s.custom_id("page-selector");
            s.options(|o| {
                debug!("adding n pages: {}", self.pages.len());
                for (index, _page) in self.pages.iter().enumerate() {
                    o.create_option(|c| {
                        c.label(format!("Page {index:^3}", index = index + 1));
                        c.value(index);
                        c
                    });
                }
                o
            });
            s
        });
        create_row
    }
}
