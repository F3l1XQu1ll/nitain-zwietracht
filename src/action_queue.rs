use serenity::{client::Context, prelude::TypeMapKey};
use std::{
    collections::HashMap,
    sync::{atomic::AtomicBool, Arc},
    time::Duration,
};
use tokio::{
    sync::{Mutex, RwLock},
    task::JoinHandle,
    time::sleep,
};

use crate::commands::CommandContext;

#[async_trait]
pub trait ContextActionQueue {
    async fn action_queue<'a>(&'a self) -> Arc<RwLock<ActionQueue>>;
}

#[async_trait]
pub trait CrabAction {
    async fn action(&mut self) -> ActionContinue;
    fn name(&self) -> String;
}

/// A wrapper that is returned by all tasks in the action queue
/// telling the handler function if the task should stay in the queue.
pub struct ActionContinue(pub bool);

/// The action queue is a list (actually HashMap) of "tasks" to be periodically executed.
/// This was introduced to avoid many parallel loops.
pub struct ActionQueue {
    queue: Arc<RwLock<HashMap<String, ActionInfo>>>,
    service: Option<Arc<Mutex<JoinHandle<anyhow::Result<()>>>>>,
    pub running: bool,
    pub tick: u64,
    terminate: Arc<AtomicBool>,
}

/// Helper struct for the Action Queue
///
/// * `action`: The action
/// * `interval`: The number of queue ticks before the action executes
/// * `count`: The number of ticks that have passed since the last execution
/// * `delayed`: If the action should be executed immediately after enqueuing
/// * `first_run`: If the action has not been executed jet
/// * `remove`: If the action should be removed from the queue
#[derive()]
struct ActionInfo {
    action: Box<dyn CrabAction + Send + Sync>,
    interval: u64,
    count: u64,
    delayed: bool,
    first_run: bool,
    remove: bool,
}

#[async_trait]
impl ContextActionQueue for Context {
    async fn action_queue<'a>(&'a self) -> Arc<RwLock<ActionQueue>> {
        let mut write = self.data.write().await;
        let action_queue = write.get_mut::<ActionQueue>().unwrap().clone();
        action_queue
    }
}

#[async_trait]
impl ContextActionQueue for CommandContext {
    async fn action_queue<'a>(&'a self) -> Arc<RwLock<ActionQueue>> {
        self.ctx().action_queue().await
    }
}

impl TypeMapKey for ActionQueue {
    type Value = Arc<RwLock<ActionQueue>>;
}

impl ActionQueue {
    /// Create a new Action Queue.
    ///
    /// # Arguments:
    ///
    /// * `tick` – the length of the break until the execution loop starts from a new.
    /// This is *not* a fixed delay between interations!
    /// Instead, it is the interval of the exection loop.
    /// See [`Intervall`](tokio::time::interval()) for more information.
    pub fn new(tick: u64) -> Self {
        Self {
            queue: Arc::new(RwLock::new(HashMap::new())),
            service: None,
            running: false,
            tick,
            terminate: Arc::new(AtomicBool::new(false)),
        }
    }

    /// Launch the ActionQueue.
    ///
    /// # Warning:
    ///
    /// * The actual duration of the interval may be exceeded by the time required to iterate the queue,
    ///  or it takes extremely long to modify the action queue.
    pub async fn launch(&mut self) {
        if !self.running {
            self.running = true;
            let queue = self.queue.clone();
            let tick = self.tick;
            let terminate = self.terminate.clone();
            // We are running again, so a previous register was successfull
            terminate.store(false, std::sync::atomic::Ordering::Release);
            let service_handle = tokio::spawn(async move {
                //let mut lock = queue.write().await;
                let mut interval = tokio::time::interval(Duration::from_millis(tick));
                loop {
                    // shutdown task if necessary
                    if terminate.load(std::sync::atomic::Ordering::Acquire) {
                        break;
                    }
                    // iterate queue
                    Self::handle_queue(queue.clone()).await;
                    // now wait for the interval to finish
                    interval.tick().await;
                }
                Ok(())
            });
            self.service = Some(Arc::new(Mutex::new(service_handle)));
        } else {
            error!("Action queue already running");
        }
    }

    /// Add an action to the queue.
    ///
    /// This will add or override an existing action with the same `name`.
    ///
    /// # Arguments:
    ///
    /// * `name` – The identifier for the action/task.
    /// * `interval` – The amount of ticks of the queue that is waited until the action is executed.
    /// * `delayed` – If true, the interval is also awaited on the first run of the action.
    /// * `action` – The action to add.
    ///
    /// # Warning:
    ///
    /// * This will wait for the current iteration of the action queue to complete before adding the new action.
    ///
    /// # Examples:
    ///
    /// ```
    /// let action_queue = ActionQueue::new(10); // 10ms tick
    /// action_queue.register_action(
    ///     "test".to_string(),
    ///     20, // 20*10ms=200ms interval of execution
    ///     false, // don't wait before the first run
    ///     Box::new(|| Box::pin(foo())) // foo() has to return a future with ActionContinue as Output.
    /// );
    /// action_queue.launch();
    /// ```
    pub async fn register_action<A: 'static + CrabAction + Send + Sync>(
        &mut self,
        interval: u64,
        delayed: bool,
        action: A,
    ) {
        let name = action.name();

        // Wait if we are modifying the queue already
        debug!("Waiting other register task to finish");
        while self.terminate.load(std::sync::atomic::Ordering::Acquire) {
            sleep(Duration::from_millis(100)).await;
        }

        // Tell the service to shut down
        debug!("Telling service to shut down");
        self.terminate
            .store(true, std::sync::atomic::Ordering::Release);

        let service = self.service.as_ref().unwrap().clone();
        // Borrowchecker-voodoo-magic
        let write = &mut *service.lock().await;
        // Wait for the current iteration to complete
        debug!("Awaiting service shutdown");
        let _res = write.await;
        self.running = false;

        // now modify queue
        debug!("Attempting to get self write lock");
        let mut write = self.queue.write().await;
        debug!("got self write lock");
        if write.contains_key(&name) {
            write.remove(&name);
            info!("Action {:#?} overwritten", name);
        } else {
            info!("Action {:#?} added", name);
        }
        let info = ActionInfo {
            action: Box::new(action),
            interval,
            count: 0,
            delayed,
            first_run: true,
            remove: false,
        };
        write.insert(name, info);
        drop(write);

        // Now start the service again
        self.launch().await;
    }

    /// Iterate queue and execute tasks.
    /// This requires an write lock on the queue - because of that, no task can ever create new actions!
    async fn handle_queue(queue: Arc<RwLock<HashMap<String, ActionInfo>>>) {
        let mut write = queue.write().await;
        for (_name, action_info) in write.iter_mut() {
            if (action_info.first_run && !action_info.delayed)
                || (action_info.count >= action_info.interval)
            {
                let fut = action_info.action.action();
                let action_continue = fut.await;
                if !action_continue.0 {
                    action_info.remove = true;
                }
                action_info.count = 0;
                action_info.first_run = false;
            } else {
                action_info.count += 1;
            }
        }
        write.retain(|name, action_info| {
            if action_info.remove {
                info!("Removing action {name}");
                false
            } else {
                true
            }
        });
    }
}
