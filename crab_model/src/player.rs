use std::str::FromStr;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Default, Deserialize, Serialize, PartialEq, Eq)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Looping a track can be enabled or disabled
pub enum LoopState {
    #[default]
    /// Looping the current track is enabled for this player
    Disabled,
    /// Looping the current track is disabled for this player
    Enabled,
}

#[derive(Debug, Clone, Default, Deserialize, Serialize, PartialEq, Eq)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Autoplay can be enabled or disabled
pub enum AutoplayState {
    #[default]
    /// Autoplay is enabled for this player
    Disabled,
    /// Autoplay is disabled for this player
    Enabled,
}

#[derive(Default, Clone, Debug, Deserialize, Serialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// State of a "player", used to display playback status in the GUI
pub struct State {
    /// If the bot is connected to a voice channel
    pub connected: bool,
    /// The voie channel, the bot is connected to.
    ///
    /// [`None`] if the bot is not connected to a channel.
    pub channel: Option<u64>,
    /// Is loop enabled?
    pub loop_state: LoopState,
    /// Current position on the playing track if any
    pub position: Option<std::time::Duration>,
    /// Full [`Duration`](chrono::Duration) of the current track if any
    pub duration: Option<std::time::Duration>,
    /// Is the playback paused?
    pub paused: bool,
    /// Is autoplay enabled?
    pub autoplay_state: AutoplayState,
    /// Title of the currently playing track if any
    pub title: Option<String>,
    /// Artist of the currently playing track if any
    pub artist: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Possible interactions with the player API interface.
///
/// Basically commands.
/// These won't do anything if no state is beeing changed,
/// eg. pausing an already paused track
pub enum Interaction {
    /// Resume playback
    Resume,
    /// Pause playback
    Pause,
    /// Play the previously played track if any or,
    /// in case the current track has been playing for longer
    /// than [`5 seconds`](State::position) restart the current track.
    Back,
    /// Skip to the next track if any
    Next,
    /// Enable autoplay
    Autoplay,
    /// Disable autoplay
    AutoOff,
    /// Enable loop
    Loop,
    /// Disable loop
    Unloop,
    /// Remove the rack with the given [`ID`](Self::Remove::0) from the queue.
    ///
    /// This only affects the list of *future* tracks, not the already played tracks.
    Remove(u64),
    /// Swap positions of the track with the given [`ID 1`](Self::Swap::0) with another track with the given [`ID 2`](Self::Swap::1).
    ///
    /// This does not affect already played tracks.
    Swap(u64, u64),
}

impl Interaction {
    const RESUME_STR: &'static str = "resume";
    const PAUSE_STR: &'static str = "pause";
    const BACK_STR: &'static str = "back";
    const NEXT_STR: &'static str = "next";
    const AUTOPLAY_STR: &'static str = "autoplay";
    const AUTOOFF_STR: &'static str = "autooff";
    const LOOP_STR: &'static str = "loop";
    const UNLOOP_STR: &'static str = "unloop";
    const REMOVE_STR: &'static str = "remove";
    const SWAP_STR: &'static str = "swap";

    #[must_use]
    /// Map each [`Interaction`](Self) to an unique [`&'static str`](std::str) that can be used as the required API parameter
    ///
    /// | Interaction | String |
    /// |-------------|--------|
    /// |[`Self::Resume`]|"resume"|
    /// |[`Self::Pause`]|"pause"|
    /// |[`Self::Back`]|"back"|
    /// |[`Self::Next`]|"next"|
    /// |[`Self::Autoplay`]|"autoplay"|
    /// |[`Self::AutoOff`]|"autooff"|
    /// |[`Self::Loop`]|"loop"|
    /// |[`Self::Unloop`]|"unloop"|
    /// |[`Self::Remove`]|"remove"|
    /// |[`Self::Swap`]|"swap"|
    ///
    /// This mapping is also applied if [`FromStr`](Self::from_str()) is used.
    pub fn as_request(&self) -> &'static str {
        match self {
            Interaction::Resume => Self::RESUME_STR,
            Interaction::Pause => Self::PAUSE_STR,
            Interaction::Back => Self::BACK_STR,
            Interaction::Next => Self::NEXT_STR,
            Interaction::Autoplay => Self::AUTOPLAY_STR,
            Interaction::AutoOff => Self::AUTOOFF_STR,
            Interaction::Loop => Self::LOOP_STR,
            Interaction::Unloop => Self::UNLOOP_STR,
            Interaction::Remove(_) => Self::REMOVE_STR,
            Interaction::Swap(_, _) => Self::SWAP_STR,
        }
    }
}

impl FromStr for Interaction {
    type Err = ();

    /// Do not use this for anything but strings generated by [`Self::as_request()`]
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            Self::RESUME_STR => Ok(Self::Resume),
            Self::PAUSE_STR => Ok(Self::Pause),
            Self::BACK_STR => Ok(Self::Back),
            Self::NEXT_STR => Ok(Self::Next),
            Self::AUTOPLAY_STR => Ok(Self::Autoplay),
            Self::AUTOOFF_STR => Ok(Self::AutoOff),
            Self::LOOP_STR => Ok(Self::Loop),
            Self::UNLOOP_STR => Ok(Self::Unloop),
            Self::REMOVE_STR => Ok(Self::Remove(0)),
            Self::SWAP_STR => Ok(Self::Swap(0, 0)),
            &_ => Err(()),
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Info about a guild that the bot and the user have in common and potentialy have currently playback going on.
pub struct AvailableGuild {
    /// Guild ID from serenity
    pub guild_id: u64,
    /// Name of the guild
    pub guild_name: String,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// An entry in the bots playback queue
pub struct QueueEntry {
    /// Title of the track
    pub title: String,
    /// Internal, unique id of the track
    pub id: u64,
}

#[derive(Deserialize, Serialize, Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Collection of the previously played tracks and future to be played tracks
pub struct QueueState {
    /// Tracks that have previously been played or better "enqeued"
    pub played: Vec<QueueEntry>,
    /// Tracks that are queued to be played next
    pub enqueued: Vec<QueueEntry>,
}
