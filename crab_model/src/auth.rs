use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// A collection of data about a discord user profile
pub struct UserRecord {
    /// Name of the user
    pub name: String,
    /// The users account id, corresponds to [`serenity`]s [`UserID`]
    ///
    /// [`serenity`]: https://docs.rs/serenity/latest/serenity/index.html
    /// [`UserID`]: https://docs.rs/serenity/latest/serenity/model/id/struct.UserId.html
    pub id: u16,
    /// Optional url to a profile image
    pub profile_img: Option<String>,
    /// If the user is considered an admin by the bot.
    /// This only applies to the application developers not discord admins.
    pub admin: bool,
}
