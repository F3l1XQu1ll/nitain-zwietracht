use std::fmt::{Debug, Display};

use serde::{Deserialize, Serialize};

/// Link to [Warframe Hub]s *PC* API endpoint with longuage flag set to english (`?language=en`)
///
/// [Warframe Hub]: https://hub.warframestat.us/
pub const WFAPI_WORLD_URL: &str = "https://api.warframestat.us/pc/?language=en";
/// Link to [Warframe Hub]s *`solNodes/search`* api endpoint
///
/// [Warframe Hub]: https://hub.warframestat.us/
pub const WFAPI_NODES_URL: &str = "https://api.warframestat.us/solNodes/search/";

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
#[serde(rename_all = "lowercase")]
/// Active world-cycle for a [Landscape](https://warframe.fandom.com/wiki/Landscape)
pub enum WFCycleActive {
    /// Only appears in the Cambion Drift/Necralisk
    Fass,
    /// Only appears in the Cambion Drift/Necralisk
    Vome,
    /// Only appears in the Orb Vallis/Fortuna
    Cold,
    /// Only appears in the Orb Vallis/Fortuna
    Warm,
    /// Only appears in the Plains of Eidolon and the Earth Forrests
    Night,
    /// Only appears in the Plains of Eidolon and the Earth Forrests
    Day,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
#[allow(missing_docs)]
/// Enemy [Factions](https://warframe.fandom.com/wiki/Factions)
///
/// See also <https://warframe.fandom.com/wiki/Codex/EnemyList> for more info
pub enum WFEnemy {
    Corpus,
    Corrupted,
    /// Special case for "Crossfire" missions
    Crossfire,
    Grineer,
    #[serde(alias = "Infestation")]
    Infested,
    Orokin,
    Sentient,
    /// Not sure about the circumstances for this
    Tenno,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
/// A node in warframes solar system
pub struct WFNode {
    #[serde(alias = "value")]
    /// Name of the node
    pub name: String,
    /// Enemy or opposing factions associated with this node
    enemy: WFEnemy,
    #[serde(alias = "type")]
    /// Type of mission associated with this node
    ///
    /// The api for this seems to be a bit *odd*,
    /// so we don't parse this as enum variant
    pub mission_type: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Representation of a day-night cycle for a [Landscape](https://warframe.fandom.com/wiki/Landscape)
pub struct WFCycle {
    #[serde(alias = "state")]
    /// Active cycle/state
    pub active: WFCycleActive,
    /// When the currently active state expires
    /// ([`Option`](Option) because parsing may fail)
    pub expiry: Option<chrono::DateTime<chrono::Utc>>,
    #[serde(skip_deserializing)]
    /// The image associated with the current state that is displayed in the gui.
    /// This value is manually populated and may be removed in the future.
    #[deprecated]
    pub active_img: Option<&'static str>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Arbitration data
///
/// Parsing is unstable since the api for this is unstable
pub struct WFArbitration {
    /// When the data expires
    /// ([`Option`](Option) because parsing may fail)
    pub expiry: Option<chrono::DateTime<chrono::Utc>>,
    /// Node of the arbitration.
    /// String since the api does not return a full variant of WFNode
    pub node: String,
    /// Enemy associated with the node
    pub enemy: WFEnemy,
    /// Empty by default, fill with `insert_mission_type()` or with another manual implementation.
    pub mission_type: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// An item sold by Baro Ki'Teer
pub struct WFBaroItem {
    /// Item name
    pub item: String,
    /// Amount of ducates required for purchase
    pub ducats: u32,
    /// Amount of credits required for purchase
    pub credits: u32,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Void-Trader / *"Baro"* data
pub struct WFBaro {
    /// When the trader will be available
    pub activation: Option<chrono::DateTime<chrono::Utc>>,
    /// When the trader will disappear
    pub expiry: Option<chrono::DateTime<chrono::Utc>>,
    /// Where to find the trader. Unavailable if the trader is not active
    pub location: Option<String>,
    /// If the trader is currently available
    pub active: bool,
    /// The items which the trader currently sells or will sell once he is active.
    /// In the latter case, this list is unreliable for obvious reasons.
    pub inventory: Vec<WFBaroItem>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Collection of all available [`WFCycles`](WFCycle)
pub struct WFWorldCycles {
    #[serde(rename = "earthCycle")]
    /// Earth Forrests cycle
    pub earth: WFCycle,
    #[serde(rename = "cetusCycle")]
    /// Plains of Eidolon cycle
    pub cetus: WFCycle,
    #[serde(rename = "vallisCycle")]
    /// Orb Vallis cycle
    pub fortuna: WFCycle,
    #[serde(rename = "cambionCycle")]
    /// Cambion Drift cycle
    pub necralisk: WFCycle,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Data for a Void Fissure
pub struct WFFissure {
    /// Location of the fissure
    pub node: String,
    /// Tier of the fissure (*Lith, Meso, Neo, etc.*)
    pub tier: String,
    #[serde(alias = "tierNum")]
    /// Number associated with that [tier](Self::tier)
    /// starting at 1 for Lith:
    ///
    /// | Num | Tier |
    /// |-----|------|
    /// |1|Lith|
    /// |2|Meso|
    /// |3|Neo|
    /// |4|Axi|
    /// |5|Requiem|
    pub tier_num: u32,
    /// When the data expires
    /// ([`Option`](Option) because parsing may fail)
    pub expiry: Option<chrono::DateTime<chrono::Utc>>,
    /// The enemy associated with the node associated with this fissure
    pub enemy: WFEnemy,
    /*#[serde(skip_deserializing)]
    mission_type: Option<String>,*/
    #[serde(alias = "missionType")]
    /// Type of mission
    pub mission_type: String,
    #[serde(alias = "isStorm")]
    /// If the fissure is actually a Void Storm
    pub is_storm: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// A variant of a Sortie mission
pub struct WFSortieVariant {
    #[serde(alias = "missionType")]
    /// Name of the mission
    pub name: String,
    #[serde(alias = "modifierDescription")]
    /// Description of the [`modifier`](Self::modifier) applied to this mission
    pub description: String,
    /// Modifier applied to this mission, eg. "*Radiation Hazard*"
    pub modifier: String,
    #[serde(rename = "node")]
    /// Location/Node of the mission
    pub location: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Data for todays Sortie
pub struct WFSortie {
    /// When the Sortie ends and is replaced with a new one
    pub expiry: Option<chrono::DateTime<chrono::Utc>>,
    /// A Sortie has always 3 variants
    pub variants: Vec<WFSortieVariant>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// A mission for a Syndicate
pub struct WFSyndicateJob {
    /// Actually the name of the mission
    pub r#type: String,
    #[serde(alias = "enemyLevels")]
    /// Level range for enemies
    pub enemy_levels: Vec<u32>,
    /// Possible rewards in case of compleation
    #[serde(alias = "rewardPool")]
    pub rewards: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// A Syndicate
pub struct WFSyndicate {
    /// The name of the Syndicate
    pub syndicate: String,
    /// The list of missions currently available
    pub jobs: Vec<WFSyndicateJob>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// An ongoing ingame event
///
/// Note: Not really tested
pub struct WFEvent {
    /// InGame description
    pub description: String,
    /// InGame tooltip
    pub tooltip: String,
    /// When the event ends,
    /// ([`Option`](Option) because parsing may fail)
    pub expiry: Option<chrono::DateTime<chrono::Utc>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "okapi", derive(rocket_okapi::JsonSchema))]
/// Available worldstate info
pub struct WFWorld {
    #[serde(flatten)]
    /// Worldcycle info
    cycles: WFWorldCycles,
    /// Arbitration info
    arbitration: WFArbitration,
    #[serde(alias = "voidTrader")]
    /// Void Trader info
    baro: WFBaro,
    /// Current fissures
    fissures: Vec<WFFissure>,
    /// Current Sortie
    sortie: WFSortie,
    #[serde(alias = "syndicateMissions")]
    /// Syndicates info
    syndicates: Vec<WFSyndicate>,
    /// Events info
    events: Vec<WFEvent>,
}

impl Display for WFCycleActive {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self, f)
    }
}

impl Display for WFEnemy {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Debug::fmt(self, f)
    }
}

impl WFWorld {
    /// Get a [Vec] of mutable references to the [`WFCycle`] elements inside [`cycles`](Self::cycles).
    pub fn cycles_vec(&mut self) -> Vec<&mut WFCycle> {
        vec![
            &mut self.cycles.earth,
            &mut self.cycles.cetus,
            &mut self.cycles.fortuna,
            &mut self.cycles.necralisk,
        ]
    }

    #[must_use]
    /// Get cycles field of `WFWorld`
    pub fn cycles(&self) -> WFWorldCycles {
        self.cycles.clone()
    }

    #[must_use]
    /// Get arbitration field of `WFWorld`
    pub fn arbitration(&self) -> WFArbitration {
        self.arbitration.clone()
    }

    #[must_use]
    /// Get baro field of `WFWorld`
    pub fn baro(&self) -> WFBaro {
        self.baro.clone()
    }

    #[must_use]
    /// Get fissures field of `WFWorld`
    pub fn fissures(&self) -> Vec<WFFissure> {
        self.fissures.clone()
    }

    /// Get a mutable reference to the arbitration field
    pub fn arbitration_mut(&mut self) -> &mut WFArbitration {
        &mut self.arbitration
    }

    #[must_use]
    /// Get sortie field of `WFWorld`
    pub fn sortie(&self) -> WFSortie {
        self.sortie.clone()
    }

    #[must_use]
    /// Get syndicates field of `WFWorld`
    pub fn syndicates(&self) -> Vec<WFSyndicate> {
        self.syndicates.clone()
    }

    #[must_use]
    /// Get events field of `WFWorld`
    pub fn events(&self) -> Vec<WFEvent> {
        self.events.clone()
    }
}
