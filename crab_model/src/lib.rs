#![warn(clippy::pedantic)]
#![warn(missing_docs)]
#![warn(rustdoc::all)]

//! Helper crate that defines some structures and types used in `NitainCrab`
//!
//! This crate is mainly aimed to assist building our web gui by ensuring
//! that we (de)serialize the correct types.

pub extern crate chrono;
#[cfg(feature = "okapi")]
pub extern crate schemars;

/// Structs for warframe related api
pub mod warframe;

/// Structs for authentication related api
pub mod auth;

/// Structs for (music) player related api
pub mod player;
