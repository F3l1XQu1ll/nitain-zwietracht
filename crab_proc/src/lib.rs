use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{
    bracketed,
    parse::{Parse, ParseStream, Parser, Result},
    parse_macro_input,
    punctuated::Punctuated,
    Error, Ident, Meta, Path, Token,
};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}

struct GroupName {
    name: Ident,
}

impl Parse for GroupName {
    fn parse(input: ParseStream) -> Result<Self> {
        input.parse::<Token![struct]>()?;
        let name = input.parse()?;
        input.parse::<Token![;]>()?;
        Ok(Self { name })
    }
}

#[proc_macro_attribute]
pub fn crab_group(args: TokenStream, input: TokenStream) -> TokenStream {
    let GroupName { name } = parse_macro_input!(input as GroupName);
    let args_parser = Punctuated::<Ident, Token![,]>::parse_terminated;
    // list of idents for further macros
    let args_parsed = args_parser.parse(args).unwrap();
    // list of command identifiers
    let commands = args_parsed.iter().collect::<Vec<_>>();

    // list of all _com_ctx commands
    // let com_ctx_comms = commands
    //     .iter()
    //     .map(|comm| format_ident!("{}_com_ctx", comm))
    //     .collect::<Vec<_>>();
    // // list of corresponding serenity commands (_COMMAND)
    // let serenity_commands = commands
    //     .iter()
    //     .map(|comm| format_ident!("{}_COMMAND", comm.to_string().to_uppercase()))
    //     .collect::<Vec<_>>();
    // // list of corresponding _SLASH_ARGS_ENABLED properties
    // let comms_args_enabled = commands
    //     .iter()
    //     .map(|comm| format_ident!("{}_SLASH_ARGS_ENABLED", comm.to_string().to_uppercase()));

    // list of kniveboxes
    let knive_groups = commands
        .iter()
        .map(|comm| format_ident!("{}_KNIVEBOX", comm.to_string().to_uppercase()));
    // name of the serenity commands group (to be generated)
    let serenity_group_name = format_ident!("{}_GROUP", name.to_string().to_uppercase());
    // name of the crab commands group
    let crab_group_name = format_ident!("{}_CRAB_GROUP", name.to_string().to_uppercase());
    let quoted = quote! {
        #[group]
        #[commands(
            #(#commands),*
        )]
        struct #name;

        extern crate self as nitain_crab;
        pub static #crab_group_name: nitain_crab::commands::CrabGroup = nitain_crab::commands::CrabGroup{
            group: &#serenity_group_name,
            slash_commands: &[#(
                &#knive_groups
                /*nitain_crab::commands::KniveBox {
                    serenity_command: &#serenity_commands,
                    crab_command: #com_ctx_comms,
                    slash_args_enabled: &#comms_args_enabled,
                }*/

            ),*],
        };
    };
    TokenStream::from(quoted)
}

struct CommandArgs {
    name: Ident,
    meta: Vec<Meta>,
    args_enabled: bool,
    slash_enabled: bool,
    callback: Path,
}

impl Parse for CommandArgs {
    fn parse(input: ParseStream) -> Result<Self> {
        let name = input.parse()?;
        input.parse::<Token![,]>()?;
        let mut meta = Vec::new();
        while input.peek(Token![#]) {
            input.parse::<Token![#]>()?;
            let meta_arg;
            bracketed!(meta_arg in input);
            meta.push(meta_arg.parse::<Meta>()?);
            input.parse::<Token![,]>()?;
        }
        if meta.is_empty() {
            input.parse::<Token![,]>()?;
        }
        let mut args_enabled = false;
        let mut slash_enabled = true;
        while input.peek(Token![@]) {
            input.parse::<Token![@]>()?;
            let attribute = input.parse::<Ident>()?.to_string();
            match attribute.as_str() {
                "call" => {}
                "call_args" => args_enabled = true,
                "no_slash" => slash_enabled = false,
                _ => {
                    Error::new_spanned(attribute, "Unrecognized anotation").to_compile_error();
                }
            }
        }
        let callback = input.parse()?;
        Ok(Self {
            name,
            meta,
            args_enabled,
            slash_enabled,
            callback,
        })
    }
}

#[proc_macro]
pub fn crab_command(input: TokenStream) -> TokenStream {
    let CommandArgs {
        name,
        meta,
        args_enabled,
        slash_enabled,
        callback,
    } = parse_macro_input!(input as CommandArgs);
    let command_com_ctx = format_ident!("{}_com_ctx", name);
    let serenity_command = format_ident!("{}_COMMAND", name.to_string().to_uppercase());
    let knive_box_name = format_ident!("{}_KNIVEBOX", name.to_string().to_uppercase());
    let quoted = quote! {
        //extern crate self as nitain_crab;

        #[command]
        #(
            #[#meta]
        )*
        pub async fn #name(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
            let context = nitain_crab::commands::CommandContext::new(
                ctx.clone(),
                msg.guild(&ctx),
                msg.channel_id,
                Some(msg.id),
                msg.author.clone(),
                args,
            );
            #command_com_ctx(&context).await
        }

        pub static #knive_box_name: nitain_crab::commands::KniveBox = nitain_crab::commands::KniveBox {
            serenity_command: &#serenity_command,
            crab_command: #command_com_ctx,
            slash_args_enabled: #args_enabled,
            slash_enabled: #slash_enabled,
        };
        pub fn #command_com_ctx<'fut>(ctx: &'fut nitain_crab::commands::CommandContext) -> futures::future::BoxFuture<'fut, CommandResult> {
            use futures::future::FutureExt;
            async move {
                let res: CommandResult = {
                    nitain_crab::maybe_result!(ctx.ctx(), ctx.channel_id(), ctx.ref_msg(), #callback(&ctx).await);
                    Ok(())
                };
                res
            }.boxed()
        }
    };
    TokenStream::from(quoted)
}
