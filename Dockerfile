FROM alpine:latest
RUN apk add openssl
RUN apk add opus
RUN apk add py3-pip
RUN pip install youtube-dlc
RUN apk add ffmpeg
COPY target/release/nitain_crab_bot .
COPY static ./static
RUN ls /
CMD ROCKET_PORT=$PORT ROCKET_ADDRESS=$ROCKET_ADDRESS ./nitain_crab_bot
